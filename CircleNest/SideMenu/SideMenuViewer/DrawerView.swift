//
//  DrawerView.swift
//  NavigationDrawer
//
//  Created by Sowrirajan Sugumaran on 05/10/17.
//  Copyright © 2017 Sowrirajan Sugumaran. All rights reserved.
//

import UIKit

// Delegate protocolo for parsing viewcontroller to push the selected viewcontroller
protocol DrawerControllerDelegate: class {
    func pushTo(viewController : UIViewController)
    func showAlertForLogout()
}

class DrawerView: UIView {
    public let screenSize = UIScreen.main.bounds
    var backgroundView = UIView()
    var drawerView = UIView()
    var tblVw = UITableView()
    var aryViewControllers = NSArray()
    weak var delegate:DrawerControllerDelegate?
    var currentViewController = UIViewController()
    var sideMenuView = SideMenuHeader()

    convenience init(aryControllers: NSArray, controller:UIViewController) {
        self.init(frame: UIScreen.main.bounds)
        self.tblVw.register(UINib.init(nibName: "DrawerCell", bundle: nil), forCellReuseIdentifier: "DrawerCell")
        self.initialise(controllers: aryControllers, controller:controller)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func initialise(controllers:NSArray,controller:UIViewController) {
        currentViewController = controller
        currentViewController.tabBarController?.tabBar.isHidden = true
        
        backgroundView.frame = frame
        drawerView.backgroundColor = UIColor.white
        backgroundView.backgroundColor = UIColor.black
        backgroundView.alpha = 0.6

        let tap = UITapGestureRecognizer(target: self, action: #selector(DrawerView.actDissmiss))
        backgroundView.addGestureRecognizer(tap)
        addSubview(backgroundView)
        
        drawerView.frame = CGRect(x:0, y:0, width:screenSize.width/2+125, height:screenSize.height)
        drawerView.clipsToBounds = true
        self.allocateLayout(controllers:controllers)
    }
    
    func allocateLayout(controllers:NSArray) {
        self.sideMenuView =  SideMenuHeader.instanceFromNib()
        self.sideMenuView.delegate = self
        let userDefaults = UserDefaults.standard
        if let userName = userDefaults.value(forKey: "user_name") as? String {
            self.sideMenuView.lblUserName.text = userName
        }
        if let userNo = userDefaults.value(forKey: "user_no") as? String {
            self.sideMenuView.lblUserMobileNumber.text = userNo
        }
        if let userGender = userDefaults.value(forKey: "user_gender") as? String {
            if userGender.lowercased() == "Male".lowercased() {
                self.sideMenuView.ivUserImageView.image = UIImage(named: "men.png")
            } else {
                self.sideMenuView.ivUserImageView.image = UIImage(named: "women.png")
            }
        }
        if let totalSalesCount = userDefaults.value(forKey: "totalSalesCount") as? String {
            for char in totalSalesCount {
                let label = UILabel()
                label.text = " \(char) "
                label.layer.borderWidth = 1.0
                label.layer.borderColor = UIColor.black.cgColor
                label.layer.masksToBounds = true
                label.layer.cornerRadius = 5.0
                label.backgroundColor = UIColor.white
                self.sideMenuView.stackViewForSales.addArrangedSubview(label)
            }
        }
        tblVw.frame = CGRect(x:0, y:sideMenuView.frame.origin.y+sideMenuView.frame.size.height, width:screenSize.width/2+125, height:screenSize.height)
        
        tblVw.separatorStyle = UITableViewCell.SeparatorStyle.none
        aryViewControllers = controllers
        tblVw.delegate = self
        tblVw.dataSource = self
        tblVw.backgroundColor = UIColor.clear   
        drawerView.addSubview(tblVw)
        tblVw.reloadData()
        drawerView.addSubview(sideMenuView)
        addSubview(drawerView)
    }
    
    
    // To dissmiss the current view controller tab bar along with navigation drawer
    @objc func actDissmiss() {
        currentViewController.tabBarController?.tabBar.isHidden = false
        self.dissmiss()
    }
    
    // Action for logout to quit the application.
    @objc func actLogOut() {
        exit(0)
    }
    func show() {
        self.drawerView.frame.origin = CGPoint(x:-self.frame.size.width, y:0)
        self.backgroundView.alpha = 0.0
        
        UIApplication.shared.keyWindow?.addSubview(self)

        //UIApplication.shared.delegate?.window??.rootViewController?.view.addSubview(self)
        UIView.animate(withDuration: 0.3) {
            self.backgroundView.alpha = 0.6
            self.drawerView.frame.origin = CGPoint(x:0, y:0)
        }
    }
    
    // UIView animation for hiding the navigation drawer
    func dissmiss() {
       UIView.animate(withDuration: 0.2, animations: {
        self.drawerView.frame.origin = CGPoint(x:-self.frame.size.width, y:0)
        self.backgroundView.alpha = 0.0
        
       }) { (completion) in
            self.removeFromSuperview()
        }
    }
}
extension DrawerView : headerSelectionDelegate {
    func clickedHeader(sender: UIButton) {
        actDissmiss()
        let storyBoard = UIStoryboard(name:"CircleNest", bundle:nil)
        let controllerName = storyBoard.instantiateViewController(withIdentifier: "CNUserProfileVC")
        controllerName.hidesBottomBarWhenPushed = true
        self.delegate?.pushTo(viewController: controllerName)
        
    }
}

extension DrawerView : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryViewControllers.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DrawerCell") as! DrawerCell
        cell.backgroundColor = UIColor.clear
        cell.lblController?.text = aryViewControllers[indexPath.row] as? String
        cell.imgController?.image = UIImage(named: Utill.DrawerArray.img_array[indexPath.row] as? String ?? "")
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        if indexPath.row == self.aryViewControllers.count-2{
            cell.lblSeperator.isHidden = false
        }else{
            cell.lblSeperator.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        actDissmiss()
        let storyBoard = UIStoryboard(name:"Main", bundle:nil)
        let identifier = aryViewControllers[indexPath.row] as! String
        if identifier == "Logout"{
            self.delegate?.showAlertForLogout()
            
        }else{
            var controllerName = storyBoard.instantiateViewController(withIdentifier: identifier)
            if identifier == "Home"{
                controllerName.hidesBottomBarWhenPushed = false
            
            }else if identifier == "Refer A Friend" || identifier == "About Us" || identifier == "Help & Support"{
                controllerName  = storyBoard.instantiateViewController(withIdentifier: identifier)
                controllerName.hidesBottomBarWhenPushed = true
                
            }else{
                if identifier == "My Wallet" {
                    isPushed = true
                }
                controllerName.hidesBottomBarWhenPushed = true
            }
            self.delegate?.pushTo(viewController: controllerName)
        }
    }
}
