//
//  SideMenuHeader.swift
//  CircleNest
//
//  Created by Ankit Gupta on 12/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

protocol headerSelectionDelegate {
    func clickedHeader(sender : UIButton)
}

class SideMenuHeader: UIView {
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserMobileNumber: UILabel!
    @IBOutlet weak var ivUserImageView: UIImageView!
    @IBOutlet weak var btnHeaderSelected: UIButton!
    @IBOutlet weak var stackViewForSales: UIStackView!
    
    var delegate: headerSelectionDelegate?
    
    override class func awakeFromNib() {
        print("Hello World")
    }
    
    class func instanceFromNib() -> SideMenuHeader {
        return UINib(nibName: "SideMenuHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SideMenuHeader
    }

    @IBAction func btnHeaderSelectionAction(_ sender: UIButton) {
        delegate?.clickedHeader(sender: sender)
    }
    

}
