//
//  CNCancellingOrderTableCell.swift
//  CircleNest
//
//  Created by techjini on 13/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
protocol cancelTableCellAction {
    func cancellingReasonTapped(indexPath: Int)
    func submitRequestTapped(cancelReasonValue: String?, textFieldText: String?)
}

class CNCancellingOrderTableCell: UITableViewCell {

    @IBOutlet weak var lblCancelReasonValue: UILabel!
    @IBOutlet weak var tfCommentField: UITextField!
    @IBOutlet weak var btnSubmitRequest: UIButton!
    @IBOutlet weak var btnCancelReason: UIButton!
    
    var delegate: cancelTableCellAction?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnCancellationReason(_ sender: UIButton) {
        delegate?.cancellingReasonTapped(indexPath: sender.tag)
    }
    
    @IBAction func btnCancelSubmitRequest(_ sender: UIButton) {
        delegate?.submitRequestTapped(cancelReasonValue: self.lblCancelReasonValue.text, textFieldText: self.tfCommentField.text)
    }
    
}

