//
//  CNCancelOrderVC.swift
//  CircleNest
//
//  Created by techjini on 21/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNCancelOrderVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var product : [productList]?
    var popController: CNPPopupController?
    var cancelReason: String? = "Reason For Cancellation"
    var orderId: DynamicDataType?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI() {
        self.navigationItem.title = "Order List"
        self.tableView.register(UINib(nibName: "CNOrderListTableCell", bundle: nil), forCellReuseIdentifier: "CNOrderListTableCell")
        self.tableView.register(UINib(nibName: "CNCancellingOrderTableCell", bundle: nil), forCellReuseIdentifier: "CNCancellingOrderTableCell")
        self.tableView.register(UINib(nibName: "CNOrderIdTableCell", bundle: nil), forCellReuseIdentifier: "CNOrderIdTableCell")
    }
    
    func callCancelOrderAPI(orderCancelReason: String?, orderCancelComment: String?){
        self.showHUD(progressLabel: "Loading")
        let vm = CNMyOrderDetailVM()
        var paramDict  = ["method" : "cancelOrder","orderId" : self.orderId?.type ?? "", "storeId": 101, "reason":orderCancelReason ?? "","comment":orderCancelComment ?? ""] as [String:AnyObject]
        let userDefaults = UserDefaults.standard
        if let userId = userDefaults.value(forKey: "user_id") {
            paramDict.updateValue(userId as AnyObject, forKey: "userId")
        }
        vm.callOrderCancelAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.view.makeToast(responseMessage)
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: CNMyorderListVC.self) {
                        _ =  self.navigationController?.popToViewController(controller, animated: true)
                        break
                    }
                }
            } else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    
    @IBAction func btnBackPressed(_sender:Any){
        self.navigationController?.popViewController(animated: true)
    }

}

extension CNCancelOrderVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let productCount = self.product?.count ?? 0
        return productCount + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let productCount = self.product?.count ?? 0
        if indexPath.row == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNOrderIdTableCell", for: indexPath) as? CNOrderIdTableCell {
                cell.lblOrderid.text = "ORDER ID : \(self.orderId?.type ?? "0")"
                return cell
            }
        } else if indexPath.row < productCount + 1 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNOrderListTableCell", for: indexPath) as? CNOrderListTableCell {
                let productDict = self.product?[indexPath.row - 1]
                cell.populatedata(dict_item: productDict)
                return cell
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNCancellingOrderTableCell", for: indexPath) as? CNCancellingOrderTableCell {
                cell.delegate = self
                cell.tfCommentField.delegate = self
                cell.lblCancelReasonValue.text = self.cancelReason
                if self.cancelReason == "Reason For Cancellation"{
                   cell.lblCancelReasonValue.textColor = UIColor.lightGray
                }else{
                   cell.lblCancelReasonValue.textColor = UIColor.black
                }
                
                return cell
            }
        }
        return UITableViewCell()
    }
}

extension CNCancelOrderVC: cancelTableCellAction {
    
    func cancellingReasonTapped(indexPath: Int) {
        let rectShape = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        let view = CNDeliveryTimePopUp(frame: rectShape)
        view.data_array = ["Order placed by mistake","The delivery is delayed","Item Price/shipping cost is too high","Need to change shipping address","Bought it from somewhere else","My reason is not listed"]
        view.delegate = self
        view.lblTitleText.text = "Reason for Cancellation"
        view.lblSubtitleText.text = ""
        view.popUpHeightConstraint.constant = 320
        view.setupUI()
        view.tblPopUpView.reloadData()
        let popupController = CNPPopupController(contents: [view])
        self.popController = popupController
        self.popController?.theme = CNPPopupTheme.default()
        self.popController?.theme.popupStyle = .centered
        self.popController?.delegate = self
        popupController.present(animated: false)
    }
    
    func submitRequestTapped(cancelReasonValue: String?, textFieldText: String?) {
        
        if cancelReasonValue?.lowercased() == "Reason For Cancellation".lowercased() {
            self.view.makeToast(ConstantValue.orderCancelError.rawValue)
        } else {
            self.callCancelOrderAPI(orderCancelReason: cancelReasonValue, orderCancelComment: textFieldText)
        }
        
    }
    
}

extension CNCancelOrderVC: CNPPopupControllerDelegate {
    
}

extension CNCancelOrderVC: deliveryTimeDelegate {
    func deliveryTimePresed(timePresed: String?, popUpPresentText: String?, timeIdPresed: String?) {
        self.cancelReason = timePresed
        self.tableView.reloadData()
        popController?.dismiss(animated: false)
    }
    
    func dismissPopup() {
        popController?.dismiss(animated: false)
    }
}

extension CNCancelOrderVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
}
