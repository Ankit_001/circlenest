//
//  CNHelpSupportVC.swift
//  CircleNest
//
//  Created by Ankit Gupta on 14/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
import MessageUI

class CNHelpSupportVC: UIViewController {
    @IBOutlet weak var tablevView : UITableView!
    @IBOutlet weak var viewMail : UIView!
    @IBOutlet weak var viewMobile : UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUi()
        self.registerCell()
    }
    func registerCell(){
       self.tablevView.register(UINib(nibName: "CNHelpSupportCell", bundle: nil), forCellReuseIdentifier: "CNHelpSupportCell")
    }
    func setupUi(){
        self.tablevView.estimatedRowHeight = 44.0
        self.tablevView.rowHeight = UITableView.automaticDimension
        self.viewMail.layer.cornerRadius = 5.0
        self.viewMail.layer.borderWidth = 1.0
        self.viewMail.layer.borderColor = Utill.Color.color_blue.cgColor
        self.viewMobile.layer.cornerRadius = 5.0
        self.viewMobile.layer.borderWidth = 1.0
        self.viewMobile.layer.borderColor = Utill.Color.color_blue.cgColor
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_SupportEmailPopUpAction(_ sender: UIButton) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setSubject("Feedback")
            mail.setToRecipients(["support@circlenest.com"])
            mail.setMessageBody("Feature request or bug report?", isHTML: false)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    @IBAction func btn_SupportPhonePopUpAction(_ sender: UIButton) {
        if let url = URL(string: "tel://\(9910253377)") {
            UIApplication.shared.open(url)
        }
    }
    
}
extension CNHelpSupportVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CNHelpSupportCell", for: indexPath) as? CNHelpSupportCell{
            cell.delegate = self
            return cell
        }
        return UITableViewCell()
    }
}
extension CNHelpSupportVC : btnClickDelegate{
    func btnPressed(_tag: Int) {
        let tag = _tag
        if let vc = self.storyboard?.instantiateViewController(identifier: "CNHelpSupportdetailVC") as? CNHelpSupportdetailVC{
            vc.btn_tag = tag
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension CNHelpSupportVC: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case .cancelled:
            print("Mail cancelled")
        case .failed:
            print("Mail sent failure: \(error?.localizedDescription ?? "")")
        case .saved:
            print("Mail saved")
        case .sent:
            print("Mail sent")
        default:
            break
        }
        self.dismiss(animated: true, completion: nil)
    }
    
}
