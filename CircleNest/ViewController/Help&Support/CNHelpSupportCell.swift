//
//  CNHelpSupportCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 07/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
protocol btnClickDelegate {
    func btnPressed(_tag : Int)
}

class CNHelpSupportCell: UITableViewCell {
    var delegate : btnClickDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    @IBAction func btnClicked(_sender:UIButton){
        print(_sender.tag)
        self.delegate?.btnPressed(_tag: _sender.tag)
        
    }
    
    
}
