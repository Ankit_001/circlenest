//
//  CNHelpSupportdetailVC.swift
//  CircleNest
//
//  Created by Ankit Gupta on 07/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
import MessageUI

class CNHelpSupportdetailVC: UIViewController {
    var btn_tag : Int?
    @IBOutlet weak var tablevView : UITableView!
    @IBOutlet weak var viewMail : UIView!
    @IBOutlet weak var viewMobile : UIView!


    override func viewDidLoad() {
        super.viewDidLoad()
        if let tag = self.btn_tag{
            switch tag {
            case 0:
                self.navigationItem.title = "Registration" 
                break
            case 1:
                self.navigationItem.title = "Account"
                break
                
            case 2:
                self.navigationItem.title = "Order"
                break
            default:
                self.navigationItem.title = "Customer"
                break
            }
        }
        self.registerCell()
        self.setupUi()
        
    }
    func registerCell(){
       self.tablevView.register(UINib(nibName: "CNHelpSupportDetailCell", bundle: nil), forCellReuseIdentifier: "CNHelpSupportDetailCell")
    }
    func setupUi(){
        self.tablevView.estimatedRowHeight = 44.0
        self.tablevView.rowHeight = UITableView.automaticDimension
        self.viewMail.layer.cornerRadius = 5.0
        self.viewMail.layer.borderWidth = 1.0
        self.viewMail.layer.borderColor = Utill.Color.color_blue.cgColor
        self.viewMobile.layer.cornerRadius = 5.0
        self.viewMobile.layer.borderWidth = 1.0
        self.viewMobile.layer.borderColor = Utill.Color.color_blue.cgColor
    }
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_SupportEmailPopUpAction(_ sender: UIButton) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setSubject("Feedback")
            mail.setToRecipients(["support@circlenest.com"])
            mail.setMessageBody("Feature request or bug report?", isHTML: false)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    @IBAction func btn_SupportPhonePopUpAction(_ sender: UIButton) {
        if let url = URL(string: "tel://\(9910253377)") {
            UIApplication.shared.open(url)
        }
    }

}
extension CNHelpSupportdetailVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if btn_tag == 0 || btn_tag == 2{
            return 4
        
        }else if btn_tag == 1 {
            return 3
            
        }else{
           return 2
        }
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CNHelpSupportDetailCell", for: indexPath) as? CNHelpSupportDetailCell{
            if btn_tag == 0 {
                if indexPath.row == 0 {
                    cell.lblDesc.text = "You will be required to enter your personal information including your name,contact details,valid phone number while registering on the platform."
                    
                }else if indexPath.row == 1{
                    cell.lblDesc.text = "As a part of the registeration,you may be required to undertake a verification process to verify your personal information and setting up the user's account."
                    
                }else if indexPath.row == 2{
                    cell.lblDesc.text = "Company shall have to the right to display the information,feedback,ratings,reviews etc.provided by you on the platform."
                    
                }else{
                    cell.lblDesc.text = "You agree and accept that as on the date of your registration on the platform,the information provided by you is incomplete,accurate and up-to-date "
                    
                }
            }else if btn_tag == 1 {
                if indexPath.row == 0 {
                    cell.lblDesc.text = "In order to use the platform and avail the services,you will have to register on the platform and create an account with a aunique user identity and password"
                    
                }else if indexPath.row == 1{
                    cell.lblDesc.text = "You will be responsible for maintaining the confidentiality of the account information and are fully responsible for all activities that occur under your account"
                    
                }else {
                    cell.lblDesc.text = "you acknowledge and agree to immediately notify comapny at [support@circlenest.com] of any unauthorized use of your account information or any other breach of security.  "
                    
                }
            }else if btn_tag == 2 {
                if indexPath.row == 0 {
                    cell.lblDesc.text = "Order will be cancel if designated address to avail the services provided by you is outside the servicezone of Platform."
                    
                }else if indexPath.row == 1{
                    cell.lblDesc.text = "Order will be cancel if information,instructions and authorizations provided by you is incomplete or insufficient to execute the transaction initiated by you on the platform."
                    
                }else if indexPath.row == 2{
                    cell.lblDesc.text = "Order will be cancel if a delivery partner is not available to perform the delivery service,as may be requested."
                    
                }else{
                    cell.lblDesc.text = "Order will be cancel if the transaction can not be completed for a reason not in control of company."
                    
                }
            }else{
                if indexPath.row == 0 {
                    cell.lblDesc.text = "You are solely responsible for and in control of the information you provided to us.Compilation of accounts bearing contact number and e-mail addresses are owned by Company."
                    
                }else{
                    cell.lblDesc.text = "In the case where the platform is unable to establish unique identity of the user against a valid mobile number or e-mail address,the account shall be identifinitely suspended."
                    
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    
    
    

}

extension CNHelpSupportdetailVC: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case .cancelled:
            print("Mail cancelled")
        case .failed:
            print("Mail sent failure: \(error?.localizedDescription ?? "")")
        case .saved:
            print("Mail saved")
        case .sent:
            print("Mail sent")
        default:
            break
        }
        self.dismiss(animated: true, completion: nil)
    }
}
