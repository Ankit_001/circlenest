//
//  CNJoinPlanCollectionViewCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 15/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNJoinPlanCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblSubTitle : UILabel!
    @IBOutlet weak var lblDesc : UILabel!
    var indexPath : IndexPath!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func populateData(){
        if self.indexPath.item == 0{
            self.lblTitle.text = "Buy Circle Nest"
            self.lblSubTitle.text = "Membership Plan"
            self.lblDesc.text = "Choose your plan based on your need & enjoy"
            
        }else if self.indexPath.item == 1{
            self.lblTitle.text = "Get Back"
            self.lblSubTitle.text = "Redeemable Cash"
            self.lblDesc.text = "You will get cash for lifetime. 1 Cashpoint = 1 INR "
            
        }else{
            self.lblTitle.text = "Pay Your"
            self.lblSubTitle.text = "Daily Bills"
            self.lblDesc.text = "Use your redeemable cash to pay at our online & offline storage"
            
        }
        
    }

}
