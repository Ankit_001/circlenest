//
//  CNJoinCircleNestVC.swift
//  CircleNest
//
//  Created by Ankit Gupta on 15/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit


class CNJoinCircleNestVC: UIViewController {
    @IBOutlet weak var btnScheduleVisit : UIButton!
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var pageControl : UIPageControl!
    var timer = Timer()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.btnScheduleVisit.layer.cornerRadius = 5.0
        self.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.timer.invalidate()
        
    }
    func startTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.scrollCellHorizontally), userInfo: nil, repeats: true)

    }
    @IBAction func btnBackPressed(_sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    @objc func scrollCellHorizontally(){
        Utill.scrollNextCellUsingDots(collectionView: self.collectionView, pageControl: self.pageControl)
        
    }
    @IBAction func btnScheduleVisitPressed(_sender:Any){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "CNScheduleVisitVC") as? CNScheduleVisitVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
extension CNJoinCircleNestVC : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CNJoinPlanCollectionViewCell", for: indexPath) as? CNJoinPlanCollectionViewCell{
            cell.indexPath = indexPath
            cell.populateData()
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        return CGSize(width: width, height: 128)
    }

}
