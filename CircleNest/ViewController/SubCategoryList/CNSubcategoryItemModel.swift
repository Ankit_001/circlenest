//
//  CNSubcategoryItemModel.swift
//  CircleNest
//
//  Created by Ankit Gupta on 26/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNSubcategoryItemModel: Codable {
    public var responseCode : Int?
    public var data : [item_list]?
    public var msg : String?
}


public class item_list : Codable {
    public var id : DynamicDataType?
    public var storeId : DynamicDataType?
    public var name : DynamicDataType?
    public var brand : DynamicDataType?
    public var description : DynamicDataType?
    public var sku : DynamicDataType?
    public var price : DynamicDataType?
    public var weight : DynamicDataType?
    public var discount : DynamicDataType?
    //public var discount offers : String?
    public var stock : DynamicDataType?
    public var stock_qty : DynamicDataType?
    public var isAvailable : Bool?
    public var status : DynamicDataType?
    public var image : DynamicDataType?
    public var category : DynamicDataType?
//    public var variants : [DynamicDataType]?
   // public var best Selling Product offers : [String]?
}
