//
//  CNSubcategoryItemVM.swift
//  CircleNest
//
//  Created by Ankit Gupta on 26/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNSubcategoryItemVM: NSObject {
    func callItemListAPI (param:[String:Any],completionHandler: @escaping (_ success: Bool, _ message: String, _ response : CNSubcategoryItemModel?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    if let dict = json["msg"] as? [String:Any]{
                        let result = Utill.parseObject(CNSubcategoryItemModel.self, data: dict)
                        completionHandler(true,"",result)
                    }
                }
            case .failure :
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
                           
            }
        }
    }
    func callAddItemIntoCarttAPI (param:[String:Any],completionHandler: @escaping (_ success: Bool, _ message: String, _ response : [String:Any]?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    if let dict = json["msg"] as? [String:Any]{
                        completionHandler(true,"",dict)
                    }
                }
            case .failure :
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
                           
            }
        }
    }
    func removeItemFromCart (param:[String:Any],completionHandler: @escaping (_ success: Bool, _ message: String, _ response : [String:Any]?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    completionHandler(true,"",json)
                }
            case .failure :
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
                           
            }
        }
    }
}
