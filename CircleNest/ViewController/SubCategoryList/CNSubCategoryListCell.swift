//
//  CNSubCategoryListCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 01/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
protocol btnDelegate {
    func btnAddPressed(indexpath : IndexPath , cell : CNSubCategoryListCell)
    func btnPlusPressed(indexpath : IndexPath , cell : CNSubCategoryListCell)
    func btnMinusPressed(indexpath : IndexPath , cell : CNSubCategoryListCell)

}

class CNSubCategoryListCell: UITableViewCell {
    @IBOutlet weak var viewCard : UIView!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblWeight : UILabel!
    @IBOutlet weak var lblPrice : UILabel!
    @IBOutlet weak var imgProduct : UIImageView!
    @IBOutlet weak var btnWishList : UIButton!
    @IBOutlet weak var viewAdd : UIView!
    @IBOutlet weak var btnAdd : UIButton!
    @IBOutlet weak var viewCount : UIView!
    @IBOutlet weak var btnPlus : UIButton!
    @IBOutlet weak var btnMinus : UIButton!
    @IBOutlet weak var lblCount : UILabel!
    @IBOutlet weak var viewOutOfStock : UIView!
    
    var array_wishList : [Wishlist] = []
    var arrayCartDB : [CartItem] = []
    
    var delegate : btnDelegate?
    var indexPath : IndexPath!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        viewCard.layer.cornerRadius = 5.0
        viewCard.layer.masksToBounds = false
        viewCard.layer.shadowColor = UIColor.black.cgColor
        viewCard.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        viewCard.layer.shadowRadius = 5.0
        viewCard.layer.shadowOpacity = 0.3
        viewAdd.layer.cornerRadius = 3.0
        viewAdd.clipsToBounds = true
        viewOutOfStock.layer.cornerRadius = 3.0
        viewOutOfStock.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func populatedata(dict_item : item_list){
        self.lblName.text = dict_item.name?.type
        self.lblWeight.text = dict_item.weight?.type
        self.lblPrice.text  = "₹ \(dict_item.price?.type ?? "")"
        let image = dict_item.image?.type?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        self.imgProduct.sd_setImage(with: URL(string: image ?? ""), placeholderImage: UIImage(named: "Placeholder"))
        
        self.btnWishList.setImage(UIImage(named: "Wishlist"), for: UIControl.State.normal)
        
        for dict_wishlist in self.array_wishList{
            if dict_item.id?.type == dict_wishlist.id {
                self.btnWishList.setImage(UIImage(named: "wishlist_red"), for: UIControl.State.normal)
            }
        }

        self.viewCount.isHidden = true
        self.viewAdd.isHidden = false
        
        for dict in self.arrayCartDB{
            if dict_item.id?.type == dict.id {
                self.viewAdd.isHidden = true
                self.viewCount.isHidden = false
                self.lblCount.text = "\(dict.count)"
            }
        }

    }
    
    @IBAction func btnAddPressed(_sender:Any){
        self.delegate?.btnAddPressed(indexpath: indexPath, cell: self)
       
    }
    @IBAction func btnPlusPressed(_sender:Any){
        self.delegate?.btnPlusPressed(indexpath: indexPath, cell: self)
    }
    @IBAction func btnMinusPressed(_sender:Any){
        self.delegate?.btnMinusPressed(indexpath: indexPath, cell: self)
    }
    
}
