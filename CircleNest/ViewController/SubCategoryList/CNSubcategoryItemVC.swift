//
//  CNSubcategoryItemVC.swift
//  CircleNest
//
//  Created by Ankit Gupta on 01/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
import CoreData


class CNSubcategoryItemVC: CNBaseViewController {
    @IBOutlet weak var tableView : UITableView!
    var dict_subitem : sub_item!
    var array_item : [item_list] = []
    var array_wishList : [Wishlist] = []
    var arrayCartDB : [CartItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCell()
        self.tableView.estimatedRowHeight = 1000
        self.tableView.rowHeight = UITableView.automaticDimension
        self.callItemListAPI(_id: self.dict_subitem.id ?? "0")
        titleLabel.text = self.dict_subitem.name
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.arrayCartDB = CNCoreDataManager.sharedManager.getCartItemData()
        self.getWishListFromLocalDB()
        self.setItemCount()
    }
    
    func registerCell(){
        self.tableView.register(UINib(nibName: "CNSubCategoryListCell", bundle: nil), forCellReuseIdentifier: "CNSubCategoryListCell")
    }
    @IBAction func btnBackPressed(_sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    func getWishListFromLocalDB(){
        self.array_wishList = CNCoreDataManager.sharedManager.getWishListData()
        self.tableView.reloadData()
    }
    func callItemListAPI(_id : String){
        self.showHUD(progressLabel: "Loading")
        let vm = CNSubcategoryItemVM()
        let paramDict  = ["method" : "getProductList","storeId" : 101,"catId" : _id,"brandId": [],"subcategoryId":[],"unit":[]] as [String:AnyObject]
        vm.callItemListAPI(param: paramDict) { (success, message,response) in
            if success && message.isEmpty {
                self.dismissHUD(isAnimated: true)
                self.array_item = response?.data ?? []
                self.getWishListFromLocalDB()
            }
        }
    }
    @objc func btnWishListPressed(_sender:UIButton){
        let cat_id = self.dict_subitem.id ?? "0"
        let dict_item = self.array_item[_sender.tag]
        CNCoreDataManager.sharedManager.insertWishlistItemInLocalDB(dict_item: dict_item, cat_id: cat_id)
        self.getWishListFromLocalDB()
    }
    func addItemIntoCart(indexpath: IndexPath, quantity : Int , productId :String , method : String){
        self.showHUD(progressLabel: "Loading")
        let vm = CNSubcategoryItemVM()
        let user_id = UserDefaults.standard.string(forKey: "user_id")
        let paramDict  = ["method" : method,
                          "storeId" : 101,
                          "userId" : user_id ?? "0",
                          "productId":productId,
                          "qty":quantity] as [String:AnyObject]
        print(paramDict)
        
        vm.callAddItemIntoCarttAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.insertItemIntoDB(indexpath: indexpath, count: quantity)
            
            } else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    func removeItemFromCart(indexpath: IndexPath, quantity : Int , productId :String){
        self.showHUD(progressLabel: "Loading")
        let vm = CNSubcategoryItemVM()
        let user_id = UserDefaults.standard.string(forKey: "user_id")
        let paramDict  = ["method" : "removeCart",
                          "storeId" : 101,
                          "userId" : user_id ?? "0",
                          "productId":productId,
                          ] as [String:AnyObject]
        print(paramDict)
        
        vm.removeItemFromCart(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.insertItemIntoDB(indexpath: indexpath, count: quantity)
            
            } else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    func insertItemIntoDB(indexpath: IndexPath, count :Int){
        let dict_item = self.array_item[indexpath.row]
        CNCoreDataManager.sharedManager.checkItemPresence(dict_item: dict_item,item_count: count)
        self.arrayCartDB = CNCoreDataManager.sharedManager.getCartItemData()
    }
}
extension CNSubcategoryItemVC : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.array_item.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CNSubCategoryListCell", for: indexPath) as? CNSubCategoryListCell{
            cell.btnWishList.tag = indexPath.row
            cell.btnWishList .addTarget(self, action: #selector(CNSubcategoryItemVC.btnWishListPressed(_sender:)), for: UIControl.Event.touchUpInside)
            
            cell.delegate = self
            cell.indexPath = indexPath
            cell.arrayCartDB = self.arrayCartDB
            cell.array_wishList = self.array_wishList

            cell.selectionStyle = .none
            let dict_item = self.array_item[indexPath.row]
            if let stock_quan = dict_item.stock_qty?.type{
                if stock_quan == "0"{
                    cell.viewOutOfStock.isHidden = false
                }else{
                    cell.viewOutOfStock.isHidden = true
                }
            }
            
            cell.populatedata(dict_item: dict_item)

            return cell

        }else{
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(identifier: "CNProductDetailVC") as? CNProductDetailVC
        let dict_item = self.array_item[indexPath.row]
        let cat_id = self.dict_subitem.id ?? "0"
        vc?.product_name = dict_item.name?.type
        vc?.product_id = dict_item.id?.type
        vc?.cat_id = cat_id
        vc?.array_wishList = self.array_wishList
        vc?.stock_qty = dict_item.stock_qty?.type
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    

}
 
extension CNSubcategoryItemVC : btnDelegate{
    func btnAddPressed(indexpath: IndexPath, cell: CNSubCategoryListCell) {
        if let item_count = Int(cell.lblCount.text ?? "0"){
            let count = item_count + 1
            cell.lblCount.text = "\(count)"
            cell.viewAdd.isHidden = true
            cell.viewCount.isHidden = false
            self.countLabel.isHidden = false
            
            let count_cart = Int64(self.countLabel.text ?? "0") ?? 0
            let count_inc = count_cart+1
            self.countLabel.text = "\(count_inc)"
            
            let product_id = self.array_item[indexpath.row].id?.type
            
            DispatchQueue.main.async {
                self.addItemIntoCart(indexpath: indexpath, quantity: count, productId: product_id ?? "0", method: "addToCart")
            }
        }
    }
    
    func btnPlusPressed(indexpath: IndexPath, cell: CNSubCategoryListCell) {
        print(indexpath.row)
        if let item_count = Int(cell.lblCount.text ?? "0"){
            let count = item_count + 1
            cell.lblCount.text = "\(count)"
            self.countLabel.isHidden = false

            
            let count_cart = Int64(self.countLabel.text ?? "0") ?? 0
            let count_inc = count_cart+1
            self.countLabel.text = "\(count_inc)"

            DispatchQueue.main.async {
                let product_id = self.array_item[indexpath.row].id?.type
                self.addItemIntoCart(indexpath: indexpath, quantity: count, productId: product_id ?? "0", method: "updateCart")
            }
            
        }
    }
    
    func btnMinusPressed(indexpath: IndexPath, cell: CNSubCategoryListCell) {
        print(indexpath.row)
        
        if let item_count = Int(cell.lblCount.text ?? "0"){
            let count = item_count - 1
            cell.lblCount.text = "\(count)"
            
            let count_cart = Int64(self.countLabel.text ?? "0") ?? 0
            let count_inc = count_cart-1
            self.countLabel.text = "\(count_inc)"
            
            
            if count_inc == 0 {
                self.countLabel.isHidden = true
            }else{
                self.countLabel.isHidden = false
            }
            let product_id = self.array_item[indexpath.row].id?.type
            if count == 0{
                cell.viewAdd.isHidden = false
                cell.viewCount.isHidden = true
                DispatchQueue.main.async {
                    self.removeItemFromCart(indexpath: indexpath, quantity: count, productId: product_id ?? "0")
                }
                
            }else{
                cell.viewAdd.isHidden = true
                cell.viewCount.isHidden = false
                
                DispatchQueue.main.async {
                    self.addItemIntoCart(indexpath: indexpath, quantity: count, productId: product_id ?? "0", method: "updateCart")
                }
            }
        }

    }
}
