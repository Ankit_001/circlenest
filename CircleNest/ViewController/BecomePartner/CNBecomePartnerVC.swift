//
//  CNBecomePartnerVC.swift
//  CircleNest
//
//  Created by Ankit Gupta on 14/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNBecomePartnerVC: UIViewController {
    @IBOutlet weak var viewButton : UIView!
    @IBOutlet weak var btnPartner : UIButton!
    @IBOutlet weak var btnEntreprnr : UIButton!
    @IBOutlet weak var lblPartner : UILabel!
    @IBOutlet weak var lblEntreprnr : UILabel!
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblOption_One : UILabel!
    @IBOutlet weak var lblOption_Two : UILabel!
    @IBOutlet weak var lblOption_Three : UILabel!
    @IBOutlet weak var lblMessage : UILabel!
    @IBOutlet weak var lblDesc : UILabel!
    @IBOutlet weak var btnBecomePartner : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewButton.layer.cornerRadius = 5.0
        self.viewButton.clipsToBounds = true
        self.viewButton.layer.borderColor = UIColor.lightGray.cgColor
        self.viewButton.layer.borderWidth = 1.0
        self.btnBecomePartner.layer.cornerRadius = 5.0
        self.btnBecomePartner.clipsToBounds = true

        self.btnPartner .setTitleColor(UIColor(red: 256.0/255.0, green: 133.0/255.0, blue: 92.0/255.0, alpha: 1), for: UIControl.State.normal)
        self.lblPartner.isHidden = false
        self.btnEntreprnr .setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        self.lblEntreprnr.isHidden = true
        self.lblTitle.text = "Double your business and increase your saving"
        self.lblOption_One.text = "Better Sales"
        self.lblOption_Two.text = "Better Procurement Pricing"
        self.lblOption_Three.text = "Readymade Demand"
        self.lblMessage.text = "You have a Running Business"
        self.lblDesc.text = "Partner With Us"
        self.btnBecomePartner.addTarget(self, action: #selector(actionBecomeAPartner), for: .touchUpInside)
        
    }
    @IBAction func btnPartnerPressed(_sender:Any){
        self.btnPartner .setTitleColor(UIColor(red: 256.0/255.0, green: 133.0/255.0, blue: 92.0/255.0, alpha: 1), for: UIControl.State.normal)
        self.lblPartner.isHidden = false
        self.btnEntreprnr .setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        self.lblEntreprnr.isHidden = true
        self.lblTitle.text = "Double your business and increase your saving"
        self.lblOption_One.text = "Better Sales"
        self.lblOption_Two.text = "Better Procurement Pricing"
        self.lblOption_Three.text = "Readymade Demand"
        self.lblMessage.text = "You have a Running Business"
        self.lblDesc.text = "Partner With Us"
        self.btnBecomePartner.setTitle("BECOME A PARTNER", for: .normal)
    }
    @IBAction func btnEntreprnrPressed(_sender:Any){
       self.btnEntreprnr .setTitleColor(UIColor(red: 256.0/255.0, green: 133.0/255.0, blue: 92.0/255.0, alpha: 1), for: UIControl.State.normal)
        self.lblPartner.isHidden = true
        self.btnPartner .setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        self.lblEntreprnr.isHidden = false
        self.lblTitle.text = "Become an enterpreneur with satble income every month"
        self.lblOption_One.text = "Growth Opportunity"
        self.lblOption_Two.text = "Better Earnings & Savings"
        self.lblOption_Three.text = "Risk Free, Recession Free"
        self.lblMessage.text = "You have a Passion to do something Amazing"
        self.lblDesc.text = "We have a Platform for You"
        self.btnBecomePartner.setTitle("BECOME AN ENTREPRENEUR", for: .normal)
    }
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func actionBecomeAPartner(sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "CNScheduleVisitVC") as? CNScheduleVisitVC
        if sender.currentTitle == "BECOME AN ENTREPRENEUR" {
            vc?.partnerCategory = "ENTREPRENEUR"
        } else {
            vc?.partnerCategory = "PARTNER"
        }
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
