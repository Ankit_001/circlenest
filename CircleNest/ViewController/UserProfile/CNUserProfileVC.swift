//
//  CNUserProfileVC.swift
//  CircleNest
//
//  Created by techjini on 25/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNUserProfileVC: UIViewController {

    @IBOutlet weak var tableViewProfile: UITableView!
    var userName: String?
    var userPhoneNo: String?
    var popController: CNPPopupController?
    var addressValue: String? = ",,,\nPincode -"
    var memberCount: String?
    var partnerCount: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCell()
        self.callUserProfileAPI()
        self.callAddressSaleAPI()
        self.callGetTotalNumberMemberAPI()
        self.callGetTotalNumberPartnerAPI()

    }
    
    func registerCell(){
        self.tableViewProfile.register(UINib(nibName: "CNProfileHeaderCell", bundle: nil), forCellReuseIdentifier: "CNProfileHeaderCell")
        self.tableViewProfile.register(UINib(nibName: "CNCommunityCollectionCell", bundle: nil), forCellReuseIdentifier: "CNCommunityCollectionCell")
        self.tableViewProfile.register(UINib(nibName: "CNProfileOtherOptionCell", bundle: nil), forCellReuseIdentifier: "CNProfileOtherOptionCell")
    }
    
    func pushToOtherOptions(_ storyBoardName: String, controllerIdentifier: String, selectedOption: ProfileOtherOption) {
        let storyBoard = UIStoryboard(name:storyBoardName, bundle:nil)
        switch selectedOption {
        case .myorder:
            let controllerName = storyBoard.instantiateViewController(withIdentifier: controllerIdentifier) as! UITabBarController
            controllerName.selectedIndex = 3
            controllerName.hidesBottomBarWhenPushed = true
            self.navigationController?.navigationBar.isHidden = true
            self.navigationController?.pushViewController(controllerName, animated: true)
        default:
            let controllerName = storyBoard.instantiateViewController(withIdentifier: controllerIdentifier)
            controllerName.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(controllerName, animated: true)
        }
        
    }
    
    func callUserProfileAPI(){
        self.showHUD(progressLabel: "Loading")
        let vm = CNUserProfileVM()
        var paramDict  = ["method" : "getuserprofile","storeId" : 101] as [String:AnyObject]
        let userDefaults = UserDefaults.standard
        if let userId = userDefaults.value(forKey: "user_id") {
            paramDict.updateValue(userId as AnyObject, forKey: "userId")
        }
        vm.callUserProfileAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.userName = result?.data?.first?.name
                self.userPhoneNo = result?.data?.first?.phone
                self.tableViewProfile.reloadData()
            } else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    
    func callGetTotalNumberMemberAPI(){
        self.showHUD(progressLabel: "Loading")
        let vm = CNUserProfileVM()
        let paramDict  = ["method" : "getTotalNumberMember","storeId" : 101] as [String:AnyObject]
        vm.callGetTotalNumberMemberAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.partnerCount = result
                self.tableViewProfile.reloadData()
            } else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    
    func callGetTotalNumberPartnerAPI(){
        self.showHUD(progressLabel: "Loading")
        let vm = CNUserProfileVM()
        let paramDict  = ["method" : "getTotalNumberPartner","storeId" : 101] as [String:AnyObject]
        vm.callGetTotalNumberPartnerAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.memberCount = result
                self.tableViewProfile.reloadData()
            } else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    
    func callUserProfileUpdateAPI(userName: String, userPhoneNumber: String) {
        self.showHUD(progressLabel: "Loading")
        let vm = CNUserProfileVM()
        var paramDict  = ["method" : "updateProfile","storeId" : 101,"name":userName,"email":"","phone":userPhoneNumber] as [String:AnyObject]
        let userDefaults = UserDefaults.standard
        if let userId = userDefaults.value(forKey: "user_id") {
            paramDict.updateValue(userId as AnyObject, forKey: "userId")
        }
        vm.callUserUpdateProfileAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.popController?.dismiss(animated: true)
                self.userName = result?.data?.name
                self.userPhoneNo = result?.data?.phone
                let userDefaults = UserDefaults.standard
                userDefaults.set(result?.data?.name, forKey: "user_name")
                userDefaults.set(result?.data?.phone, forKey: "user_no")
                userDefaults.synchronize()
                self.tableViewProfile.reloadData()
            } else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    
    func callAddressSaleAPI() {
        self.showHUD(progressLabel: "Loading")
        let vm = CNUserProfileVM()
        var paramDict  = ["method" : "getAddress","storeId" : 101] as [String:AnyObject]
        let userDefaults = UserDefaults.standard
        if let userId = userDefaults.value(forKey: "user_id") {
            paramDict.updateValue(userId as AnyObject, forKey: "userId")
        }
        vm.callGetAddressAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.addressValue = "\(result?.house_no ?? ""),\(result?.city ?? ""),\(result?.state ?? ""),\nPincode - \(result?.pincode ?? "")"
                self.tableViewProfile.reloadData()
            } else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension CNUserProfileVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNProfileHeaderCell", for: indexPath) as? CNProfileHeaderCell {
                cell.lblUserName.text = userName
                cell.lblUserPhoneNumber.text = userPhoneNo
                let userDefaults = UserDefaults.standard
                if let userGender = userDefaults.value(forKey: "user_gender") as? String {
                    if userGender.lowercased() == "Male".lowercased() {
                        cell.ivUserGenderProfileIcon.image = UIImage(named: "men.png")
                    } else {
                        cell.ivUserGenderProfileIcon.image = UIImage(named: "women.png")
                    }
                }
                cell.delegate = self
                return cell
            }else{
                return UITableViewCell()
            }
            
        } else if indexPath.row == 1 {
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNCommunityCollectionCell", for: indexPath) as? CNCommunityCollectionCell {
                cell.memberCount = self.memberCount
                cell.partnerCount = self.partnerCount
                cell.cvcCommunityCollectionCell.reloadData()
                return cell
            }else{
                return UITableViewCell()
            }
            
        } else {
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNProfileOtherOptionCell", for: indexPath) as? CNProfileOtherOptionCell {
                cell.delegate = self
                cell.addressValue = self.addressValue
                return cell
            }else{
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension CNUserProfileVC: ProfileOtherOptionSelectionDelegate {
    func profileOtherSelectedOption(selectedOption: ProfileOtherOption) {
        switch selectedOption {
        case .wallet:
            self.pushToOtherOptions("Main", controllerIdentifier: "My Wallet", selectedOption: .wallet)
        case .myorder:
            self.pushToOtherOptions("Main", controllerIdentifier: "HomeTabBar", selectedOption: .myorder)
        case .myaddress:
            self.tableViewProfile.reloadData()
        case.logout:
            Utill.showAlertWithLogout(title: "CircleNest", Message: "Are you Sure? You want to logout this app")
        }
    }
}

extension CNUserProfileVC: profileEditDelegate {
    func editDelegateCalled() {
        let rectShape = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 50.0, height: 459.0)
        let view = CNEditProfilePopup(frame: rectShape)
        view.delegate = self
        let popupController = CNPPopupController(contents: [view])
        self.popController = popupController
        self.popController?.theme = CNPPopupTheme.default()
        self.popController?.theme.popupStyle = .centered
        self.popController?.delegate = self
        popupController.present(animated: true)
    }
}

extension CNUserProfileVC: editProfilePopUpDelegate {
    func updateTheProfile(userName: String?, userMobile: String?) {
        
        if !(userName?.isValidField ?? false) {
            self.view.makeToast(ConstantValue.nameAlertMessage.rawValue)
        } else if !(userMobile?.isValidPhoneNumberMBL ?? false){
            self.view.makeToast(ConstantValue.mobileAlertMessage.rawValue)
        } else {
            self.callUserProfileUpdateAPI(userName: userName ?? "", userPhoneNumber: userMobile ?? "")
        }
    }
    
    func cancelTheProfile() {
        print("Cancel")
        self.popController?.dismiss(animated: true)
    }
    
    
}

extension CNUserProfileVC: CNPPopupControllerDelegate {
    
}

