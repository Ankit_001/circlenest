//
//  cvcCommunityCollectionCell.swift
//  CircleNest
//
//  Created by techjini on 26/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNCVCCommunityCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitleCOmmuninty: UILabel!
    @IBOutlet weak var lblCommunityCount: UILabel!
    @IBOutlet weak var vwCommunityContent: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
