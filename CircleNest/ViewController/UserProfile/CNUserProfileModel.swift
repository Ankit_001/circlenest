//
//  CNUserProfileModel.swift
//  CircleNest
//
//  Created by techjini on 26/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import Foundation

public class CNUserProfileModel : Codable {
    public var responseCode : Int?
    public var data : [userData]?
    public var msg : String?
    public var userVerification : String?
    
    private enum CodingKeys : String, CodingKey {
        case responseCode = "responseCode", data = "data", msg = "msg", userVerification = "userVerification"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let responseCode = try container.decodeIfPresent(Int.self, forKey: .responseCode) {
            self.responseCode = responseCode
        }
        
        if let msg = try container.decodeIfPresent(String.self, forKey: .msg) {
            self.msg = msg
        }
        if let data = try container.decodeIfPresent([userData].self, forKey: .data) {
            self.data = data
        }
        
        if let userVerification = try container.decodeIfPresent(String.self, forKey: .userVerification) {
            self.userVerification = userVerification
        }
    }
}

public class CNUserUpdateProfileModel : Codable {
    public var responseCode : Int?
    public var data : userData?
    public var msg : String?
    public var userVerification : String?
    
    private enum CodingKeys : String, CodingKey {
        case responseCode = "responseCode", data = "data", msg = "msg", userVerification = "userVerification"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let responseCode = try container.decodeIfPresent(Int.self, forKey: .responseCode) {
            self.responseCode = responseCode
        }
        
        if let msg = try container.decodeIfPresent(String.self, forKey: .msg) {
            self.msg = msg
        }
        if let data = try container.decodeIfPresent(userData.self, forKey: .data) {
            self.data = data
        }
        
        if let userVerification = try container.decodeIfPresent(String.self, forKey: .userVerification) {
            self.userVerification = userVerification
        }
    }
}

public class CNUserAddressModel : Codable {
    public var address : String?
    public var city : String?
    public var id : String?
    public var house_no : String?
    public var pincode : String?
    public var state : String?
    
    private enum CodingKeys : String, CodingKey {
        case address = "address", city = "city", id = "id", house_no = "house_no", state = "state", pincode = "pincode"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let address = try container.decodeIfPresent(String.self, forKey: .address) {
            self.address = address
        }
        
        if let city = try container.decodeIfPresent(String.self, forKey: .city) {
            self.city = city
        }
        if let id = try container.decodeIfPresent(String.self, forKey: .id) {
            self.id = id
        }
        
        if let house_no = try container.decodeIfPresent(String.self, forKey: .house_no) {
            self.house_no = house_no
        }
        
        if let pincode = try container.decodeIfPresent(String.self, forKey: .pincode) {
            self.pincode = pincode
        }
        
        if let state = try container.decodeIfPresent(String.self, forKey: .state) {
            self.state = state
        }
    }

}
