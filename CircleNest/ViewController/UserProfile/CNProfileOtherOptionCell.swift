//
//  profileOtherOptionCell.swift
//  CircleNest
//
//  Created by techjini on 25/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

enum ProfileOtherOption {
    case wallet
    case myorder
    case myaddress
    case logout
}

enum addressCollapsed {
    case yes
    case no
}

protocol ProfileOtherOptionSelectionDelegate {
    func profileOtherSelectedOption(selectedOption: ProfileOtherOption)
}

class CNProfileOtherOptionCell: UITableViewCell {
    
    @IBOutlet weak var lblAddressDetails: UILabel!
    @IBOutlet weak var cnstAddressHeight: NSLayoutConstraint!
    var delegate: ProfileOtherOptionSelectionDelegate?
    var addressCollapsedVal: addressCollapsed? = .yes
    var addressValue: String? = ""

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btnWalletAction(_ sender: UIButton) {
        delegate?.profileOtherSelectedOption(selectedOption: .wallet)
    }
    
    @IBAction func btnOrderAction(_ sender: UIButton) {
        delegate?.profileOtherSelectedOption(selectedOption: .myorder)
    }
    
    @IBAction func btnMyAddressSection(_ sender: UIButton) {
        if addressCollapsedVal == .yes {
            addressCollapsedVal = .no
            cnstAddressHeight.constant = 150.0
            lblAddressDetails.text = addressValue
        } else {
            addressCollapsedVal = .yes
            cnstAddressHeight.constant = 100.0
            lblAddressDetails.text = ""
        }
        delegate?.profileOtherSelectedOption(selectedOption: .myaddress)
    }
    
    @IBAction func btnLogoutAction(_ sender: UIButton) {
        delegate?.profileOtherSelectedOption(selectedOption: .logout)
    }
}
