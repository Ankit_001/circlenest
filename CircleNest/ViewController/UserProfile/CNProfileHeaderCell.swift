//
//  profileHeaderCell.swift
//  CircleNest
//
//  Created by techjini on 25/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
protocol profileEditDelegate {
    func editDelegateCalled()
}

class CNProfileHeaderCell: UITableViewCell {
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var ivUserGenderProfileIcon: UIImageView!
    @IBOutlet weak var lblUserPhoneNumber: UILabel!
    @IBOutlet weak var btnEditOutlet: UIButton!
    var delegate: profileEditDelegate?
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btnEditAction(_ sender: UIButton) {
        delegate?.editDelegateCalled()
    }
}
