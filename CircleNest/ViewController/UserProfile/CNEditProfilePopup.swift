//
//  CNEditProfilePopup.swift
//  CircleNest
//
//  Created by techjini on 30/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

protocol editProfilePopUpDelegate {
    func updateTheProfile(userName: String?, userMobile: String?)
    func cancelTheProfile()
}

class CNEditProfilePopup: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var tfUserMobileNumber: UITextField!
    var delegate: editProfilePopUpDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("CNEditProfilePopup", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    @IBAction func btnUpdateAction(_ sender: UIButton) {
        delegate?.updateTheProfile(userName: tfUserName.text, userMobile: tfUserMobileNumber.text)
    }
    
    @IBAction func btnCancelAction(_ sender: UIButton) {
        delegate?.cancelTheProfile()
    }
    
}
