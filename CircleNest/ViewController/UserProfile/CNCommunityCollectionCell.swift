//
//  communityCollectionCell.swift
//  CircleNest
//
//  Created by techjini on 25/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNCommunityCollectionCell: UITableViewCell {

    @IBOutlet weak var cvcCommunityCollectionCell: UICollectionView!
    var memberCount: String?
    var partnerCount: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cvcCommunityCollectionCell.register(UINib(nibName: "CNCVCCommunityCollectionCell", bundle: nil), forCellWithReuseIdentifier: "CNCVCCommunityCollectionCell")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension CNCommunityCollectionCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CNCVCCommunityCollectionCell", for: indexPath) as? CNCVCCommunityCollectionCell {
            if indexPath.item == 0 {
                cell.lblTitleCOmmuninty.text = "Total Sales"
                cell.lblCommunityCount.text = "0"
                let userDefaults = UserDefaults.standard
                if let totalSalesCount = userDefaults.value(forKey: "totalSalesCount") as? String {
                    cell.lblCommunityCount.text = totalSalesCount
                }
                cell.vwCommunityContent.backgroundColor = UIColor(red: 126.0/255.0, green: 118.0/255.0, blue: 247.0/255.0, alpha: 1.0)
            } else if indexPath.item == 1 {
                cell.lblTitleCOmmuninty.text = "Total Member"
                cell.lblCommunityCount.text = memberCount ?? ""
                cell.vwCommunityContent.backgroundColor = UIColor(red: 237.0/255.0, green: 156.0/255.0, blue: 62.0/255.0, alpha: 1.0)
            } else {
                cell.lblTitleCOmmuninty.text = "Total Partner"
                cell.lblCommunityCount.text = partnerCount ?? ""
                cell.vwCommunityContent.backgroundColor = UIColor(red: 126.0/255.0, green: 118.0/255.0, blue: 247.0/255.0, alpha: 1.0)
            }
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/2 - 10, height: 84.0)
    }
    
}
