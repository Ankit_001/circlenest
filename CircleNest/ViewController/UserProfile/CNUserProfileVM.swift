//
//  CNUserProfileVM.swift
//  CircleNest
//
//  Created by techjini on 26/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import Foundation

class CNUserProfileVM: NSObject {
    func callUserProfileAPI(param:[String:Any],completionHandler: @escaping (_ success: Bool, _ responseMessage : String?, _ response: CNUserProfileModel?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    if let responseCode = json["responseCode"] as? Int, let responseMessage = json["msg"] as? String {
                        let result = Utill.parseObject(CNUserProfileModel.self, data: json)
                        completionHandler("\(responseCode)".bool, responseMessage, result)
                    } else {
                        completionHandler(false,"",nil)
                    }
                }
            case .failure :
                completionHandler(false,"",nil)
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
            }
        }
    }
    
    func callUserUpdateProfileAPI(param:[String:Any],completionHandler: @escaping (_ success: Bool, _ responseMessage : String?, _ response: CNUserUpdateProfileModel?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    if let response = json["msg"] as? [String:Any],let responseCode = response["responseCode"] as? Int {
                        let result = Utill.parseObject(CNUserUpdateProfileModel.self, data: response)
                        completionHandler("\(responseCode)".bool, "", result)
                    } else {
                        completionHandler(false,"",nil)
                    }
                }
            case .failure :
                completionHandler(false,"",nil)
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
            }
        }
    }
    
    func callGetAddressAPI(param:[String:Any],completionHandler: @escaping (_ success: Bool, _ responseMessage : String?, _ response: CNUserAddressModel?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    if let responseMessage = json["msg"] as? [String: Any] {
                        let result = Utill.parseObject(CNUserAddressModel.self, data: responseMessage)
                        completionHandler(true, "Success", result)
                    } else {
                        completionHandler(false,"",nil)
                    }
                }
            case .failure :
                completionHandler(false,"",nil)
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
                
                
            }
        }
    }
    
    func callGetSalesAPI(param:[String:Any],completionHandler: @escaping (_ success: Bool, _ totalSalesCount: String?) -> Void) {
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    if let responseMessage = json["msg"] as? [String: Any], let responseCode = responseMessage["responseCode"] as? Int, let data = responseMessage["data"] as? [String: Any], let totalSalesCount = data["total_sales"] as? Int {
                        completionHandler("\(responseCode)".bool, "\(totalSalesCount)")
                    } else {
                        completionHandler(false,nil)
                    }
                }
            case .failure :
                completionHandler(false,nil)
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
            }
        }
    }
    
    func callGetTotalNumberMemberAPI(param:[String:Any],completionHandler: @escaping (_ success: Bool, _ responseMessage : String?, _ response: String?) -> Void) {
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    if let responseMessage = json["msg"] as? [String: Any], let responseCode = responseMessage["responseCode"] as? Int, let data = responseMessage["data"] as? [String: Any], let totalMembersCount = data["number"] as? String {
                        completionHandler("\(responseCode)".bool, "", "\(totalMembersCount)")
                    } else {
                        completionHandler(false,"",nil)
                    }
                }
            case .failure :
                completionHandler(false,"",nil)
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
            }
        }
    }
    
    func callGetTotalNumberPartnerAPI(param:[String:Any],completionHandler: @escaping (_ success: Bool, _ responseMessage : String?, _ response: String?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    if let responseMessage = json["msg"] as? [String: Any], let responseCode = responseMessage["responseCode"] as? Int, let data = responseMessage["data"] as? [String: Any], let totalPartnerCount = data["number"] as? String {
                        completionHandler("\(responseCode)".bool, "", "\(totalPartnerCount)")
                    } else {
                        completionHandler(false,"",nil)
                    }
                }
            case .failure :
                completionHandler(false,"",nil)
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
            }
        }
    }
}
