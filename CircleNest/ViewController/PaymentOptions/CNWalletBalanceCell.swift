//
//  CNWalletBalanceCell.swift
//  CircleNest
//
//  Created by techjini on 08/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
protocol paidByWalletDelegate {
    func paidByWallet(isWalletSelected: Bool?, emptyWallet: Bool)
}

class CNWalletBalanceCell: UITableViewCell {
    
    @IBOutlet weak var lblWalletCodeValue: UILabel!
    @IBOutlet weak var btnWalletBalanceOutlet: UIButton!
    var delegate: paidByWalletDelegate?
    var wallet_Balance: Double = 0.0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnWalletBalanceCheckmark(_ sender: UIButton) {
        if wallet_Balance == 0.0 {
            delegate?.paidByWallet(isWalletSelected: false, emptyWallet: true)
        } else {
            if sender.currentImage == UIImage(named: "checkmark.png") {
                sender.setImage(UIImage(), for: .normal)
                delegate?.paidByWallet(isWalletSelected: false, emptyWallet: false)
            } else {
                sender.setImage(UIImage(named: "checkmark.png"), for: .normal)
                delegate?.paidByWallet(isWalletSelected: true, emptyWallet: false)
            }
        }
    }
    
}
