//
//  CNUserCashbackModel.swift
//  CircleNest
//
//  Created by techjini on 15/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import Foundation

public class CNUserCashbackModel : Codable {
    public var responseCode : DynamicDataType?
    public var msg : DynamicDataType?
    public var couponAmount : DynamicDataType?
    public var couponName : DynamicDataType?
    public var couponType : DynamicDataType?
    public var couponTypeValue : DynamicDataType?
    
    private enum CodingKeys : String, CodingKey {
        case responseCode = "responseCode", msg = "msg", couponAmount = "couponAmount", couponName = "couponName", couponType = "couponType", couponTypeValue = "couponTypeValue"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let responseCode = try container.decodeIfPresent(DynamicDataType.self, forKey: .responseCode) {
            self.responseCode = responseCode
        }
        
        if let msg = try container.decodeIfPresent(DynamicDataType.self, forKey: .msg) {
            self.msg = msg
        }
        
        if let couponAmount = try container.decodeIfPresent(DynamicDataType.self, forKey: .couponAmount) {
            self.couponAmount = couponAmount
        }
        
        if let couponName = try container.decodeIfPresent(DynamicDataType.self, forKey: .couponName) {
            self.couponName = couponName
        }
        
        if let couponType = try container.decodeIfPresent(DynamicDataType.self, forKey: .couponType) {
            self.couponType = couponType
        }
        
        if let couponTypeValue = try container.decodeIfPresent(DynamicDataType.self, forKey: .couponTypeValue) {
            self.couponTypeValue = couponTypeValue
        }
    }
}
