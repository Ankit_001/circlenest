
//
//  paidByPopUp.swift
//  CircleNest
//
//  Created by techjini on 08/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

protocol CNPaidByPopupDelegate {
    func popUpCancelAction()
    func popUpDismissAction()
    func popUpPlaceOrderAction()
}

enum paidByPopUp {
    case COD
    case Wallet
}

class CNPaidByPopUp: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var popUpTitle: UILabel!
    @IBOutlet weak var popUpSubtitle: UILabel!
    @IBOutlet weak var popUpImage: UIImageView!
    @IBOutlet weak var btnPlaceOrderOutlet: UIButton!
    @IBOutlet weak var btnCancelOutlet: UIButton!
    var delegate: CNPaidByPopupDelegate?
    var popupValue: paidByPopUp?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("CNPaidByPopUp", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func settingOfPopUp() {
        if popupValue == paidByPopUp.COD {
            popUpTitle.text = "CASH ON DELIVERY"
            popUpSubtitle.text = "Please pay amount to the delivery executive"
            popUpImage.image = UIImage(named: "cashondelivery.png")
        } else {
            popUpTitle.text = "PAY BY WALLET"
            popUpSubtitle.text = "Amount will be deducted from your wallet"
            popUpImage.image = UIImage(named: "walletscreenicon.png")
        }
    }
    
    @IBAction func btnDismissPopUp(_ sender: UIButton) {
        delegate?.popUpDismissAction()
    }
    
    @IBAction func btnCancelAction(_ sender: UIButton) {
        delegate?.popUpCancelAction()
    }
    
    @IBAction func btnPlaceOrderAction(_ sender: UIButton) {
        delegate?.popUpPlaceOrderAction()
    }
}
