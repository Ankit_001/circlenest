//
//  CNPaymentOptionsVC.swift
//  CircleNest
//
//  Created by techjini on 08/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNPaymentOptionsVC: UIViewController {
    
    @IBOutlet weak var tblPaymentOptions: UITableView!
    var payingByWallet: Bool?
    var promoCodeApplied: Bool = false
    var popController: CNPPopupController?
    var walletBalance: String? = "₹ 0.0"
    var wallet_Balance: Double = 0.0
    var order_Amount: Double = 0.0
    var coupon_Amount: Double = 0.0
    var array_main : [CategoryData] = []
    
    var isCouponApplied: String? = "0"
    var isPaidByWalletOrCod: String? = "1"
    var couponName: String? = ""
    var couponAmount: String? = ""
    var address: String? = ""
    var pincode: String? = ""
    var state: String? = ""
    var city: String? = ""
    var delivery_Date: String? = ""
    var order_id : Int = 0


    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.getTotalWalletCashback()
        print(self.order_Amount)
    }
    
    func setupUI() {
        self.tblPaymentOptions.register(UINib(nibName: "CNAvailaibleOfferCell", bundle: nil), forCellReuseIdentifier: "CNAvailaibleOfferCell")
        self.tblPaymentOptions.register(UINib(nibName: "CNApplyPromoCodeCell", bundle: nil), forCellReuseIdentifier: "CNApplyPromoCodeCell")
        self.tblPaymentOptions.register(UINib(nibName: "CNAmountPayableCell", bundle: nil), forCellReuseIdentifier: "CNAmountPayableCell")
        self.tblPaymentOptions.register(UINib(nibName: "CNWalletBalanceCell", bundle: nil), forCellReuseIdentifier: "CNWalletBalanceCell")
        self.tblPaymentOptions.register(UINib(nibName: "CNWalletImageCell", bundle: nil), forCellReuseIdentifier: "CNWalletImageCell")
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnConfirmOrder(_ sender: UIButton) {
        self.confirmOrderPopUp()
    }
    
    func getTotalWalletCashback() {
        self.showHUD(progressLabel: "Loading")
        let vm = CNPaymentOptionsVM()
        var paramDict  = ["method" : "getTotalWalletCashback"] as [String:AnyObject]
        let userDefaults = UserDefaults.standard
        if let userId = userDefaults.value(forKey: "user_id") {
            paramDict.updateValue(userId as AnyObject, forKey: "userId")
        }
        vm.callGetTotalWalletCashbackAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                if result?.data?.count ?? 0 > 0 {
                    self.walletBalance = "₹ \(result?.data?.first?.amount?.type ?? "")"
                    self.wallet_Balance = Double(result?.data?.first?.amount?.type ?? "0.00") ?? 0.00
                }
                self.tblPaymentOptions.reloadData()
            }
        }
    }
    
    func placeOrderApi(params: [String: Any],array_item : [[String:Any]]) {
        self.popController?.dismiss(animated: false)
        self.showHUD(progressLabel: "Loading")
        let vm = CNPaymentOptionsVM()
        vm.callPlaceOrderAPI(param: params) { (success , responseMessage, result) in
            
            if success {
                self.order_id = result ?? 0
                self.clearAllCartApi(array_item: array_item)
                
            }else{
                self.view.makeToast("Something went wrong.Please try again later")
            }
        }
    }
    func clearAllCartApi(array_item : [[String:Any]]) {
        let user_id  = UserDefaults.standard.string(forKey: "user_id") ?? ""
        let params  = ["method" : "removeAllCart",
                          "userId":user_id,
        "storeId":101] as [String:Any]
        
        let vm = CNPaymentOptionsVM()
        vm.callClearCartAPI(param: params) { (success , responseMessage) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.removeCartItemFromDB(array_item: array_item)
                let storyboard = UIStoryboard(name: "CircleNest", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "CNOrderPlacedSuccessVC") as? CNOrderPlacedSuccessVC
                vc?.orderId = self.order_id
                self.navigationController?.pushViewController(vc!, animated: true)
                
            }else{
                self.view.makeToast("Something went wrong.Please try again later")
            }
        }
    }
    func removeCartItemFromDB(array_item : [[String:Any]]){
        for i in 0..<array_item.count{
            if let dict = array_item[i] as? [String:Any]{
                let product_id = dict["product_id"] as? String
                CNCoreDataManager.sharedManager.deleteItemFromCart(product_id: product_id ?? "")
            }
        }
    }
  
    func getApplyPromoCodeAPI(senderButton: UIButton, lblPromoCodeValue: UILabel, lblPromoDescriptionLabel: UILabel) {
        self.showHUD(progressLabel: "Loading")
        let vm = CNPaymentOptionsVM()
        var paramDict  = ["method" : "calculateCoupon", "couponCode":"NEST", "storeId":101] as [String:AnyObject]
        let userDefaults = UserDefaults.standard
        if let userId = userDefaults.value(forKey: "user_id") {
            paramDict.updateValue(userId as AnyObject, forKey: "userId")
        }
        vm.callCouponCodeAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.coupon_Amount = Double(result?.couponAmount?.type ?? "0.0") ?? 0.0
                self.couponName = result?.couponName?.type
                self.couponAmount = result?.couponAmount?.type ?? ""
                self.order_Amount = self.order_Amount - self.coupon_Amount
                lblPromoCodeValue.text = "₹ \(String(format: "%.2f", self.coupon_Amount))"
                lblPromoDescriptionLabel.text = "Congrats on your savings!"
                self.tblPaymentOptions.reloadData()
            } else {
                self.couponName = ""
                self.couponAmount = ""
                lblPromoCodeValue.text = ""
                lblPromoDescriptionLabel.text = ""
                senderButton.setImage(UIImage(), for: .normal)
                self.view.makeToast(result?.msg?.type)
            }
        }
    }
    
    func confirmOrderPopUp() {
        let rectShape = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        let view = CNPaidByPopUp(frame: rectShape)
        view.delegate = self
        if self.payingByWallet ?? false {
            view.popupValue = paidByPopUp.Wallet
        } else {
            view.popupValue = paidByPopUp.COD
        }
        view.settingOfPopUp()
        let popupController = CNPPopupController(contents: [view])
        self.popController = popupController
        self.popController?.theme = CNPPopupTheme.default()
        self.popController?.theme.popupStyle = .centered
        self.popController?.delegate = self
        popupController.present(animated: false)
    }
}

extension CNPaymentOptionsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNAvailaibleOfferCell", for: indexPath) as? CNAvailaibleOfferCell {
                return cell
            }else{
                return UITableViewCell()
            }
        } else if indexPath.row == 1 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNApplyPromoCodeCell", for: indexPath) as? CNApplyPromoCodeCell {
                cell.delegate = self
                cell.coupon_Amount = self.coupon_Amount
                return cell
            }else{
                return UITableViewCell()
            }
        } else if indexPath.row == 2 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNAmountPayableCell", for: indexPath) as? CNAmountPayableCell {
                cell.lblAmountPayableValue.text = "₹ \(String(format: "%.2f", self.order_Amount))"
                return cell
            }else{
                return UITableViewCell()
            }
        } else if indexPath.row == 3 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNWalletBalanceCell", for: indexPath) as? CNWalletBalanceCell {
                cell.delegate = self
                cell.lblWalletCodeValue.text = self.walletBalance ?? ""
                cell.wallet_Balance = self.wallet_Balance
                return cell
            }else{
                return UITableViewCell()
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNWalletImageCell", for: indexPath) as? CNWalletImageCell {
                return cell
            }else{
                return UITableViewCell()
            }
        }
    }
}

extension CNPaymentOptionsVC: paidByWalletDelegate {
    func paidByWallet(isWalletSelected: Bool?, emptyWallet: Bool) {
        if emptyWallet {
            self.view.makeToast(ConstantValue.noWalletBalance.rawValue)
        } else {
            self.payingByWallet = isWalletSelected
            if isWalletSelected ?? false {
                self.isPaidByWalletOrCod = "2"
            } else {
                self.isPaidByWalletOrCod = "1"
            }
        }
    }
}

extension CNPaymentOptionsVC: CNPaidByPopupDelegate {
    func popUpCancelAction() {
        self.popController?.dismiss(animated: false)
    }
    
    func popUpDismissAction() {
        self.popController?.dismiss(animated: false)
    }
    
    func popUpPlaceOrderAction() {
        var array_item : [[String:Any]] = []
        for i in 0..<self.array_main.count{
            if let array_product = self.array_main[i].products{
                for dict in array_product{
                    let product_id = dict.id?.type ?? ""
                    let product_name = dict.name?.type ?? ""
                    let product_sku = ""
                    let qty = dict.qty ?? 0
                    let price = dict.price?.type ?? ""
                    let discount_amount = dict.discount?.type ?? ""
                    let weight = dict.weight?.type ?? ""
                    let unit = dict.unit?.type ?? ""
                    let status = ""
                    
                    let dict_item = ["product_id" : product_id,
                                     "product_name" : product_name,
                                     "product_sku" : product_sku,
                                     "qty" : qty,
                                     "price" : price,
                                     "discount_amount" : discount_amount,
                                     "weight" : weight,
                                     "unit" : unit,
                                     "status" : status

                        ] as [String : Any]
                    
                    array_item.append(dict_item)
                }
            }
            
        }
        print(array_item)
        

        let user_id  = UserDefaults.standard.string(forKey: "user_id") ?? ""
        let name  = UserDefaults.standard.string(forKey: "user_name") ?? ""
        let phone  = UserDefaults.standard.string(forKey: "user_no") ?? ""
        let shipping_amount = 50.0
        
        let order_amount = self.order_Amount-shipping_amount
        let isCoupon = self.isCouponApplied ?? "0"

        let dict = ["method" : "placeOrder",
                    "storeId" : "101",
                    "userId"  : user_id,
                    "cod" : order_amount,
                    "status" : self.isPaidByWalletOrCod ?? "1",
                    "order_amount" : order_amount,
                    "shipping_amount" : "\(shipping_amount)",
                    "paid_amount" : order_amount,
                    "delivery_date" : self.delivery_Date ?? "",
                    "order_status" : "Delivered",
                    "name" : name,
                    "phone" : phone,
                    "pincode" : self.pincode ?? "",
                    "state" : self.state ?? "",
                    "city" : self.city ?? "",
                    "address" : self.address ?? "",
                    "isCouponApplied" : isCoupon,
                    "couponName" : self.couponName ?? "",
                    "couponPrice" : self.couponAmount ?? "",
                    "order_items" : array_item

            ] as [String : Any]
        
        self.placeOrderApi(params: dict, array_item: array_item)
    }
}

extension CNPaymentOptionsVC: CNPPopupControllerDelegate{
    
}

extension CNPaymentOptionsVC: CNPromoCodeDelegate {
    func promoCodeSelectionDelegate(senderButton: UIButton, lblPromoCodeValue: UILabel, lblPromoDescriptionLabel: UILabel) {
        if senderButton.currentImage == UIImage(named: "checkmark.png") {
            self.promoCodeApplied = false
            self.order_Amount = self.order_Amount + self.coupon_Amount
            self.coupon_Amount = 0.0
            self.isCouponApplied = "0"
            senderButton.setImage(UIImage(), for: .normal)
            lblPromoCodeValue.text = ""
            lblPromoDescriptionLabel.text = ""
            self.tblPaymentOptions.reloadData()
        } else {
            senderButton.setImage(UIImage(named: "checkmark.png"), for: .normal)
            self.promoCodeApplied = true
            self.isCouponApplied = "1"
            self.getApplyPromoCodeAPI(senderButton: senderButton, lblPromoCodeValue: lblPromoCodeValue, lblPromoDescriptionLabel: lblPromoDescriptionLabel)
        }
    }
}



