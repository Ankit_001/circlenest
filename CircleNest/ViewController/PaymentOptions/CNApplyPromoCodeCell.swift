//
//  CNApplyPromoCodeCell.swift
//  CircleNest
//
//  Created by techjini on 08/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

protocol CNPromoCodeDelegate {
    func promoCodeSelectionDelegate(senderButton: UIButton, lblPromoCodeValue: UILabel, lblPromoDescriptionLabel: UILabel)
}

class CNApplyPromoCodeCell: UITableViewCell {

    @IBOutlet weak var lblPromoCodeValue: UILabel!
    @IBOutlet weak var btnPromoCodeOutlet: UIButton!
    @IBOutlet weak var lblPromoDescriptionLabel: UILabel!
    var coupon_Amount: Double = 0.0
    
    var delegate: CNPromoCodeDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnPromoCodeCheckmark(_ sender: UIButton) {
        delegate?.promoCodeSelectionDelegate(senderButton: sender, lblPromoCodeValue: self.lblPromoCodeValue, lblPromoDescriptionLabel: self.lblPromoDescriptionLabel)
    }
}
