//
//  CNPaymentOptionsVM.swift
//  CircleNest
//
//  Created by techjini on 14/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import Foundation

class CNPaymentOptionsVM: NSObject {
    
    func callGetTotalWalletCashbackAPI(param:[String:Any],completionHandler: @escaping (_ success: Bool, _ responseMessage : String?, _ response: CNGetTotalWalletCashbackModel?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    print(json)
                    if let response = json["msg"] as? [String:Any], let responseCode = response["responseCode"] as? Int {
                        let result = Utill.parseObject(CNGetTotalWalletCashbackModel.self, data: response)
                        completionHandler("\(responseCode)".bool, "", result)
                    } else {
                        completionHandler(false,"",nil)
                    }
                }
            case .failure :
                completionHandler(false,"",nil)
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
            }
        }
    }
    
    func callCouponCodeAPI(param:[String:Any],completionHandler: @escaping (_ success: Bool, _ responseMessage : String?, _ response: CNUserCashbackModel?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    print(json)
                    if let response = json["msg"] as? [String:Any], let responseCode = response["responseCode"] as? Int {
                        let result = Utill.parseObject(CNUserCashbackModel.self, data: response)
                        completionHandler("\(responseCode)".bool, "", result)
                    } else {
                        completionHandler(false,"",nil)
                    }
                }
            case .failure :
                completionHandler(false,"",nil)
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
            }
        }
    }
    
    func callPlaceOrderAPI(param:[String:Any],completionHandler: @escaping (_ success: Bool, _ responseMessage : String?, _ orderId: Int?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    print(json)
                    if let response = json["msg"] as? [String:Any], let responseCode = response["responseCode"] as? Int,  let orderId = response["orderId"] as? Int {
                        completionHandler("\(responseCode)".bool, "", orderId)
                    } else {
                        completionHandler(false,"",nil)
                    }
                }
            case .failure :
                completionHandler(false,"",nil)
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
            }
        }
    }
    
    func callClearCartAPI(param:[String:Any],completionHandler: @escaping (_ success: Bool, _ responseMessage : String?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    print(json)
                    if let response = json["msg"] as? [String:Any], let responseCode = response["responseCode"] as? Int {
                        completionHandler("\(responseCode)".bool, "")
                    } else {
                        completionHandler(false,"")
                    }
                }
            case .failure :
                completionHandler(false,"")
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
            }
        }
    }
}
