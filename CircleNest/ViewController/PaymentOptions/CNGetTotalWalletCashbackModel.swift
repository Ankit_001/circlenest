//
//  CNGetTotalWalletCashbackModel.swift
//  CircleNest
//
//  Created by techjini on 14/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import Foundation

public class CNGetTotalWalletCashbackModel : Codable {
    public var responseCode : DynamicDataType?
    public var data : [cashbackAmount]?
    public var msg : DynamicDataType?
    
    private enum CodingKeys : String, CodingKey {
        case responseCode = "responseCode", data = "data", msg = "msg"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let responseCode = try container.decodeIfPresent(DynamicDataType.self, forKey: .responseCode) {
            self.responseCode = responseCode
        }
        
        if let msg = try container.decodeIfPresent(DynamicDataType.self, forKey: .msg) {
            self.msg = msg
        }
        if let data = try container.decodeIfPresent([cashbackAmount].self, forKey: .data) {
            self.data = data
        }
    }
}

public class cashbackAmount : Codable {
    public var amount : DynamicDataType?
    
    private enum CodingKeys : String, CodingKey {
        case amount = "amount"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let amount = try container.decodeIfPresent(DynamicDataType.self, forKey: .amount) {
            self.amount = amount
        }
    }
}
