//
//  CNOrderPlacedSuccessVC.swift
//  CircleNest
//
//  Created by techjini on 08/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNOrderPlacedSuccessVC: UIViewController {

    @IBOutlet weak var lblOrderIdValue: UILabel!
    var orderId: Int? = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblOrderIdValue.text = "\(self.orderId ?? 0)"
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnOkActionOrderPlaced(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: false)
       
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}
