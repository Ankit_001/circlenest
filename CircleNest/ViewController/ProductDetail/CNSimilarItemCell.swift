//
//  CNSimilarItemCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 01/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
protocol similarItemDelegate {
    func clickedinSimilarItem(_indexpath : IndexPath)
}

class CNSimilarItemCell: UITableViewCell {
    @IBOutlet weak var collectionView : UICollectionView!
    var delegate : similarItemDelegate?
    var indexPath : IndexPath!
    var array_simItems : [item_list]?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.register(UINib(nibName: "CNSimilarItemCollectionCell", bundle: nil), forCellWithReuseIdentifier: "CNSimilarItemCollectionCell")
        
        let layout = UICollectionViewFlowLayout()
        if #available(iOS 10.0, *) {
            layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
         }else {
            layout.estimatedItemSize = CGSize(width: 160, height: 159)
         
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
extension CNSimilarItemCell : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.array_simItems?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CNSimilarItemCollectionCell", for: indexPath) as? CNSimilarItemCollectionCell{
            if let dict_sim = self.array_simItems?[indexPath.item]{
                cell.populatedata(dict_item: dict_sim)
            }
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 160, height: 159)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.clickedinSimilarItem(_indexpath: indexPath)
        
    }
    
}
