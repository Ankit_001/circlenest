//
//  CNProductDetailCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 01/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
protocol btnCountDelegate {
    func btnAddPressed(indexpath : IndexPath , cell : CNProductDetailCell)
    func btnPlusPressed(indexpath : IndexPath , cell : CNProductDetailCell)
    func btnMinusPressed(indexpath : IndexPath , cell : CNProductDetailCell)
}

class CNProductDetailCell: UITableViewCell {
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDesc : UILabel!
    @IBOutlet weak var lblWeight : UILabel!
    @IBOutlet weak var lblPrice : UILabel!
    @IBOutlet weak var imgProduct : UIImageView!
    @IBOutlet weak var btnWishList : UIButton!
    
    @IBOutlet weak var viewAdd : UIView!
    @IBOutlet weak var btnAdd : UIButton!
    @IBOutlet weak var viewCount : UIView!
    @IBOutlet weak var btnPlus : UIButton!
    @IBOutlet weak var btnMinus : UIButton!
    @IBOutlet weak var lblCount : UILabel!
    @IBOutlet weak var viewOutOfStock : UIView!
    @IBOutlet weak var collectionView : UICollectionView!
    
    var array_wishList : [Wishlist] = []
    var delegate : btnCountDelegate?
    var indexPath : IndexPath!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        viewAdd.layer.cornerRadius = 3.0
        viewAdd.clipsToBounds = true
        viewOutOfStock.layer.cornerRadius = 3.0
        viewOutOfStock.clipsToBounds = true
        self.collectionView.register(UINib(nibName: "CNVariantCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CNVariantCollectionViewCell")
        self.collectionView.reloadData()
        
    }
    func populatedata(dict_item : item_list){
        
        self.lblTitle.text = dict_item.name?.type
        self.lblWeight.text = dict_item.weight?.type
        self.lblPrice.text  = dict_item.price?.type
        self.lblDesc.text = dict_item.description?.type
        let image = dict_item.image?.type?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        self.imgProduct.sd_setImage(with: URL(string: image ?? ""), placeholderImage: UIImage(named: "Placeholder"))
        
        self.btnWishList.setImage(UIImage(named: "Wishlist"), for: UIControl.State.normal)
        for dict_wishlist in self.array_wishList{
            if dict_wishlist.id == dict_item.id?.type {
                self.btnWishList.setImage(UIImage(named: "wishlist_red"), for: UIControl.State.normal)
            }
        }
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    @IBAction func btnAddPressed(_sender:Any){
        self.delegate?.btnAddPressed(indexpath: indexPath, cell: self)
       
    }
    @IBAction func btnPlusPressed(_sender:Any){
        self.delegate?.btnPlusPressed(indexpath: indexPath, cell: self)
    }
    @IBAction func btnMinusPressed(_sender:Any){
        self.delegate?.btnMinusPressed(indexpath: indexPath, cell: self)
    }
}
extension CNProductDetailCell :  UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CNVariantCollectionViewCell", for: indexPath) as? CNVariantCollectionViewCell{
            
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 50)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}
