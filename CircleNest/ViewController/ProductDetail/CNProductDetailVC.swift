//
//  CNProductDetailVC.swift
//  CircleNest
//
//  Created by Ankit Gupta on 01/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit



class CNProductDetailVC: CNBaseViewController {
    @IBOutlet weak var tableView : UITableView!
    var cat_id : String?
    var array_sim : [item_list]?
    var array_wishList : [Wishlist] = []
    var array_Detail : [item_list]?
    var product_name : String?
    var product_id : String?
    var arrayCartDB : [CartItem] = []
    var stock_qty : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerCell()
        self.tableView.estimatedRowHeight = 1000
        self.tableView.rowHeight = UITableView.automaticDimension
        titleLabel.text = self.product_name
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.arrayCartDB = CNCoreDataManager.sharedManager.getCartItemData()
        self.setItemCount()
        self.getProductDetail(product_id: self.product_id ?? "0")
    }
    
    func getProductDetail(product_id : String){
        let vm = CNSimilarProductDetailVM()
        let paramDict  = ["method" : "getProductDetail","productId" : product_id] as [String:AnyObject]
        vm.getProductDetailAPI(param: paramDict) { (success, message,response) in
            if success && message.isEmpty {
                self.array_Detail = response?.data
                if self.array_Detail?.count ?? 0 > 0{
                    self.getSimilarProduct(product_id: product_id, cat_id: self.cat_id ?? "0")

                }
            }
        }
    }
    
    func getSimilarProduct(product_id : String, cat_id : String){
        self.showHUD(progressLabel: "Loading")
        let vm = CNSimilarProductDetailVM()
        let paramDict  = ["method" : "getSimiliarProduct","storeId" : 101,"catId" : cat_id,"productId" : product_id] as [String:AnyObject]
        vm.similarProductListAPI(param: paramDict) { (success, message,response) in
            if success && message.isEmpty {
                self.dismissHUD(isAnimated: true)
                self.array_sim = response?.data
                if self.array_sim?.count ?? 0 > 0{
                    self.tableView.reloadData()
                }
            }
        }
    }

    func registerCell(){
        self.tableView.register(UINib(nibName: "CNProductDetailCell", bundle: nil), forCellReuseIdentifier: "CNProductDetailCell")
        self.tableView.register(UINib(nibName: "CNSimilarItemCell", bundle: nil), forCellReuseIdentifier: "CNSimilarItemCell")
    }
    @IBAction func btnBackPressed(_sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    @objc func btnWishlistPressed(){
        if let dict = self.array_Detail?[0]{
            CNCoreDataManager.sharedManager.insertWishlistItemInLocalDB(dict_item: dict, cat_id: cat_id ?? "0")
            self.array_wishList = CNCoreDataManager.sharedManager.getWishListData()
            self.tableView.reloadData()
        }
    }
    func addItemIntoCart(indexpath: IndexPath, quantity : Int , productId :String , method : String){
        self.showHUD(progressLabel: "Loading")
        let vm = CNSubcategoryItemVM()
        let user_id = UserDefaults.standard.string(forKey: "user_id")
        let paramDict  = ["method" : method,
                          "storeId" : 101,
                          "userId" : user_id ?? "0",
                          "productId":productId,
                          "qty":quantity] as [String:AnyObject]
        print(paramDict)
        
        vm.callAddItemIntoCarttAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.insertItemIntoDB(indexpath: indexpath, count: quantity)
            
            } else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    func removeItemFromCart(indexpath: IndexPath, quantity : Int , productId :String){
        self.showHUD(progressLabel: "Loading")
        let vm = CNSubcategoryItemVM()
        let user_id = UserDefaults.standard.string(forKey: "user_id")
        let paramDict  = ["method" : "removeCart",
                          "storeId" : 101,
                          "userId" : user_id ?? "0",
                          "productId":productId,
                          ] as [String:AnyObject]
        print(paramDict)
        
        vm.removeItemFromCart(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.insertItemIntoDB(indexpath: indexpath, count: quantity)
            
            } else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    
    func insertItemIntoDB(indexpath: IndexPath, count :Int){
        if let array = self.array_Detail{
            let dict_item = array[indexpath.row]
            print(dict_item.name ?? "")
            CNCoreDataManager.sharedManager.checkItemPresence(dict_item: dict_item, item_count: count)
            self.arrayCartDB = CNCoreDataManager.sharedManager.getCartItemData()
        }
    }
}
extension CNProductDetailVC : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if indexPath.section == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNProductDetailCell", for: indexPath) as? CNProductDetailCell{
                cell.selectionStyle = .none
                cell.btnWishList .addTarget(self, action: #selector(CNProductDetailVC.btnWishlistPressed), for: UIControl.Event.touchUpInside)
                cell.array_wishList = self.array_wishList
                cell.indexPath = indexPath
                cell.delegate = self
                
                if let stock_quan = self.stock_qty{
                    if stock_quan == "0"{
                        cell.viewOutOfStock.isHidden = false
                    }else{
                        cell.viewOutOfStock.isHidden = true
                    }
                }
                

                if let dict = self.array_Detail?[indexPath.row]{
                    cell.populatedata(dict_item: dict)
                }
                cell.viewCount.isHidden = true
                cell.viewAdd.isHidden = false
                cell.lblCount.text = "0"
                if let dict_detail = self.array_Detail?[indexPath.row]{
                    for dict in self.arrayCartDB{
                        if dict_detail.id?.type == dict.id {
                            cell.viewAdd.isHidden = true
                            cell.viewCount.isHidden = false
                            cell.lblCount.text = "\(dict.count)"
                        }
                    }
                }

                return cell
            }
        }else{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNSimilarItemCell", for: indexPath) as? CNSimilarItemCell{
                cell.selectionStyle = .none
                cell.delegate = self
                cell.indexPath = indexPath
                cell.array_simItems = self.array_sim
                cell.collectionView.reloadData()
                return cell

            }
        }
        return UITableViewCell()
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section==0{
            return 0
        }else{
            return 15
        }
    }
}
extension CNProductDetailVC : btnCountDelegate{
    func btnAddPressed(indexpath: IndexPath, cell: CNProductDetailCell) {
        print(indexpath.row)
        
        if let item_count = Int(cell.lblCount.text ?? "0"){
            let count = item_count + 1
            cell.lblCount.text = "\(count)"
            cell.viewAdd.isHidden = true
            cell.viewCount.isHidden = false
            self.countLabel.isHidden = false
            
            let count_cart = Int64(self.countLabel.text ?? "0") ?? 0
            let count_inc = count_cart+1
            self.countLabel.text = "\(count_inc)"
            
            DispatchQueue.main.async {
                let product_id = self.array_Detail?[indexpath.row].id?.type
                self.addItemIntoCart(indexpath: indexpath, quantity: count, productId: product_id ?? "0", method: "addToCart")
            }
        }

    }
    
    func btnPlusPressed(indexpath: IndexPath, cell: CNProductDetailCell) {
        print(indexpath.row)
        if let item_count = Int(cell.lblCount.text ?? "0"){
            let count = item_count + 1
            cell.lblCount.text = "\(count)"
            self.countLabel.isHidden = false

            
            let count_cart = Int64(self.countLabel.text ?? "0") ?? 0
            let count_inc = count_cart+1
            self.countLabel.text = "\(count_inc)"
            
            DispatchQueue.main.async {
                let product_id = self.array_Detail?[indexpath.row].id?.type
                self.addItemIntoCart(indexpath: indexpath, quantity: count, productId: product_id ?? "0", method: "updateCart")
            }
        }
    }
    
    func btnMinusPressed(indexpath: IndexPath, cell: CNProductDetailCell) {
        print(indexpath.row)
        
        if let item_count = Int(cell.lblCount.text ?? "0"){
            let count = item_count - 1
            cell.lblCount.text = "\(count)"
            
            let count_cart = Int64(self.countLabel.text ?? "0") ?? 0
            let count_inc = count_cart-1
            self.countLabel.text = "\(count_inc)"
            
            if count_inc == 0 {
                self.countLabel.isHidden = true
            }else{
                self.countLabel.isHidden = false
            }
            
            let product_id = self.array_Detail?[indexpath.row].id?.type
            
            if count == 0{
                cell.viewAdd.isHidden = false
                cell.viewCount.isHidden = true
                DispatchQueue.main.async {
                    self.removeItemFromCart(indexpath: indexpath, quantity: count, productId: product_id ?? "0")
                }
                
            }else{
                cell.viewAdd.isHidden = true
                cell.viewCount.isHidden = false
                DispatchQueue.main.async {
                    self.addItemIntoCart(indexpath: indexpath, quantity: count, productId: product_id ?? "0", method: "updateCart")
                }
            }
        }
    }

}

extension CNProductDetailVC : similarItemDelegate{
    func clickedinSimilarItem(_indexpath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(identifier: "CNProductDetailVC") as? CNProductDetailVC
        
        if let dict_similar = self.array_sim?[_indexpath.row]{
            vc?.product_name = dict_similar.name?.type
            vc?.product_id = dict_similar.id?.type
            vc?.cat_id = self.cat_id
            vc?.array_wishList = self.array_wishList
            vc?.stock_qty = dict_similar.stock_qty?.type
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
}
