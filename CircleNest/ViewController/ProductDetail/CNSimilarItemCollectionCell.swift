//
//  CNSimilarItemCollectionCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 01/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNSimilarItemCollectionCell: UICollectionViewCell {
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var imgProduct : UIImageView!
    @IBOutlet weak var lblPrice : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func populatedata(dict_item : item_list){
        self.lblName.text = dict_item.name?.type
        self.lblPrice.text  = dict_item.price?.type
        let image = dict_item.image?.type?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        self.imgProduct.sd_setImage(with: URL(string: image ?? ""), placeholderImage: UIImage(named: "Placeholder"))

    }

}
