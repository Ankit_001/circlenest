//
//  CNOwnStoreVC.swift
//  CircleNest
//
//  Created by Ankit Gupta on 14/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNOwnStoreVC: UIViewController {
    @IBOutlet weak var tableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()

    }
    func setupUI() {

        self.tableView.register(UINib(nibName: "CNOwnStoreTableCell", bundle: nil), forCellReuseIdentifier: "CNOwnStoreTableCell")
        self.tableView.register(UINib(nibName: "OwnStoreImageCell", bundle: nil), forCellReuseIdentifier: "OwnStoreImageCell")
        
    }
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension CNOwnStoreVC : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "OwnStoreImageCell", for: indexPath) as? OwnStoreImageCell{
                return cell
                
            }
        
        }else{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNOwnStoreTableCell", for: indexPath) as? CNOwnStoreTableCell{
                cell.protocolDelegate = self
                return cell
            
            }
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 379

        }else{
            return 271
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
}

extension CNOwnStoreVC: OwnAStorePressedDelegate {
    
    func actionPerformedOwnStore() {
        
        let storyboard = UIStoryboard(name: "CircleNest", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "CNOwnAStoreFormVC") as? CNOwnAStoreFormVC
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
}
