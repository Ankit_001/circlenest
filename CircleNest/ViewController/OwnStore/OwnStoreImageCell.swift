//
//  OwnStoreImageCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 03/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class OwnStoreImageCell: UITableViewCell {
    @IBOutlet weak var pageControl : UIPageControl!
    @IBOutlet weak var collectionView : UICollectionView!
    var timer = Timer()

    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.register(UINib(nibName: "CNOwnStoreCell", bundle: nil), forCellWithReuseIdentifier: "CNOwnStoreCell")
        self.startTimer()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func startTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.scrollCellHorizontally), userInfo: nil, repeats: true)
    }
    @objc func scrollCellHorizontally(){
        Utill.scrollNextCellUsingDots(collectionView: self.collectionView, pageControl: self.pageControl)
    }
   
    
}
extension OwnStoreImageCell : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CNOwnStoreCell", for: indexPath) as? CNOwnStoreCell{
            cell.indexPath = indexPath
            cell.populateData()
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.collectionView.frame.size.width
        let height = self.collectionView.frame.size.height
        return CGSize(width: width, height: height)
    }
    
    

}
