//
//  CNOwnStoreTableCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 03/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

protocol OwnAStorePressedDelegate {
    func actionPerformedOwnStore()
}

class CNOwnStoreTableCell: UITableViewCell {
    @IBOutlet weak var btnOwnStore : UIButton!
    var protocolDelegate: OwnAStorePressedDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.btnOwnStore.layer.cornerRadius = 5.0
        self.btnOwnStore.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnOwnAStoreAction(_ sender: UIButton) {
        protocolDelegate?.actionPerformedOwnStore()
    }
    
    
}
