//
//  CNOwnStoreCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 03/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNOwnStoreCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var imgLogin : UIImageView!
    var indexPath : IndexPath!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func populateData(){
        if self.indexPath.item == 0{
            self.lblTitle.text = "Open Your Plant Nursery"
            self.imgLogin.image = UIImage(named: "nurseryicon")
            
        }else if self.indexPath.item == 1{
            self.lblTitle.text = "Earn From Your Space"
            self.imgLogin.image = UIImage(named: "earnfromyourspace.png")
            
        }else{
            self.lblTitle.text = "Business From Your Home"
            self.imgLogin.image = UIImage(named: "businessfromhome.png")
            
        }
        
    }


}
