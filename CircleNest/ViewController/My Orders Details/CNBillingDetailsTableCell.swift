//
//  CNBillingDetailsTableCell.swift
//  CircleNest
//
//  Created by techjini on 10/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNBillingDetailsTableCell: UITableViewCell {
    
    @IBOutlet weak var lblOrderAmountValue: UILabel!
    @IBOutlet weak var lblShippingAmountValue: UILabel!
    @IBOutlet weak var lblTotalAmountValue: UILabel!
    @IBOutlet weak var lblPaidAmountValue: UILabel!
    @IBOutlet weak var lblCashOnDeliveryValue: UILabel!
    @IBOutlet weak var lblCouponCodePriceValue: UILabel!
    @IBOutlet weak var lblCouponCode: UILabel!
    @IBOutlet weak var cnstCouponViewHeight: NSLayoutConstraint!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populatedata(prodOrderDetail : orderDetail?){
        self.lblOrderAmountValue.text = "₹ \(prodOrderDetail?.subtotal?.type ?? "")"
        self.lblShippingAmountValue.text = "₹ \(prodOrderDetail?.shippingamount?.type ?? "")"
        self.lblCouponCode.text = "₹ \(prodOrderDetail?.couponName?.type ?? "")"
        self.lblCouponCodePriceValue.text = "₹ \(prodOrderDetail?.couponPrice?.type ?? "")"
        self.lblTotalAmountValue.text = "₹ \(prodOrderDetail?.TotalAmount?.type ?? "")"
        self.lblPaidAmountValue.text = "₹ \(prodOrderDetail?.paid_amount?.type ?? "")"
        self.lblCashOnDeliveryValue.text = "₹ \(prodOrderDetail?.cashOnDelivery?.type ?? "")"
        
    }
    
}
