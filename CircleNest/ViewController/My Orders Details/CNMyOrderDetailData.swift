//
//  CNMyOrderDetailData.swift
//  CircleNest
//
//  Created by techjini on 02/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import Foundation

public class CNOrderDetailModel : Codable {
    public var responseCode : Int?
    public var data : orderDetail?
    public var msg : String?
    
    private enum CodingKeys : String, CodingKey {
        case responseCode = "responseCode", data = "data", msg = "msg"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let responseCode = try container.decodeIfPresent(Int.self, forKey: .responseCode) {
            self.responseCode = responseCode
        }
        
        if let msg = try container.decodeIfPresent(String.self, forKey: .msg) {
            self.msg = msg
        }
        if let data = try container.decodeIfPresent(orderDetail.self, forKey: .data) {
            self.data = data
        }
    }
}

public class orderDetail : Codable {
    public var cashOnDelivery : DynamicDataType?
    public var cod : DynamicDataType?
    public var couponName : DynamicDataType?
    public var couponPrice : DynamicDataType?
    public var couponType : DynamicDataType?
    public var couponTypeValue : DynamicDataType?
    public var deliverydate : DynamicDataType?
    public var isCouponApplied : DynamicDataType?
    public var orderid : DynamicDataType?
    public var orderAmount : DynamicDataType?
    public var orderDate : DynamicDataType?
    public var orderStatus : DynamicDataType?
    public var paid_amount : DynamicDataType?
    public var pickup : DynamicDataType?
    public var pickupStoreAddress : DynamicDataType?
    public var pickupStoreId : DynamicDataType?
    public var pickupStorePhone : DynamicDataType?
    public var TotalAmount : DynamicDataType?
    public var shippingamount : DynamicDataType?
    public var shippingaddress : DynamicDataType?
    public var shippingcity : DynamicDataType?
    public var shippingmobile : DynamicDataType?
    public var shippingname : DynamicDataType?
    public var shippingpincode : DynamicDataType?
    public var shippingstate : DynamicDataType?
    public var subtotal : DynamicDataType?
    public var useCashback : DynamicDataType?
    public var product : [productList]?
    
    private enum CodingKeys : String, CodingKey {
        case cashOnDelivery = "Cash on Delivery"
        case cod = "cod"
        case couponName = "couponName"
        case couponPrice = "couponPrice"
        case couponType = "couponType"
        case couponTypeValue = "couponTypeValue"
        case deliverydate = "delivery date"
        case isCouponApplied = "isCouponApplied"
        case orderid = "order_id"
        case orderAmount = "order_amount"
        case orderDate = "order_date"
        case orderStatus = "order_status"
        case paid_amount = "paid_amount"
        case pickup = "pickup"
        case pickupStoreAddress = "pickupStoreAddress"
        case pickupStoreId = "pickupStoreId"
        case pickupStorePhone = "pickupStorePhone"
        case TotalAmount = "total_amount"
        case shippingamount = "shipping_amount"
        case shippingaddress = "shipping_address"
        case shippingcity = "shipping_city"
        case shippingmobile = "shipping_mobile"
        case shippingname = "shipping_name"
        case shippingpincode = "shipping_pincode"
        case shippingstate = "shipping_state"
        case subtotal = "sub_total"
        case useCashback = "use_cashback"
        case product = "product"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        if let cashOnDelivery = try container.decodeIfPresent(DynamicDataType.self, forKey: .cashOnDelivery) {
            self.cashOnDelivery = cashOnDelivery
        }
        if let cod = try container.decodeIfPresent(DynamicDataType.self, forKey: .cod) {
            self.cod = cod
        }
        if let couponName = try container.decodeIfPresent(DynamicDataType.self, forKey: .couponName) {
            self.couponName = couponName
        }
        if let couponPrice = try container.decodeIfPresent(DynamicDataType.self, forKey: .couponPrice) {
            self.couponPrice = couponPrice
        }
        if let couponType = try container.decodeIfPresent(DynamicDataType.self, forKey: .couponType) {
            self.couponType = couponType
        }
        if let couponTypeValue = try container.decodeIfPresent(DynamicDataType.self, forKey: .couponTypeValue) {
            self.couponTypeValue = couponTypeValue
        }
        if let TotalAmount = try container.decodeIfPresent(DynamicDataType.self, forKey: .TotalAmount) {
            self.TotalAmount = TotalAmount
        }
        if let orderid = try container.decodeIfPresent(DynamicDataType.self, forKey: .orderid) {
            self.orderid = orderid
        }
        if let deliverydate = try container.decodeIfPresent(DynamicDataType.self, forKey: .deliverydate) {
            self.deliverydate = deliverydate
        }
        if let isCouponApplied = try container.decodeIfPresent(DynamicDataType.self, forKey: .isCouponApplied) {
            self.isCouponApplied = isCouponApplied
        }
        if let orderAmount = try container.decodeIfPresent(DynamicDataType.self, forKey: .orderAmount) {
            self.orderAmount = orderAmount
        }
        if let orderDate = try container.decodeIfPresent(DynamicDataType.self, forKey: .orderDate) {
            self.orderDate = orderDate
        }
        if let orderStatus = try container.decodeIfPresent(DynamicDataType.self, forKey: .orderStatus) {
            self.orderStatus = orderStatus
        }
        if let pickup = try container.decodeIfPresent(DynamicDataType.self, forKey: .pickup) {
            self.pickup = pickup
        }
        if let useCashback = try container.decodeIfPresent(DynamicDataType.self, forKey: .useCashback) {
            self.useCashback = useCashback
        }
        if let paid_amount = try container.decodeIfPresent(DynamicDataType.self, forKey: .paid_amount) {
            self.paid_amount = paid_amount
        }
        if let product = try container.decodeIfPresent([productList].self, forKey: .product) {
            self.product = product
        }
        if let pickupStoreAddress = try container.decodeIfPresent(DynamicDataType.self, forKey: .pickupStoreAddress) {
            self.pickupStoreAddress = pickupStoreAddress
        }
        if let pickupStoreId = try container.decodeIfPresent(DynamicDataType.self, forKey: .pickupStoreId) {
            self.pickupStoreId = pickupStoreId
        }
        if let pickupStorePhone = try container.decodeIfPresent(DynamicDataType.self, forKey: .pickupStorePhone) {
            self.pickupStorePhone = pickupStorePhone
        }
        if let shippingamount = try container.decodeIfPresent(DynamicDataType.self, forKey: .shippingamount) {
            self.shippingamount = shippingamount
        }
        if let shippingaddress = try container.decodeIfPresent(DynamicDataType.self, forKey: .shippingaddress) {
            self.shippingaddress = shippingaddress
        }
        if let shippingcity = try container.decodeIfPresent(DynamicDataType.self, forKey: .shippingcity) {
            self.shippingcity = shippingcity
        }
        if let shippingmobile = try container.decodeIfPresent(DynamicDataType.self, forKey: .shippingmobile) {
            self.shippingmobile = shippingmobile
        }
        if let shippingname = try container.decodeIfPresent(DynamicDataType.self, forKey: .shippingname) {
            self.shippingname = shippingname
        }
        if let shippingpincode = try container.decodeIfPresent(DynamicDataType.self, forKey: .shippingpincode) {
            self.shippingpincode = shippingpincode
        }
        if let shippingstate = try container.decodeIfPresent(DynamicDataType.self, forKey: .shippingstate) {
            self.shippingstate = shippingstate
        }
        if let subtotal = try container.decodeIfPresent(DynamicDataType.self, forKey: .subtotal) {
            self.subtotal = subtotal
        }
    }
}

public class productList : Codable {
    public var discount_amount : DynamicDataType?
    public var discount_type : DynamicDataType?
    public var image : DynamicDataType?
    public var price : DynamicDataType?
    public var product_id : DynamicDataType?
    public var product_name : DynamicDataType?
    public var product_sku : DynamicDataType?
    public var qty : DynamicDataType?
    public var unit : DynamicDataType?
    public var weight : DynamicDataType?
    
    private enum CodingKeys : String, CodingKey {
        case discount_amount = "discount_amount", discount_type = "discount_type", image = "image", price = "price", product_id = "product_id", product_name = "product_name", product_sku = "product_sku", qty = "qty", unit = "unit", weight = "weight"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let discount_amount = try container.decodeIfPresent(DynamicDataType.self, forKey: .discount_amount) {
            self.discount_amount = discount_amount
        }
        if let discount_type = try container.decodeIfPresent(DynamicDataType.self, forKey: .discount_type) {
            self.discount_type = discount_type
        }
        if let image = try container.decodeIfPresent(DynamicDataType.self, forKey: .image) {
            self.image = image
        }
        if let price = try container.decodeIfPresent(DynamicDataType.self, forKey: .price) {
            self.price = price
        }
        if let product_id = try container.decodeIfPresent(DynamicDataType.self, forKey: .product_id) {
            self.product_id = product_id
        }
        if let product_name = try container.decodeIfPresent(DynamicDataType.self, forKey: .product_name) {
            self.product_name = product_name
        }
        if let product_sku = try container.decodeIfPresent(DynamicDataType.self, forKey: .product_sku) {
            self.product_sku = product_sku
        }
        if let qty = try container.decodeIfPresent(DynamicDataType.self, forKey: .qty) {
            self.qty = qty
        }
        if let unit = try container.decodeIfPresent(DynamicDataType.self, forKey: .unit) {
            self.unit = unit
        }
        if let weight = try container.decodeIfPresent(DynamicDataType.self, forKey: .weight) {
            self.weight = weight
        }
    }
}
