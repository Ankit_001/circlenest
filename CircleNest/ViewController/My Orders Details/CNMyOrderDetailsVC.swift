//
//  CNMyOrderDetailsVC.swift
//  CircleNest
//
//  Created by techjini on 10/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNMyOrderDetailsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var prodOrderDetail: orderDetail?
    var popController: CNPPopupController?
    var indexValue: Int = 0
    var orderId: DynamicDataType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.callUserOrderDetailAPI()
    }
    
    func setupUI() {
        self.navigationItem.title = "Order List"
        self.tableView.register(UINib(nibName: "CNOrderDetailsTableCell", bundle: nil), forCellReuseIdentifier: "CNOrderDetailsTableCell")
        self.tableView.register(UINib(nibName: "CNOrderListTableCell", bundle: nil), forCellReuseIdentifier: "CNOrderListTableCell")
        self.tableView.register(UINib(nibName: "CNBillingDetailsTableCell", bundle: nil), forCellReuseIdentifier: "CNBillingDetailsTableCell")
        self.tableView.register(UINib(nibName: "CNDeliveryAddressTableCell", bundle: nil), forCellReuseIdentifier: "CNDeliveryAddressTableCell")
        self.tableView.register(UINib(nibName: "CNOrderCancelOptionTableCell", bundle: nil), forCellReuseIdentifier: "CNOrderCancelOptionTableCell")
        
    }
    
    @IBAction func btnBackPressed(_sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func callUserOrderDetailAPI(){
        self.showHUD(progressLabel: "Loading")
        let vm = CNMyOrderDetailVM()
        var paramDict  = ["method" : "getOrderDetail","orderId" : self.orderId?.type] as [String:AnyObject]
        let userDefaults = UserDefaults.standard
        if let userId = userDefaults.value(forKey: "user_id") {
            paramDict.updateValue(userId as AnyObject, forKey: "userId")
        }
        vm.callUserOrderDetailAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.prodOrderDetail = result?.data
                self.tableView.reloadData()
            } else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }

}

extension CNMyOrderDetailsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let orderDetails = prodOrderDetail?.product
        let orderArrayCount = orderDetails?.count ?? 0
        let orderStatus = self.prodOrderDetail?.orderStatus?.type
        if orderStatus?.lowercased() == "Pending".lowercased() {
            return orderArrayCount + 4
        } else {
            return orderArrayCount + 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let orderDetails = prodOrderDetail?.product
        let orderArrayCount = orderDetails?.count ?? 0
        if indexPath.row == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNOrderDetailsTableCell", for: indexPath) as? CNOrderDetailsTableCell {
                cell.lblOrderStatus.text = "Order is \(self.prodOrderDetail?.orderStatus?.type ?? "")"
                cell.lblOrderDate.text = "\(self.prodOrderDetail?.orderDate?.type ?? "")"
                cell.lblOrderIdValue.text = "\(self.prodOrderDetail?.orderid?.type ?? "")"
                return cell
            }
        } else if indexPath.row < orderArrayCount + 1 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNOrderListTableCell", for: indexPath) as? CNOrderListTableCell {
                let productDict = self.prodOrderDetail?.product?[indexPath.row - 1]
                cell.populatedata(dict_item: productDict)
                return cell
            }
        } else if indexPath.row == orderArrayCount + 1 {
            if self.prodOrderDetail?.orderStatus?.type?.lowercased() == "Pending".lowercased() {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "CNOrderCancelOptionTableCell", for: indexPath) as? CNOrderCancelOptionTableCell {
                    cell.delegate = self
                    return cell
                }
            } else {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "CNDeliveryAddressTableCell", for: indexPath) as? CNDeliveryAddressTableCell {
                    cell.populatedata(prodOrderDetail: self.prodOrderDetail)
                    return cell
                }
            }
            
        } else if indexPath.row == orderArrayCount + 2 {
            if self.prodOrderDetail?.orderStatus?.type?.lowercased() == "Pending".lowercased() {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "CNDeliveryAddressTableCell", for: indexPath) as? CNDeliveryAddressTableCell {
                    cell.populatedata(prodOrderDetail: self.prodOrderDetail)
                    return cell
                }
            } else {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "CNBillingDetailsTableCell", for: indexPath) as? CNBillingDetailsTableCell {
                    cell.populatedata(prodOrderDetail: self.prodOrderDetail)
                    if self.prodOrderDetail?.couponName?.type?.isEmpty ?? true {
                        cell.cnstCouponViewHeight.constant = 0
                    } else {
                        cell.cnstCouponViewHeight.constant = 66
                    }
                    return cell
                }
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNBillingDetailsTableCell", for: indexPath) as? CNBillingDetailsTableCell {
                cell.populatedata(prodOrderDetail: self.prodOrderDetail)
                if self.prodOrderDetail?.couponName?.type?.isEmpty ?? true {
                    cell.cnstCouponViewHeight.constant = 0
                } else {
                    cell.cnstCouponViewHeight.constant = 66
                }
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension CNMyOrderDetailsVC: cancelOrderTapped{
    
    func cancelOrderAction() {
        let storyboard = UIStoryboard(name: "CircleNest", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "CNCancelOrderVC") as? CNCancelOrderVC
        vc?.product = prodOrderDetail?.product
        vc?.orderId = self.orderId
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
