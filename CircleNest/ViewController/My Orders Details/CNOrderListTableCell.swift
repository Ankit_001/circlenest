//
//  CNOrderListTableCell.swift
//  CircleNest
//
//  Created by techjini on 10/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNOrderListTableCell: UITableViewCell {
    
    @IBOutlet weak var ivProductImage: UIImageView!
    @IBOutlet weak var lblProductTitleValue: UILabel!
    @IBOutlet weak var lblQuantityValue: UILabel!
    @IBOutlet weak var lblWeightValue: UILabel!
    @IBOutlet weak var lblPriceValue: UILabel!
    @IBOutlet weak var lblDiscountedPriceValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populatedata(dict_item : productList?){
        self.lblQuantityValue.text = dict_item?.qty?.type ?? ""
        self.lblProductTitleValue.text = dict_item?.product_name?.type ?? ""
        self.lblPriceValue.text = "₹ \(dict_item?.price?.type ?? "")"
        self.lblWeightValue.text = dict_item?.weight?.type ?? ""
        if Double(dict_item?.discount_amount?.type ?? "0.0") == 0.0 {
            self.lblDiscountedPriceValue.text = ""
        } else {
            self.lblDiscountedPriceValue.text = "₹ \(dict_item?.discount_amount?.type ?? "")"
        }
        let image = dict_item?.image?.type?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        self.ivProductImage.sd_setImage(with: URL(string: image ?? ""), placeholderImage: UIImage(named: "no-image-available.jpeg"))
        
    }
    
}
