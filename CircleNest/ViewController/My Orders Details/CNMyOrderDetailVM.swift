//
//  CNMyOrderDetailVM.swift
//  CircleNest
//
//  Created by techjini on 02/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import Foundation
class CNMyOrderDetailVM: NSObject {
    func callUserOrderDetailAPI(param:[String:Any],completionHandler: @escaping (_ success: Bool, _ responseMessage : String?, _ response: CNOrderDetailModel?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    if let response = json["msg"] as? [String:Any], let responseCode = response["responseCode"] as? Int {
                        let result = Utill.parseObject(CNOrderDetailModel.self, data: response)
                        completionHandler("\(responseCode)".bool, "", result)
                    } else {
                        completionHandler(false,"",nil)
                    }
                }
            case .failure :
                completionHandler(false,"",nil)
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
            }
        }
    }
    
    func callOrderCancelAPI(param:[String:Any],completionHandler: @escaping (_ success: Bool, _ responseMessage : String?, _ response: Any?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    if let response = json["msg"] as? [String:Any], let responseCode = response["responseCode"] as? Int, let errorMessage =  response["msg"] as? String {
//                        let result = Utill.parseObject(CNOrderDetailModel.self, data: response)
                        completionHandler("\(responseCode)".bool, errorMessage, nil)
                    } else {
                        completionHandler(false,"",nil)
                    }
                }
            case .failure :
                completionHandler(false,"",nil)
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
            }
        }
    }
}
