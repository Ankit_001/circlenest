//
//  CNOrderCancelOptionTableCell.swift
//  CircleNest
//
//  Created by techjini on 21/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

protocol cancelOrderTapped {
    func cancelOrderAction()
}

class CNOrderCancelOptionTableCell: UITableViewCell {
    
    var delegate: cancelOrderTapped?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnCancelOrderAction(_ sender: UIButton) {
        delegate?.cancelOrderAction()
    }
}
