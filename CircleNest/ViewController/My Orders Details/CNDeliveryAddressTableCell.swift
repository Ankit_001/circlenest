//
//  CNDeliveryAddressTableCell.swift
//  CircleNest
//
//  Created by techjini on 10/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNDeliveryAddressTableCell: UITableViewCell {

    @IBOutlet weak var lblContactPerson: UILabel!
    @IBOutlet weak var lblDeliveryAddress: UILabel!
    @IBOutlet weak var lblContactPersonMobileNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populatedata(prodOrderDetail : orderDetail?){
        self.lblContactPerson.text = "\(prodOrderDetail?.shippingname?.type ?? "")"
        self.lblContactPersonMobileNumber.text = "Phone Number: \(prodOrderDetail?.shippingmobile?.type ?? "")"
        self.lblDeliveryAddress.text = "\(prodOrderDetail?.shippingaddress?.type ?? ""), \(prodOrderDetail?.shippingcity?.type ?? ""), \(prodOrderDetail?.shippingstate?.type ?? ""), \(prodOrderDetail?.shippingpincode?.type ?? "")"
        
    }
    
}
