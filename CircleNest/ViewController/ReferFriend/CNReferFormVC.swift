//
//  CNReferFormVC.swift
//  CircleNest
//
//  Created by Ankit Gupta on 05/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNReferFormVC: UIViewController {
    @IBOutlet weak var viewMale : UIView!
    @IBOutlet weak var viewFemale : UIView!
    @IBOutlet weak var viewSelected : UIView!
    @IBOutlet weak var btnSlected : UIButton!
    @IBOutlet weak var lblSelected : UILabel!
    @IBOutlet weak var lblOr : UILabel!
    @IBOutlet weak var btnSubmit : UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewSelected.isHidden = true
        self.lblOr.isHidden = false
        self.viewFemale.layer.cornerRadius = 10
        self.viewFemale.clipsToBounds = true
        self.viewMale.layer.cornerRadius = 10
        self.viewMale.clipsToBounds = true
        self.viewSelected.layer.cornerRadius = 10
        self.viewSelected.clipsToBounds = true
        self.btnSubmit.layer.cornerRadius = 30.0
        self.btnSubmit.clipsToBounds = true

    }
    @IBAction func btnBackPressed(){
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnMalePressed(){
        self.viewMale.isHidden = true
        self.viewFemale.isHidden = true
        self.viewSelected.isHidden = false
        self.btnSlected.setBackgroundImage(UIImage(named: "men"), for: UIControl.State.normal)
        self.lblSelected.text = "Male"
        self.lblOr.isHidden = true
    }
    @IBAction func btnFemalePressed(){
        self.viewMale.isHidden = true
        self.viewFemale.isHidden = true
        self.viewSelected.isHidden = false
        self.btnSlected.setBackgroundImage(UIImage(named: "women"), for: UIControl.State.normal)
        self.lblSelected.text = "Female"
        self.lblOr.isHidden = true



    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
