//
//  CNReferFriendVC.swift
//  CircleNest
//
//  Created by Ankit Gupta on 14/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNReferFriendVC: UIViewController {
    @IBOutlet weak var btnReferFriend : UIButton!
    @IBOutlet weak var btnShareLink : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnReferFriend.layer.cornerRadius = 5.0
        self.btnReferFriend.clipsToBounds = true
        self.btnShareLink.layer.cornerRadius = 5.0
        self.btnShareLink.clipsToBounds = true

    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnReferFriendPressed(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "CNReferFormVC") as? CNReferFormVC
        self.navigationController?.pushViewController(vc!, animated: true)
    
    }
    @IBAction func btnShareLinkPressed(_ sender: Any) {
        Utill.openShareLinkDialogue(viewController: self)
    }
    

   

}
