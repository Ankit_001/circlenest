//
//  CNBecomeMemberVC.swift
//  CircleNest
//
//  Created by Ankit Gupta on 14/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNBecomeMemberVC: UIViewController {
    @IBOutlet weak var viewBottom : UIView!
    @IBOutlet weak var tablevView : UITableView!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.tablevView.register(UINib(nibName: "SMBecomemberBenifitCell", bundle: nil), forCellReuseIdentifier: "SMBecomemberBenifitCell")
        self.tablevView.register(UINib(nibName: "SMBecomeMemberExpCell", bundle: nil), forCellReuseIdentifier: "SMBecomeMemberExpCell")
        self.viewBottom.layer.cornerRadius = 30
        self.viewBottom.layer.borderWidth = 2.0
        self.viewBottom.layer.borderColor = UIColor(red: 256.0/255.0, green: 133.0/255.0, blue: 92.0/255.0, alpha: 1).cgColor
        self.viewBottom.clipsToBounds = true

        // Do any additional setup after loading the view.
    }
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnJoinCNPressed(_sender:Any){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "CNJoinCircleNestVC") as! CNJoinCircleNestVC
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
extension CNBecomeMemberVC : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "SMBecomeMemberExpCell", for: indexPath) as? SMBecomeMemberExpCell{
                return cell
                
            }
        
        }else{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "SMBecomemberBenifitCell", for: indexPath) as? SMBecomemberBenifitCell{
                return cell
            
            }
        }
        return UITableViewCell()
    }
}
