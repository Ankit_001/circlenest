//
//  CNOTPDataModel.swift
//  CircleNest
//
//  Created by techjini on 22/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import Foundation

public class CNLoginDataModel : Codable {
    public var responseCode : Int?
    public var data : userData?
    public var msg : String?
    public var userVerification : String?
    
    private enum CodingKeys : String, CodingKey {
        case responseCode = "responseCode", data = "data", msg = "msg", userVerification = "userVerification"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let responseCode = try container.decodeIfPresent(Int.self, forKey: .responseCode) {
            self.responseCode = responseCode
        }
        
        if let msg = try container.decodeIfPresent(String.self, forKey: .msg) {
            self.msg = msg
        }
        if let data = try container.decodeIfPresent(userData.self, forKey: .data) {
            self.data = data
        }
        
        if let userVerification = try container.decodeIfPresent(String.self, forKey: .userVerification) {
            self.userVerification = userVerification
        }
    }

}
public class userData : Codable {
    public var id : String?
    public var name : String?
    public var email : String?
    public var phone : String?
    public var gender : String?
    public var authKey : String?
    public var level : Int?
    public var shipping : Int?
    public var total : String?
    
    private enum CodingKeys : String, CodingKey {
        case id = "id", name = "name" , email = "email", phone = "phone" , gender = "gender", authKey = "authKey", level = "level", total = "total", shipping
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let id = try container.decodeIfPresent(String.self, forKey: .id) {
            self.id = id
        }
        if let name = try container.decodeIfPresent(String.self, forKey: .name) {
            self.name = name
        }
        if let email = try container.decodeIfPresent(String.self, forKey: .email) {
            self.email = email
        }
        if let phone = try container.decodeIfPresent(String.self, forKey: .phone) {
            self.phone = phone
        }
        if let gender = try container.decodeIfPresent(String.self, forKey: .gender) {
            self.gender = gender
        }
        if let authKey = try container.decodeIfPresent(String.self, forKey: .authKey) {
            self.authKey = authKey
        }
        if let level = try container.decodeIfPresent(Int.self, forKey: .level) {
            self.level = level
        }
        if let total = try container.decodeIfPresent(String.self, forKey: .total) {
            self.total = total
        }
        if let shipping = try container.decodeIfPresent(Int.self, forKey: .shipping) {
            self.shipping = shipping
        }
    }
}

