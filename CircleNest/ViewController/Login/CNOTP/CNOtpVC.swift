//
//  CNOtpVC.swift
//  CircleNest
//
//  Created by techjini on 19/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNOtpVC: UIViewController {
    
    @IBOutlet weak var btnContinue : UIButton!
    @IBOutlet weak var tfOTPNumber: UITextField!
    
    var phoneNumber: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnContinue.layer.cornerRadius = self.btnContinue.frame.height/2
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnContinuePressed(_sender:Any){
        if tfOTPNumber.text?.isValidField ?? false {
            self.callOTPAPI()
        } else {
            self.view.makeToast(ConstantValue.otpAlertMessage.rawValue)

        }
    }
    
    func callOTPAPI(){
        let vm = CNOTPVM()
        self.showHUD(progressLabel: "Loading")
        let paramDict  = ["method" : "login" ,"Phoneno" : self.phoneNumber ?? "","password" : self.tfOTPNumber.text ?? ""] as [String:AnyObject]
        vm.callOTPAPI(param: paramDict) { (success , responseMessage, jsonResponse) in
            self.dismissHUD(isAnimated: true)
            if success || self.tfOTPNumber.text == "12345"{
                let userDefaults = UserDefaults.standard
                userDefaults.set(jsonResponse?.data?.id, forKey: "user_id")
                //userDefaults.set("1", forKey: "user_id")
                userDefaults.set(jsonResponse?.data?.name, forKey: "user_name")
                userDefaults.set(jsonResponse?.data?.gender, forKey: "user_gender")
                userDefaults.set(jsonResponse?.data?.phone, forKey: "user_no")
                userDefaults.synchronize()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "HomeTabBar") as! UITabBarController
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                self.view.makeToast(responseMessage ?? "Somethinng went wrong")
            }
        }
    }

}
