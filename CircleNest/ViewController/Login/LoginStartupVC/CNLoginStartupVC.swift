//
//  LoginStartupVC.swift
//  CircleNest
//
//  Created by techjini on 19/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNLoginStartupVC: UIViewController {
    
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var pageControl : UIPageControl!
    @IBOutlet weak var btnLogin : UIButton!
    @IBOutlet weak var btnSignup : UIButton!
    
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.startTimer()
        self.collectionView.register(UINib(nibName: "CNloginStartupCell", bundle: nil), forCellWithReuseIdentifier: "CNloginStartupCell")
        self.setupUI()
    }
    
   
    func setupUI() {
        self.btnLogin.layer.borderWidth = 2.0
        self.btnLogin.layer.borderColor = UIColor(red: 220/255, green: 140/255, blue: 17/255, alpha: 1.0).cgColor
        self.btnLogin.layer.cornerRadius = self.btnLogin.frame.height/2
        self.btnSignup.layer.cornerRadius = self.btnSignup.frame.height/2
    }
    
    func startTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.scrollCellHorizontally), userInfo: nil, repeats: true)
    }
    
    
    
    @objc func scrollCellHorizontally(){
        Utill.scrollNextCellUsingDots(collectionView: self.collectionView, pageControl: self.pageControl)
    }
    override func viewDidDisappear(_ animated: Bool) {
           self.timer.invalidate()
       
    }
    
    @IBAction func btnLoginPressed(_sender:Any){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CNLoginVC") as! CNLoginVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnRegistrationPressed(_sender:Any){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CNRegistrationVC") as! CNRegistrationVC
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension CNLoginStartupVC : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CNloginStartupCell", for: indexPath) as? CNloginStartupCell{
            cell.indexPath = indexPath
            cell.populateData()
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.collectionView.frame.size.width
        let h = self.collectionView.frame.size.height
        return CGSize(width: width, height: h)
    }
    
    

}
