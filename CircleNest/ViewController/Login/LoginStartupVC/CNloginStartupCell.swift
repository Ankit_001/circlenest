//
//  CNloginStartupCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 30/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNloginStartupCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var imgLogin : UIImageView!
    var indexPath : IndexPath!
    
    func populateData(){
        if self.indexPath.item == 0{
            self.lblTitle.text = "Converting Expentiture to investment"
            self.imgLogin.image = UIImage(named: "landing_1")
            
        }else if self.indexPath.item == 1{
            self.lblTitle.text = "Integrating Community to benefit"
            self.imgLogin.image = UIImage(named: "landing_2")
            
        }else{
            self.lblTitle.text = "One platform for all daily need"
            self.imgLogin.image = UIImage(named: "landing_3")
            
        }
        
    }
    
}
