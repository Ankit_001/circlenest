//
//  CNRegistrationVM.swift
//  CircleNest
//
//  Created by techjini on 22/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import Foundation
import Alamofire

class CNRegistrationVM: NSObject {
    func callRegistrationAPI(param:[String:Any],completionHandler: @escaping (_ success: Bool, _ responseMessage : String?, _ response: CNLoginDataModel?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    if let jsonMsg = json["msg"] as? [String:Any], let responseCode = jsonMsg["responseCode"] as? Int, let responseMessage = jsonMsg["msg"] as? String {
                        let result = Utill.parseObject(CNLoginDataModel.self, data: jsonMsg)
                        completionHandler("\(responseCode)".bool, responseMessage, result)
                    }
                }
            case .failure :
                completionHandler(false,"",nil)
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
                           
            }
        }
    }
}
