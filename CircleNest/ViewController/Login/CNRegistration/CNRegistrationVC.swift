//
//  CNRegistrationVC.swift
//  CircleNest
//
//  Created by techjini on 19/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

enum SelectedGender: String {
    case male = "Male"
    case female = "Female"
}

class CNRegistrationVC: UIViewController {
    
    @IBOutlet weak var tfMobileNumber: UITextField!
    @IBOutlet weak var tfEmailId: UITextField!
    @IBOutlet weak var tfFullName: UITextField!
    @IBOutlet weak var btnContinueOutlet: UIButton!
    @IBOutlet weak var btnMaleOutlet: UIButton!
    @IBOutlet weak var btnFemaleOutlet: UIButton!
    
    var selectedGender: SelectedGender = .male
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnContinueOutlet.layer.cornerRadius = self.btnContinueOutlet.frame.height/2
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnContinueAction(_ sender: UIButton) {
        if self.isValid().validity {
            callRegistrationAPI()
        } else {
            self.view.makeToast(self.isValid().errorMessage)
        }
        
    }
    
    @IBAction func btnLoginNowAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnMaleCheckmarkAsGender(_ sender: UIButton) {
        selectedGender = .male
        self.btnMaleOutlet.setImage(UIImage(systemName: "checkmark.circle.fill"), for: .normal)
        self.btnFemaleOutlet.setImage(UIImage(systemName: "circle"), for: .normal)
    }
    
    @IBAction func btnFemaleCheckmarkAsGender(_ sender: UIButton) {
        selectedGender = .female
        self.btnMaleOutlet.setImage(UIImage(systemName: "circle"), for: .normal)
        self.btnFemaleOutlet.setImage(UIImage(systemName: "checkmark.circle.fill"), for: .normal)
    }
    
    func callRegistrationAPI(){
        let vm = CNRegistrationVM()
        self.showHUD(progressLabel: "Loading")
        let paramDict  = ["method" : "signup2" ,"phone" : tfMobileNumber.text ?? "","storeId" : 101, "gender": selectedGender.rawValue, "name": tfFullName.text ?? "", "email": tfEmailId.text ?? ""] as [String:AnyObject]
        vm.callRegistrationAPI(param: paramDict) { (success , responseMessage, jsonResponse) in
            self.dismissHUD(isAnimated: true)
            if success {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "CNOtpVC") as! CNOtpVC
                vc.phoneNumber = self.tfMobileNumber.text
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                self.view.makeToast(responseMessage ?? "Somethinng went wrong")
            }
        }
    }
    
    func isValid() -> (errorMessage: String, validity: Bool){
        if !(tfFullName.text?.isValidField ?? false) {
            return (errorMessage: ConstantValue.nameAlertMessage.rawValue, validity: false)
        } else if !(tfEmailId.text?.isValidEmail ?? false) {
            return (errorMessage: ConstantValue.emailAlertMessage.rawValue, validity: false)
        } else if !(tfMobileNumber.text?.isValidPhoneNumberMBL ?? false){
            return (errorMessage: ConstantValue.mobileAlertMessage.rawValue, validity: false)
        } else {
            return (errorMessage: "", validity: true)
        }
    }
    
}

extension CNRegistrationVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
}
