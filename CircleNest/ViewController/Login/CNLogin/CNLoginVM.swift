//
//  CNLoginVM.swift
//  CircleNest
//
//  Created by techjini on 22/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
import Alamofire

class CNLoginVM: NSObject {
    func callLoginAPI (param:[String:Any],completionHandler: @escaping (_ success: Bool, _ responseMessage : String?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    if let responseCode = json["responseCode"] as? Int, let responseMessage = json["msg"] as? String {
                        completionHandler("\(responseCode)".bool, responseMessage)
                    }
                }
            case .failure :
                completionHandler(false,"")
                print("Somethinng went wrong")
                
                           
            }
        }
    }
}
