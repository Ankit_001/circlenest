//
//  CNLoginVC.swift
//  CircleNest
//
//  Created by Ankit Gupta on 12/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNLoginVC: UIViewController {
    
    @IBOutlet weak var tfMobileNumber: UITextField!
    @IBOutlet weak var btnContinueOutlet: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.btnContinueOutlet.layer.cornerRadius = self.btnContinueOutlet.frame.height/2
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnContinueAction(_ sender: UIButton) {
        if self.tfMobileNumber.text?.isValidPhoneNumberMBL ?? false {
            self.callLoginAPI()
        } else {
            self.view.makeToast(ConstantValue.mobileAlertMessage.rawValue)
        }
    }
    
    func callLoginAPI(){
        self.showHUD(progressLabel: "Loading")
        let vm = CNLoginVM()
        let paramDict  = ["method" : "SendLoginPassword","storeId" : 101,"phone" : self.tfMobileNumber.text ?? ""] as [String:AnyObject]
        vm.callLoginAPI(param: paramDict) { (success , responseMessage) in
            self.dismissHUD(isAnimated: true)
            if success {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "CNOtpVC") as! CNOtpVC
                vc.phoneNumber = self.tfMobileNumber.text
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                self.view.makeToast(responseMessage ?? "Somethinng went wrong")
            }
        }
    }
    
    @IBAction func btnRegisterNowAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CNRegistrationVC") as! CNRegistrationVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
