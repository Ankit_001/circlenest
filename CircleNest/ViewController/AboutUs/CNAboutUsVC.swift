//
//  CNAboutUsVC.swift
//  CircleNest
//
//  Created by Ankit Gupta on 14/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNAboutUsVC: UIViewController {
    @IBOutlet weak var tableView : UITableView!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: "CNAboutUsCell", bundle: nil), forCellReuseIdentifier: "CNAboutUsCell")

        // Do any additional setup after loading the view.
    }
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
extension CNAboutUsVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       if let cell = tableView.dequeueReusableCell(withIdentifier: "CNAboutUsCell", for: indexPath) as? CNAboutUsCell{
           return cell
           
       }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 690
        
    }
    
}
