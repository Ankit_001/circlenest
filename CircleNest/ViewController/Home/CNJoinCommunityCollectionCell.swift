//
//  CNJoinCommunityCollectionCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 17/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNJoinCommunityCollectionCell: UICollectionViewCell {
    @IBOutlet weak var viewCard : UIView!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblSubTitle : UILabel!
    @IBOutlet weak var imgView : UIImageView!

    var indexPath  : IndexPath!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        viewCard.layer.cornerRadius = 5.0
        viewCard.layer.shadowColor = UIColor.gray.cgColor
        viewCard.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        viewCard.layer.shadowRadius = 5.0
        viewCard.layer.shadowOpacity = 0.3
        
    }
    func PopulateData(){
        if self.indexPath.item == 0 {
            self.lblTitle.text = "Be A Partner"
            self.lblSubTitle.text = "Double your business and become an entreprneur"
            self.imgView.image = UIImage(named: "partner")
        }else if self.indexPath.item == 1 {
            self.lblTitle.text = "Own A Store"
            self.lblSubTitle.text = "Start your business without any investment"
            self.imgView.image = UIImage(named: "store")
        }else{
            self.lblTitle.text = "Be A Member"
            self.lblSubTitle.text = "Enjoy discount as customer and profilt as Owner"
            self.imgView.image = UIImage(named: "member")
            
        }
        
    }

}
