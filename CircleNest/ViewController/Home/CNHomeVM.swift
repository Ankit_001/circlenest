//
//  CNHomeVM.swift
//  CircleNest
//
//  Created by Ankit Gupta on 20/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
import Alamofire

class CNHomeVM: NSObject {
    func callBannerListAPI (param:[String:Any],completionHandler: @escaping (_ success: Bool, _ message: String, _ response : CNBannerDataModel?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    if let dict = json["msg"] as? [String:Any]{
                        let result = Utill.parseObject(CNBannerDataModel.self, data: dict)
                        completionHandler(true,"",result)
                    }
                }
            case .failure :
                completionHandler(false,"",nil)
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
                           
            }
        }
    }
    
    func callCategoryListAPI (param:[String:Any],completionHandler: @escaping (_ success: Bool, _ message: String, _ response : CNCategoryListDataModel?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
                print(response)
                switch response.result {
                case .success :
                    let jsonData = response.data
                    guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                        return
                    }
                    DispatchQueue.main.async {
                        if let dict = json["msg"] as? [String:Any]{
                            let result = Utill.parseObject(CNCategoryListDataModel.self, data: dict)
                            completionHandler(true,"",result)
                        }
                    }
                case .failure :
                    completionHandler(false,"",nil)
                    Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
                    
            }
        }
    }
}
