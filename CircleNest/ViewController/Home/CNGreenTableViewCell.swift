//
//  CNGreenTableViewCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 17/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

protocol breathCleanTapped {
    func breathCleanAction()
}

class CNGreenTableViewCell: UITableViewCell {
    @IBOutlet weak var viewDesc : UIView!
    var delegate: breathCleanTapped?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnBreatheCleanTapped(_ sender: UIButton) {
        delegate?.breathCleanAction()
    }
}
