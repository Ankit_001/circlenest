//
//  CNHomeVC.swift
//  CircleNest
//
//  Created by Ankit Gupta on 13/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD


class CNHomeVC: CNBaseViewController,DrawerControllerDelegate {
    var drawerVw = DrawerView()
    @IBOutlet weak var tableView : UITableView!
    var collectionCell_height : Float = 125.0
    var array_banner: [banner]?
    var array_cat : [Category]?
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
    let sceneDelegate = UIApplication.shared.connectedScenes
    .first!.delegate as! SceneDelegate
    var array_main : [CategoryData] = []


    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = "Home"
        self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableView.automaticDimension
        self.registerCell()

        self.callCategoryBannerApi()
        self.callCircleNestSaleAPI()
    }
    override func viewWillAppear(_ animated: Bool) {
//        self.setItemCount()
        self.callCartListAPI()
    }
    
    func callCartListAPI(){
        let user_id = UserDefaults.standard.string(forKey: "user_id")
        let vm = CNCartListVM()
        let paramDict  = ["method" : "getCartList2",
                          "storeId" : 101,
                          "userId" : user_id ?? "0"] as [String:AnyObject]
        
        vm.callCartListAPI(param: paramDict) { (success, message,response) in
            if success && message.isEmpty {
                self.array_main = response?.data ?? []
                if self.array_main.count>0{
                    for dict in self.array_main{
                        if let array_product = dict.products{
                            for product in array_product{
                                let qty = product.qty
                                CNCoreDataManager.sharedManager.checkCartItem(dict_item: product, item_count: qty ?? 0)
                            }
                        }
                    }
                    self.setItemCount()
                    
                }else{
                    CNCoreDataManager.sharedManager.deleteAllItemFromCart()
                    self.setItemCount()
                }
                
                
            }else{
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    func callCategoryBannerApi(){
        let vm = CNHomeVM()
        let paramDict  = ["method" : "banner","storeId" : 101,"positionId" : 1] as [String:AnyObject]
        vm.callBannerListAPI(param: paramDict) { (success, message,response) in
            
            if success && message.isEmpty {
                self.array_banner = response?.data?.banner
                self.callCategoryApi()
                
            }else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    
    func registerCell(){
        self.tableView.register(UINib(nibName: "CNBannerCell", bundle: nil), forCellReuseIdentifier: "CNBannerCell")
        self.tableView.register(UINib(nibName: "CNCategoriesTableViewCell", bundle: nil), forCellReuseIdentifier: "CNCategoriesTableViewCell")
        self.tableView.register(UINib(nibName: "CNGreenTableViewCell", bundle: nil), forCellReuseIdentifier: "CNGreenTableViewCell")
        self.tableView.register(UINib(nibName: "CNJoinOurCommunitiyCell", bundle: nil), forCellReuseIdentifier: "CNJoinOurCommunitiyCell")
        self.tableView.register(UINib(nibName: "CNKnowCircleNestCell", bundle: nil), forCellReuseIdentifier: "CNKnowCircleNestCell")
    }
    
    func callCategoryApi(){
        self.showHUD(progressLabel: "Loading")
        let vm = CNHomeVM()
        let paramDict  = ["method" : "getCategoryList","storeId" : 101] as [String:AnyObject]
        vm.callCategoryListAPI(param: paramDict) { (success, message,response) in
            self.dismissHUD(isAnimated: true)
            if success && message.isEmpty {
                self.array_cat = response?.data?.category
                if self.array_cat?.count ?? 0 > 0{
                    self.tableView.reloadData()
                }else {
                    self.view.makeToast("Somethinng went wrong")
                }
            }
        }
    }
    
    func callCircleNestSaleAPI() {
        let monthAsNr = Calendar.current.component(.month, from: Date())
        let yearAsNr = Calendar.current.component(.year, from: Date())
        let vm = CNUserProfileVM()
        var paramDict  = ["method" : "totalSales","storeId" : 101,"month":monthAsNr,"year":yearAsNr] as [String:AnyObject]
        let userDefaults = UserDefaults.standard
        if let userId = userDefaults.value(forKey: "user_id") {
            paramDict.updateValue(userId as AnyObject, forKey: "userId")
        }
        vm.callGetSalesAPI(param: paramDict) { (success, result) in
            if success {
                userDefaults.set(result, forKey: "totalSalesCount")
            } else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }

    @IBAction func actShowMenu(_ sender: Any) {
        drawerVw = DrawerView(aryControllers:Utill.DrawerArray.array, controller:self)
        drawerVw.delegate = self
        drawerVw.show()
    }

    func pushTo(viewController: UIViewController) {
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func showAlertForLogout() {
        Utill.showAlertWithLogout(title: "CircleNest", Message: "Are you Sure? You want to logout this app")
    }

}
extension CNHomeVC : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNBannerCell", for: indexPath) as? CNBannerCell{
                cell.array_banner = self.array_banner
                cell.collectionView.reloadData()
                return cell
                
            }else{
                return UITableViewCell()
            }
        }else if indexPath.section  == 1{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNCategoriesTableViewCell", for: indexPath) as? CNCategoriesTableViewCell{
                cell.indexPath = indexPath
                if let array = self.array_cat{
                    cell.array_category = array
                    cell.collectionView.reloadData()
                }
                cell.delegate = self
                return cell
                
            }else{
                return UITableViewCell()
            }
        }else if indexPath.section  == 2{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNGreenTableViewCell", for: indexPath) as? CNGreenTableViewCell{
                cell.viewDesc.layer.cornerRadius = 5.0
                cell.viewDesc.clipsToBounds = true
                cell.delegate = self
                return cell
                
            }else{
                return UITableViewCell()
            }
        }else if indexPath.section  == 3{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNJoinOurCommunitiyCell", for: indexPath) as? CNJoinOurCommunitiyCell{
                cell.delegate = self
                return cell
                
            }else{
                return UITableViewCell()
            }
        }else if indexPath.section  == 4{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNKnowCircleNestCell", for: indexPath) as? CNKnowCircleNestCell{
                return cell
                
            }else{
                return UITableViewCell()
            }
        }else{
            return UITableViewCell()
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1{
            let total_row = Float(self.array_cat?.count ?? 0) / 3.0
            let totalrow_roundOff = ceilf(total_row)
            return CGFloat(totalrow_roundOff*collectionCell_height)+50
            
        }else if indexPath.section == 4{
            return 300
        } else {
            return UITableView.automaticDimension
        }
    }
}
extension CNHomeVC : categoryItemDelegate{
    func clickonCategoryItem(indexPath: IndexPath) {
        if let array = self.array_cat{
            let dict_subcat = array[indexPath.item]
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "CNCategoryItemVCViewController") as? CNCategoryItemVCViewController
            vc?.dict_subcat = dict_subcat
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
}

extension CNHomeVC : communitiyJoinedDelegate {
    func clickedOnSelectiveCommunity(_ indexPathItem: Int) {
        switch indexPathItem {
        case 0:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "Become A Partner") as? CNBecomePartnerVC
            self.navigationController?.pushViewController(vc!, animated: true)
        case 1:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "Own A Store") as? CNOwnStoreVC
            self.navigationController?.pushViewController(vc!, animated: true)
        case 2:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "Become A Member") as? CNBecomeMemberVC
            self.navigationController?.pushViewController(vc!, animated: true)
        default:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "Become A Member") as? CNBecomeMemberVC
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
}

extension CNHomeVC : breathCleanTapped {
    func breathCleanAction() {
        let storyboard = UIStoryboard(name: "CircleNest", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "CNComingSoonVC") as? CNComingSoonVC
        vc?.isComingFrom = "Breathe Clean With Paudhewale"
        vc?.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}


