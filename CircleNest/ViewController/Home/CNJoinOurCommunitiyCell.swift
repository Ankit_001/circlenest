//
//  CNJoinOurCommunitiyCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 17/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

protocol communitiyJoinedDelegate {
    func clickedOnSelectiveCommunity(_ indexPathItem: Int)
}

class CNJoinOurCommunitiyCell: UITableViewCell {
    
    @IBOutlet weak var collectionView : UICollectionView!
    var delegate : communitiyJoinedDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.register(UINib(nibName: "CNJoinCommunityCollectionCell", bundle: nil), forCellWithReuseIdentifier: "CNJoinCommunityCollectionCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
extension CNJoinOurCommunitiyCell : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CNJoinCommunityCollectionCell", for: indexPath) as? CNJoinCommunityCollectionCell{
            cell.indexPath = indexPath
            cell.PopulateData()
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.clickedOnSelectiveCommunity(indexPath.item)
    }

}
