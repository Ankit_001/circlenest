//
//  CNCategoriesTableViewCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 17/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

protocol categoryItemDelegate {
    func clickonCategoryItem(indexPath : IndexPath)
}

class CNCategoriesTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionView : UICollectionView!
    var array_category : [Category] = []
    var delegate : categoryItemDelegate?
    var indexPath : IndexPath!
    


    override func awakeFromNib() {
        super.awakeFromNib()
         self.collectionView.register(UINib(nibName: "CNCategoriesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CNCategoriesCollectionViewCell")
        self.collectionView.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension CNCategoriesTableViewCell : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.array_category.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CNCategoriesCollectionViewCell", for: indexPath) as? CNCategoriesCollectionViewCell{
            cell.lblName.text = self.array_category[indexPath.item].name
            let image_name = self.array_category[indexPath.item].image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            cell.imgView.sd_setImage(with: URL(string: image_name ?? ""), placeholderImage: UIImage(named: "Placeholder"))
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width/3
        return CGSize(width: width, height: 125)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.clickonCategoryItem(indexPath: indexPath)
        
    }
}
