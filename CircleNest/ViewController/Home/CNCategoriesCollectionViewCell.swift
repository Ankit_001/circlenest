//
//  CNCategoriesCollectionViewCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 17/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNCategoriesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var imgView : UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    

}
