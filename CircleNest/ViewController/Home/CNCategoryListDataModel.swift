//
//  CNCategoryListDataModel.swift
//  CircleNest
//
//  Created by Ankit Gupta on 20/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNCategoryListDataModel: Codable {
    public var responseCode : Int?
    public var data : category_data?
    public var msg : String?
}
public class category_data : Codable {
    let category : [Category]?

}
public class Category : Codable {
    let id : String?
    let name : String?
    let image : String?
}
