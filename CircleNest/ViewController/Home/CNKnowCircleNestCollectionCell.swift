//
//  CNJoinCommunityCollectionCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 17/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
import youtube_ios_player_helper_swift

class CNKnowCircleNestCollectionCell: UICollectionViewCell {
    @IBOutlet weak var viewCard : YTPlayerView!
    @IBOutlet weak var btnPlayVideo: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewCard.layer.cornerRadius = 5.0
        viewCard.layer.shadowColor = UIColor.gray.cgColor
        viewCard.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        viewCard.layer.shadowRadius = 5.0
        viewCard.layer.shadowOpacity = 0.3
    }
    @IBAction func btn_PlayVideoAction(_ sender: UIButton) {
        switch viewCard.playerState {
        case .playing:
            viewCard.pauseVideo()
            sender.setImage(UIImage(), for: .normal)
        case .paused:
            viewCard.playVideo()
            sender.setImage(UIImage(), for: .normal)
        default:
            viewCard.playVideo()
            sender.setImage(UIImage(), for: .normal)
        }
    }
    
}
