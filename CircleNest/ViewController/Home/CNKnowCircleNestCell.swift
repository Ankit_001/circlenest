//
//  CNJoinOurCommunitiyCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 17/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
import youtube_ios_player_helper_swift

class CNKnowCircleNestCell: UITableViewCell {
    @IBOutlet weak var collectionView : UICollectionView!
    var arrVideos : [[String:Any]] = [["path":"https://www.youtube.com/watch?v=lEa_9wckVJ4"],["path":"https://www.youtube.com/watch?v=vGJbujmpMLE"],["path":"https://www.youtube.com/watch?v=JDm30B3SXyo"]]

    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.register(UINib(nibName: "CNKnowCircleNestCollectionCell", bundle: nil), forCellWithReuseIdentifier: "CNKnowCircleNestCollectionCell")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
extension CNKnowCircleNestCell : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 300.0, height: 200.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrVideos.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CNKnowCircleNestCollectionCell", for: indexPath) as? CNKnowCircleNestCollectionCell{
            cell.viewCard.delegate = self
            let youtubeUrl = self.arrVideos[indexPath.row]["path"] as? String ?? ""
            let PlayVarDic:Dictionary = ["controls":"0","playsinline":"1","autohide":"1","autoplay":"1","modestbranding":"0","rel":"0" ,"showinfo":"0", "fs":"0","playlist":"0","cc_load_policy":"0","iv_load_policy":"3"]
            let myVideoURL = getYoutubeId(youtubeUrl: youtubeUrl ) ?? ""
            _ = cell.viewCard.load(videoId: myVideoURL, playerVars: PlayVarDic)
            return cell
        }
        return UICollectionViewCell()
    }

}

extension CNKnowCircleNestCell:YTPlayerViewDelegate {
    func getYoutubeId(youtubeUrl: String) -> String? {
        return URLComponents(string: youtubeUrl)?.queryItems?.first(where: { $0.name == "v" || $0.name == "V" })?.value
    }
    
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
        switch state {
        case YTPlayerState.playing:
            print("Started Playback")
            break;
        case YTPlayerState.paused:
            print("Paused Playback")
            break;
        case YTPlayerState.ended:
            print("Ended Playback")
            break;
        case YTPlayerState.buffering:
            print("Buffering Playback")
            break;
            
            
        default:
            print("Error")
        }
    }
    
    func playerViewDidBecomeReady(playerView: YTPlayerView)
    {
        playerView.playVideo()
    }
    
    func playerView(_ playerView: YTPlayerView, receivedError error: YTPlayerError) {
        print(error)
    }
}
