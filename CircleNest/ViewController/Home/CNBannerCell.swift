//
//  CNBannerCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 17/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNBannerCell: UITableViewCell {
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var pageControl : UIPageControl!
    var timer = Timer()
    var array_banner : [banner]?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.register(UINib(nibName: "CNBannerItemCollectionCell", bundle: nil), forCellWithReuseIdentifier: "CNBannerItemCollectionCell")
        self.startTimer()
        
    }
    func startTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.scrollCellHorizontally), userInfo: nil, repeats: true)

    }
    
    @objc func scrollCellHorizontally(){
        Utill.scrollNextCellUsingDots(collectionView: self.collectionView, pageControl: pageControl)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension CNBannerCell : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.array_banner?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CNBannerItemCollectionCell", for: indexPath) as? CNBannerItemCollectionCell{
            let image = self.array_banner?[indexPath.row].image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)

            cell.imgBanner.sd_setImage(with: URL(string: image ?? ""), placeholderImage: UIImage(named: "Placeholder"))
            
            
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        return CGSize(width: width, height: 170)
    }
}
