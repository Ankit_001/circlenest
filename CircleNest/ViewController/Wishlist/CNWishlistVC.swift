//
//  CNWishlistVC.swift
//  CircleNest
//
//  Created by Ankit Gupta on 13/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNWishlistVC: UIViewController {
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var view_noData : UIView!
    var drawerVw = DrawerView()
    var vwBG = UIView()
    var array_wishList : [Wishlist]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.registerCell()
        self.tableView.estimatedRowHeight = 1000
        self.tableView.rowHeight = UITableView.automaticDimension
        self.array_wishList = CNCoreDataManager.sharedManager.getWishListData()
        print(self.array_wishList?.count ?? 0)
        if self.array_wishList?.count == 0 {
            self.tableView.isHidden = true
        } else {
            self.tableView.isHidden = false
        }
        self.tableView.reloadData()


    }
    func registerCell(){
        self.tableView.register(UINib(nibName: "CNWishlistCell", bundle: nil), forCellReuseIdentifier: "CNWishlistCell")
    }
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    @objc func btnDeletePressed(sender:UIButton){
        if let dict_item = self.array_wishList?[sender.tag]{
            CNCoreDataManager.sharedManager.deleteWishlistItemFromLocalDB(item_id: dict_item.id ?? "0")
        }
        self.array_wishList = CNCoreDataManager.sharedManager.getWishListData()
        print(self.array_wishList?.count ?? 0)
        if self.array_wishList?.count == 0 {
            self.tableView.isHidden = true
        } else {
            self.tableView.isHidden = false
        }
        self.tableView.reloadData()
        
        
    }
    
    @IBAction func btnContinueShopping(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name:"Main", bundle:nil)
        let controllerName = storyBoard.instantiateViewController(withIdentifier: "HomeTabBar") as! UITabBarController
        controllerName.selectedIndex = 0
        controllerName.hidesBottomBarWhenPushed = true
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.pushViewController(controllerName, animated: false)
            
    }
   

}
extension CNWishlistVC : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.array_wishList?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CNWishlistCell", for: indexPath) as? CNWishlistCell{
            cell.selectionStyle = .none
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete .addTarget(self, action: #selector(self.btnDeletePressed(sender:)), for: UIControl.Event.touchUpInside)
            if let dict_wishlist = self.array_wishList?[indexPath.row]{
                cell.populatedata(dict_item: dict_wishlist)
            }

            return cell

        }else{
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(identifier: "CNProductDetailVC") as? CNProductDetailVC
        
        if let dict_item = self.array_wishList?[indexPath.row]{
            let cat_id = dict_item.cat_id
            vc?.product_name = dict_item.name
            vc?.product_id = dict_item.id
            vc?.cat_id = cat_id
            vc?.array_wishList = self.array_wishList ?? []
            vc?.stock_qty = dict_item.stock_qty
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    
    
   
}
 

