//
//  CNWishlistCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 01/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNWishlistCell: UITableViewCell {
    @IBOutlet weak var viewCard : UIView!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblWeight : UILabel!
    @IBOutlet weak var lblPrice : UILabel!
    @IBOutlet weak var imgProduct : UIImageView!
    @IBOutlet weak var btnDelete : UIButton!
    @IBOutlet weak var lblDiscountedPrice: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewCard.layer.cornerRadius = 5.0
        viewCard.layer.masksToBounds = false
        viewCard.layer.shadowColor = UIColor.black.cgColor
        viewCard.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        viewCard.layer.shadowRadius = 5.0
        viewCard.layer.shadowOpacity = 0.3
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func populatedata(dict_item : Wishlist){
        
        self.lblName.text = dict_item.name
        self.lblWeight.text = dict_item.weight
        self.lblPrice.text  = "₹ \(dict_item.price ?? "")"
        self.lblDiscountedPrice.text = "₹ \(dict_item.discount ?? "")"
        let image = dict_item.image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        self.imgProduct.sd_setImage(with: URL(string: image ?? ""), placeholderImage: UIImage(named: "Placeholder"))

    }
}
