//
//  CNScheduleVisitVC.swift
//  CircleNest
//
//  Created by Ankit Gupta on 15/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNScheduleVisitVC: UIViewController {
    
    @IBOutlet var tfScheduleForm: [UITextField]!
    @IBOutlet weak var btnConfirmScheduleForm: UIButton!
    
    var datePicker :UIDatePicker!
    var pickerView :UIPickerView!
    var arrMemberShipPlan: [String] = ["Starter Plan - Rs. 5,000","Premium Plan - Rs. 60,000"]
    var strSelectedPickerData: String = ""
    var partnerCategory: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDatePickerForTextField()
        self.setupPickerViewForTextField()
        if !partnerCategory.isEmpty {
            self.setupFormUI()
        }
    }
    
    @IBAction func btnBackPressed(_sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnConfirmScheduleFormAction(_ sender: UIButton) {
        
        if self.isValid().validity {
            self.showHUD(progressLabel: "Loading")
            let vm = CNScheduleVisitVM()
            let paramDict = self.gettingRequest()
            vm.callSaveFormAPI(param: paramDict) { (success, response) in
                self.dismissHUD(isAnimated: true)
                if success {
                    let storyboard = UIStoryboard(name: "CircleNest", bundle: nil)
                    let vc = storyboard.instantiateViewController(identifier: "CNThankYouVC") as? CNThankYouVC
                    self.navigationController?.pushViewController(vc!, animated: false)
                } else {
                    Utill.showAlertWithMessage(title: ConstantValue.title.rawValue, Message: response ?? "Something went wrong", viewController: self)
                }
            }
        } else {
            Utill.showAlertWithMessage(title: ConstantValue.title.rawValue, Message: self.isValid().errorMessage, viewController: self)
        }
    }
    
    func setupFormUI() {
        arrMemberShipPlan = ["Grocery", "Fruits & Vegetable", "Dairy", "Meat & Eggs", "Sweets", "Restaurants", "Plants", "Others"]
        if partnerCategory == "PARTNER"{
            self.title = "PARTNER FORM"
        } else if partnerCategory == "ENTREPRENEUR"{
            self.title = "ENTREPRENEUR FORM"
        } else {
            self.title = "SCHEDULE VISIT FORM"
        }
        tfScheduleForm[8].placeholder = "Category"
        tfScheduleForm[9].placeholder = "Feedback"
    }
    
    func setupDatePickerForTextField() {
        datePicker = UIDatePicker.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 200))
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
        tfScheduleForm[3].inputView = datePicker
        let doneButton = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(self.datePickerDone))
        let toolBar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 44))
        toolBar.setItems([UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil), doneButton], animated: true)
        tfScheduleForm[3].inputAccessoryView = toolBar
    }
    
    func setupPickerViewForTextField() {
        pickerView = UIPickerView.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 200))
        tfScheduleForm[8].inputView = pickerView
        pickerView.delegate = self
        pickerView.dataSource = self
        let doneButton = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(self.pickerDone))
        let toolBar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 44))
        toolBar.setItems([UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil), doneButton], animated: true)
        tfScheduleForm[8].inputAccessoryView = toolBar
    }
    
    @objc func pickerDone() {
        if tfScheduleForm[8].text?.isEmpty ?? false, arrMemberShipPlan.count > 0 {
            tfScheduleForm[8].text = arrMemberShipPlan.first
        } else {
            tfScheduleForm[8].text = strSelectedPickerData
        }
        tfScheduleForm[8].resignFirstResponder()
    }
    
    @objc func datePickerDone() {
        datePicker.datePickerMode = UIDatePicker.Mode.date
        let selectedDate = Utill.settingDateFormatForDate(dateFormat: "MM/dd/yyyy", dateValue: datePicker.date)
        tfScheduleForm[3].text = "\(selectedDate)"
        tfScheduleForm[3].resignFirstResponder()
    }

}

extension CNScheduleVisitVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrMemberShipPlan.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.arrMemberShipPlan[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        strSelectedPickerData = self.arrMemberShipPlan[row]
    }
}


extension CNScheduleVisitVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
}

extension CNScheduleVisitVC {
    
    func isValid() -> (errorMessage: String, validity: Bool){
        
        if !(tfScheduleForm[0].text?.isValidField ?? false) {
            return (errorMessage: ConstantValue.nameAlertMessage.rawValue, validity: false)
        } else if !(tfScheduleForm[1].text?.isValidEmail ?? false) {
            return (errorMessage: ConstantValue.emailAlertMessage.rawValue, validity: false)
        } else if !(tfScheduleForm[2].text?.isValidPhoneNumberMBL ?? false){
            return (errorMessage: ConstantValue.mobileAlertMessage.rawValue, validity: false)
        } else if !(tfScheduleForm[3].text?.isValidField ?? false) {
            return (errorMessage: ConstantValue.dobAlertMessage.rawValue, validity: false)
        } else if !(tfScheduleForm[4].text?.isValidField ?? false) {
            return (errorMessage: ConstantValue.addressAlertMessage.rawValue, validity: false)
        } else if !(tfScheduleForm[5].text?.isValidField ?? false) {
            return (errorMessage: ConstantValue.cityAlertMessage.rawValue, validity: false)
        } else if !(tfScheduleForm[6].text?.isValidField ?? false) {
            return (errorMessage: ConstantValue.stateAlertMessage.rawValue, validity: false)
        } else if !(tfScheduleForm[7].text?.isValidField ?? false) {
            return (errorMessage: ConstantValue.pincodeAlertMessage.rawValue, validity: false)
        } else if !(tfScheduleForm[8].text?.isValidField ?? false), !partnerCategory.isEmpty {
            return (errorMessage: ConstantValue.categoryAlertMessage.rawValue, validity: false)
        } else if !(tfScheduleForm[8].text?.isValidField ?? false), partnerCategory.isEmpty {
            return (errorMessage: ConstantValue.planAlertMessage.rawValue, validity: false)
        } else if !(tfScheduleForm[9].text?.isValidField ?? false), !partnerCategory.isEmpty {
            return (errorMessage: ConstantValue.feedbackAlertMessage.rawValue, validity: false)
        } else if !(tfScheduleForm[9].text?.isValidField ?? false), partnerCategory.isEmpty {
            return (errorMessage: ConstantValue.descriptionAlertMessage.rawValue, validity: false)
        } else {
            return (errorMessage: "", validity: true)
        }
        
    }
    
    func gettingRequest() -> [String:AnyObject] {
        
        var paramDict: [String:AnyObject]
        if partnerCategory == "PARTNER"{
            paramDict = ["method": "SavePartner2"] as [String:AnyObject]
        } else if partnerCategory == "ENTREPRENEUR"{
            paramDict = ["method": "SaveEnterpreneour"] as [String:AnyObject]
        } else {
            paramDict = ["method": "SendEnquiryGmail"] as [String:AnyObject]
        }
        paramDict.updateValue(101 as AnyObject, forKey: "storeId")
        
        
        let userDefaults = UserDefaults.standard
        if let userId = userDefaults.value(forKey: "user_id") {
            paramDict.updateValue(userId as AnyObject, forKey: "userId")
        }
        
        if let Name = tfScheduleForm[0].text {
            paramDict.updateValue(Name as AnyObject, forKey: "Name")
        }
        
        if let Email = tfScheduleForm[1].text {
            paramDict.updateValue(Email as AnyObject, forKey: "Email")
        }
        
        if let Mobile = tfScheduleForm[2].text {
            paramDict.updateValue(Mobile as AnyObject, forKey: "Mobile")
        }
        
        if let Dob = tfScheduleForm[3].text {
            paramDict.updateValue(Dob as AnyObject, forKey: "Dob")
        }
        
        if let Address = tfScheduleForm[4].text {
            paramDict.updateValue(Address as AnyObject, forKey: "Address")
        }
        
        if let City = tfScheduleForm[5].text {
            paramDict.updateValue(City as AnyObject, forKey: "City")
        }
        
        if let State = tfScheduleForm[6].text {
            paramDict.updateValue(State as AnyObject, forKey: "State")
        }
        
        if let PinCode = tfScheduleForm[7].text {
            paramDict.updateValue(PinCode as AnyObject, forKey: "PinCode")
        }
        
        if let StoreName = tfScheduleForm[8].text, !partnerCategory.isEmpty {
            paramDict.updateValue(StoreName as AnyObject, forKey: "CaTegory")
        }
        
        if let StoreName = tfScheduleForm[8].text, partnerCategory.isEmpty {
            paramDict.updateValue(StoreName as AnyObject, forKey: "plan")
        }
        
        if let StoreName = tfScheduleForm[9].text, partnerCategory.isEmpty {
            paramDict.updateValue(StoreName as AnyObject, forKey: "description")
        }
        
        if let Feedback = tfScheduleForm[9].text, !partnerCategory.isEmpty {
            paramDict.updateValue(Feedback as AnyObject, forKey: "Feedback")
        }
        
        return paramDict as [String:AnyObject]
        
    }
    
}
