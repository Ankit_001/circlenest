//
//  CNScheduleVisitVM.swift
//  CircleNest
//
//  Created by techjini on 24/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import Foundation

class CNScheduleVisitVM: NSObject {
    func callSaveFormAPI (param:[String:Any],completionHandler: @escaping (_ success: Bool, _ responseMessage : String?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    print(json)
                    if let responseCode = json["status"] as? Int, let responseMessage = json["msg"] as? String {
                        completionHandler("\(responseCode)".bool, responseMessage)
                    } else if let jsonMsg = json["msg"] as? [String:Any], let responseCode = jsonMsg["responseCode"] as? Int, let responseMessage = jsonMsg["msg"] as? String {
                        completionHandler("\(responseCode)".bool, responseMessage)
                    } else if let responseCode = json["responseCode"] as? Int, let responseMessage = json["msg"] as? String {
                        completionHandler("\(responseCode)".bool, responseMessage)
                    }
                }
            case .failure :
                completionHandler(false, "Somethinng went wrong")
                
                           
            }
        }
    }

}
