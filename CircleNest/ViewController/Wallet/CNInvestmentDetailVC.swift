//
//  CNInvestmentDetailVC.swift
//  CircleNest
//
//  Created by Ankit Gupta on 10/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNInvestmentDetailVC: UIViewController {
    @IBOutlet weak var tableView : UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false
        self.setupTableView()

    }
    func setupTableView(){
        self.tableView.register(UINib(nibName: "CNInvestmentDetailCell", bundle: nil), forCellReuseIdentifier: "CNInvestmentDetailCell")
        self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableView.automaticDimension
        
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
extension CNInvestmentDetailVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       if let cell = tableView.dequeueReusableCell(withIdentifier: "CNInvestmentDetailCell", for: indexPath) as? CNInvestmentDetailCell{
           return cell
       }
       return UITableViewCell()

    }

}
