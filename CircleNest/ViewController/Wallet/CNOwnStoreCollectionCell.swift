//
//  CNOwnStoreCollectionCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 10/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNOwnStoreCollectionCell: UICollectionViewCell {
    @IBOutlet weak var viewCard : UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        viewCard.layer.cornerRadius = 5.0
        viewCard.layer.shadowColor = UIColor.gray.cgColor
        viewCard.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        viewCard.layer.shadowRadius = 5.0
        viewCard.layer.shadowOpacity = 0.3
        
    }

}
