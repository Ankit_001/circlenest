//
//  CNGetWalletHistoryModel.swift
//  CircleNest
//
//  Created by techjini on 14/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import Foundation

public class CNGetWalletHistoryModel : Codable {
    public var responseCode : DynamicDataType?
    public var data : [walletDetail]?
    public var msg : DynamicDataType?
    
    private enum CodingKeys : String, CodingKey {
        case responseCode = "responseCode", data = "data", msg = "msg"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let responseCode = try container.decodeIfPresent(DynamicDataType.self, forKey: .responseCode) {
            self.responseCode = responseCode
        }
        
        if let msg = try container.decodeIfPresent(DynamicDataType.self, forKey: .msg) {
            self.msg = msg
        }
        if let data = try container.decodeIfPresent([walletDetail].self, forKey: .data) {
            self.data = data
        }
    }
}

public class walletDetail : Codable {
    public var cashbackAmount : DynamicDataType?
    public var category : DynamicDataType?
    public var date : DynamicDataType?
    public var order_id : DynamicDataType?
    public var id : DynamicDataType?
    
    private enum CodingKeys : String, CodingKey {
        case cashbackAmount = "Cashback Amount", category = "category", date = "date", order_id = "order_id", id = "id"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let cashbackAmount = try container.decodeIfPresent(DynamicDataType.self, forKey: .cashbackAmount) {
            self.cashbackAmount = cashbackAmount
        }
        
        if let category = try container.decodeIfPresent(DynamicDataType.self, forKey: .category) {
            self.category = category
        }
        
        if let date = try container.decodeIfPresent(DynamicDataType.self, forKey: .date) {
            self.date = date
        }
        
        if let order_id = try container.decodeIfPresent(DynamicDataType.self, forKey: .order_id) {
            self.order_id = order_id
        }
        
        if let id = try container.decodeIfPresent(DynamicDataType.self, forKey: .id) {
            self.id = id
        }
    }
}
