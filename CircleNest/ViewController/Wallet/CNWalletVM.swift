//
//  CNWalletVM.swift
//  CircleNest
//
//  Created by techjini on 14/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import Foundation
class CNWalletVM: NSObject {
    func callTotalWalletAPI(param:[String:Any],completionHandler: @escaping (_ success: Bool, _ responseMessage : String?, _ response: CNTotalWalletModel?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    print(json)
                    if let response = json["msg"] as? [String:Any], let responseCode = response["responseCode"] as? Int {
                        let result = Utill.parseObject(CNTotalWalletModel.self, data: response)
                        completionHandler("\(responseCode)".bool, "", result)
                    } else {
                        completionHandler(false,"",nil)
                    }
                }
            case .failure :
                completionHandler(false,"",nil)
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
            }
        }
    }
    
    func callGetWalletHistoryAPI(param:[String:Any],completionHandler: @escaping (_ success: Bool, _ responseMessage : String?, _ response: CNGetWalletHistoryModel?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    print(json)
                    if let response = json["msg"] as? [String:Any], let responseCode = response["responseCode"] as? Int {
                        let result = Utill.parseObject(CNGetWalletHistoryModel.self, data: response)
                        completionHandler("\(responseCode)".bool, "", result)
                    } else {
                        completionHandler(false,"",nil)
                    }
                }
            case .failure :
                completionHandler(false,"",nil)
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
            }
        }
    }
}
