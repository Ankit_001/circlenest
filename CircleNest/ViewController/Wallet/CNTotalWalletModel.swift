//
//  CNTotalWalletModel.swift
//  CircleNest
//
//  Created by techjini on 14/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import Foundation

public class CNTotalWalletModel : Codable {
    public var returnValue : DynamicDataType?
    public var responseCode : DynamicDataType?
    public var msg : DynamicDataType?
    public var remainCashback : DynamicDataType?
    public var shoppingAmount : DynamicDataType?
    
    private enum CodingKeys : String, CodingKey {
        case returnValue = "return", shoppingAmount = "shopping", responseCode = "responseCode", msg = "msg", remainCashback = "Remain Cashback"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let responseCode = try container.decodeIfPresent(DynamicDataType.self, forKey: .responseCode) {
            self.responseCode = responseCode
        }
        
        if let msg = try container.decodeIfPresent(DynamicDataType.self, forKey: .msg) {
            self.msg = msg
        }
        if let returnValue = try container.decodeIfPresent(DynamicDataType.self, forKey: .returnValue) {
            self.returnValue = returnValue
        }
        
        if let remainCashback = try container.decodeIfPresent(DynamicDataType.self, forKey: .remainCashback) {
            self.remainCashback = remainCashback
        }
        
        if let shoppingAmount = try container.decodeIfPresent(DynamicDataType.self, forKey: .shoppingAmount) {
            self.shoppingAmount = shoppingAmount
        }
    }
}
