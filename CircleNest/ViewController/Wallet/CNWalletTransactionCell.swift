//
//  CNWalletTransactionCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 10/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNWalletTransactionCell: UITableViewCell {
    @IBOutlet weak var height_constraint : NSLayoutConstraint!
    @IBOutlet weak var lblTransactionType: UILabel!
    @IBOutlet weak var imgTransactionIcon: UIImageView!
    @IBOutlet weak var lblTransactionDate: UILabel!
    @IBOutlet weak var lblTransactionAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
