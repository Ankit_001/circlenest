//
//  CNWalletVC.swift
//  CircleNest
//
//  Created by Ankit Gupta on 13/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNWalletVC: UIViewController {
    @IBOutlet weak var lblAvailaibleBalance: UILabel!
    @IBOutlet weak var lblShoppingBalance: UILabel!
    @IBOutlet weak var lblReturnBalance: UILabel!
    @IBOutlet weak var viewHeader : UIView!
    @IBOutlet weak var viewInvestment : UIView!
    @IBOutlet weak var viewReturn : UIView!
    @IBOutlet weak var tablevView : UITableView!
    @IBOutlet weak var btnBackButtonOutlet: UIBarButtonItem!
    var isReturnTap : Bool = false
    var arr_WalletHistory: [walletDetail]? = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false
//        if !isPushed {
//            self.btnBackButtonOutlet.isEnabled = false
//            self.btnBackButtonOutlet.tintColor = .clear
//        }
//        self.totalWalletAmount()
//        self.getWalletHistory()
//        self.setupTableView()
//        self.setupUi()
    }
    override func viewWillAppear(_ animated: Bool) {
        if !isPushed {
            self.btnBackButtonOutlet.isEnabled = false
            self.btnBackButtonOutlet.tintColor = .clear
        }
        self.totalWalletAmount()
        self.getWalletHistory()
        self.setupTableView()
        self.setupUi()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        isPushed = false
    }
    
    func setupTableView(){
        self.tablevView.register(UINib(nibName: "CNWalletTransactionCell", bundle: nil), forCellReuseIdentifier: "CNWalletTransactionCell")
        self.tablevView.register(UINib(nibName: "CNWalletGraphCell", bundle: nil), forCellReuseIdentifier: "CNWalletGraphCell")
        self.tablevView.estimatedRowHeight = 44
        self.tablevView.rowHeight = UITableView.automaticDimension
        
        self.tablevView.tableHeaderView = self.viewHeader
    }
    func setupUi(){
        self.viewReturn.layer.cornerRadius = 5.0
        self.viewReturn.clipsToBounds = true
        self.viewInvestment.layer.cornerRadius = 5.0
        self.viewInvestment.clipsToBounds = true
    }
    
    func totalWalletAmount() {
        self.showHUD(progressLabel: "Loading")
        let vm = CNWalletVM()
        var paramDict  = ["method" : "TotalWallet","storeId" : 101] as [String:AnyObject]
        let userDefaults = UserDefaults.standard
        if let userId = userDefaults.value(forKey: "user_id") {
            paramDict.updateValue(userId as AnyObject, forKey: "userId")
        }
        vm.callTotalWalletAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.lblAvailaibleBalance.text = "₹ \(result?.remainCashback?.type ?? "")"
                self.lblReturnBalance.text = "₹ \(result?.returnValue?.type ?? "")"
                self.lblShoppingBalance.text = "₹ \(result?.shoppingAmount?.type ?? "")"
            } else {
                self.lblAvailaibleBalance.text = "₹ 0.0"
                self.lblReturnBalance.text = "₹ 0.0"
                self.lblShoppingBalance.text = "₹ 0.0"
            }
        }
    }
    
    func getWalletHistory() {
        self.showHUD(progressLabel: "Loading")
        let vm = CNWalletVM()
        var paramDict  = ["method" : "getWalletHistory","storeId" : 101] as [String:AnyObject]
        let userDefaults = UserDefaults.standard
        if let userId = userDefaults.value(forKey: "user_id") {
            paramDict.updateValue(userId as AnyObject, forKey: "userId")
        }
        vm.callGetWalletHistoryAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.arr_WalletHistory = result?.data
                self.tablevView.reloadData()
            } else {
                self.tablevView.isHidden = true
            }
        }
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnInvestmentPressed(_ sender: Any) {
       // let vc = self.storyboard?.instantiateViewController(identifier: "CNInvestmentDetailVC") as! CNInvestmentDetailVC
        let vc = self.storyboard?.instantiateViewController(identifier: "Become A Member") as! CNBecomeMemberVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnReturnPressed(_ sender: Any) {
        self.isReturnTap = !self.isReturnTap
        self.tablevView.reloadData()
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}
extension CNWalletVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isReturnTap{
            return self.arr_WalletHistory?.count ?? 0 + 1
        }else{
            return self.arr_WalletHistory?.count ?? 0
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.isReturnTap{
            if indexPath.row == 0 {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "CNWalletGraphCell", for: indexPath) as? CNWalletGraphCell{
                    return cell
                }
                return UITableViewCell()
                
                
            }else{
                if let cell = tableView.dequeueReusableCell(withIdentifier: "CNWalletTransactionCell", for: indexPath) as? CNWalletTransactionCell{
                    if indexPath.row == 1{
                        cell.height_constraint.constant = 20
                    }else{
                        cell.height_constraint.constant = 0
                    }
                    let data = self.arr_WalletHistory?[indexPath.row - 1]
                    cell.lblTransactionDate.text = data?.date?.type
                    cell.lblTransactionAmount.text = "₹ \(data?.cashbackAmount?.type ?? "")"
                    cell.lblTransactionType.text = data?.category?.type
                    if data?.category?.type?.lowercased() == "Return".lowercased() {
                        cell.imgTransactionIcon.image = UIImage(named: "in.png")
                    } else {
                        cell.imgTransactionIcon.image = UIImage(named: "lifetime.png")
                    }
                    return cell
                }
                return UITableViewCell()
            }
            
            
        }else{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNWalletTransactionCell", for: indexPath) as? CNWalletTransactionCell{
                if indexPath.row == 0{
                    cell.height_constraint.constant = 20
                }else{
                    cell.height_constraint.constant = 0
                }
                let data = self.arr_WalletHistory?[indexPath.row]
                cell.lblTransactionDate.text = data?.date?.type
                cell.lblTransactionAmount.text = "₹ \(data?.cashbackAmount?.type ?? "")"
                cell.lblTransactionType.text = data?.category?.type
                if data?.category?.type?.lowercased() == "Return".lowercased() {
                    cell.imgTransactionIcon.image = UIImage(named: "in.png")
                } else {
                    cell.imgTransactionIcon.image = UIImage(named: "lifetime.png")
                }
                return cell
            }
            
        }
        return UITableViewCell()
        
    }

}
