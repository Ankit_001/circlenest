//
//  CNInvestmentDetailCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 10/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNInvestmentDetailCell: UITableViewCell {
    @IBOutlet weak var viewTrial : UIView!
    @IBOutlet weak var collectionView : UICollectionView!

    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewTrial.layer.cornerRadius = 5.0
        self.viewTrial.layer.borderColor = UIColor.lightGray.cgColor
        self.viewTrial.layer.borderWidth = 1.0
        self.viewTrial.clipsToBounds = true
        self.collectionView.register(UINib(nibName: "CNOwnStoreCollectionCell", bundle: nil), forCellWithReuseIdentifier: "CNOwnStoreCollectionCell")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
extension CNInvestmentDetailCell : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CNOwnStoreCollectionCell", for: indexPath) as? CNOwnStoreCollectionCell{
            return cell
        }
        return UICollectionViewCell()
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: 160, height: 159)
//    }

}
