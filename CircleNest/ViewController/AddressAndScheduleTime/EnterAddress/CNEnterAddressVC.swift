//
//  EnterAddressVC.swift
//  CircleNest
//
//  Created by techjini on 07/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class EnterAddressVC: UIViewController {
    
    @IBOutlet var tfFormField: [UITextField]!
    @IBOutlet weak var btnContinueOutlet: UIButton!
    var popController: CNPPopupController?
    var state_array: [stateList]?
    var city_array: [cityList]?
    var city_pincode: [cityPincodeList]?
    var state_id: String?
    var city_id: String?
    var pincode_id: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.callAddressSaleAPI()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - API CALLS
    
    func callAddressSaleAPI() {
        self.showHUD(progressLabel: "Loading")
        let vm = CNAddressAndScheduleVM()
        var paramDict  = ["method" : "getAddress","storeId" : 101] as [String:AnyObject]
        let userDefaults = UserDefaults.standard
        if let userId = userDefaults.value(forKey: "user_id") {
            paramDict.updateValue(userId as AnyObject, forKey: "userId")
        }
        vm.callGetSaveAddressAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.tfFormField[0].text = result?.house_no
                self.tfFormField[1].text = result?.address
                self.tfFormField[2].text = result?.state
                self.tfFormField[3].text = result?.city
                self.tfFormField[4].text = result?.pincode
            } else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    
    func callStateListAPI() {
        self.showHUD(progressLabel: "Loading")
        let vm = CNAddressAndScheduleVM()
        let paramDict  = ["method" : "getStateList"] as [String:AnyObject]
        vm.callGetStateListAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.state_array = result?.data
                self.stateActionPopUp()
            } else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    
    func callCityListAPI() {
        self.showHUD(progressLabel: "Loading")
        let vm = CNAddressAndScheduleVM()
        let paramDict  = ["method" : "getCityList", "state": self.state_id] as [String:AnyObject]
        vm.callGetCityListAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.city_array = result?.data?.city
                self.cityActionPopUp()
            } else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    
    func callPincodeListAPI() {
        self.showHUD(progressLabel: "Loading")
        let vm = CNAddressAndScheduleVM()
        let paramDict  = ["method" : "getPincode", "state": self.state_id, "City": self.city_id] as [String:AnyObject]
        vm.callGetPincodeListAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.city_pincode = result?.data?.cityPincode
                self.pincodeActionPopUp()
            } else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    
    func callUpdateAddressAPI() {
            
        self.showHUD(progressLabel: "Loading")
        let vm = CNAddressAndScheduleVM()
        var paramDict  = ["method" : "UpdatesaveAdd", "storeId": 101 as AnyObject, "houseno": self.tfFormField[0].text as AnyObject, "address": self.tfFormField[1].text as AnyObject, "state": self.tfFormField[2].text as AnyObject, "city": self.tfFormField[3].text as AnyObject, "pincode": self.tfFormField[4].text as AnyObject] as [String:AnyObject]
        let userDefaults = UserDefaults.standard
        if let userId = userDefaults.value(forKey: "user_id") {
            paramDict.updateValue(userId as AnyObject, forKey: "userId")
        }
        vm.callUpdateAddressAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.navigationController?.popViewController(animated: true)
//                self.city_pincode = result?.data?.cityPincode
            } else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    
    // MARK: - Popup Functions
    
    func stateActionPopUp() {
        let rectShape = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        let view = CNDeliveryTimePopUp(frame: rectShape)
        view.state_array = self.state_array
        view.delegate = self
        view.lblTitleText.text = "State"
        view.lblSubtitleText.text = ""
        view.popUpHeightConstraint.constant = 320
        view.setupUI()
        view.tblPopUpView.reloadData()
        let popupController = CNPPopupController(contents: [view])
        self.popController = popupController
        self.popController?.theme = CNPPopupTheme.default()
        self.popController?.theme.popupStyle = .centered
        self.popController?.delegate = self
        popupController.present(animated: false)
    }
    
    func cityActionPopUp() {
        let rectShape = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        let view = CNDeliveryTimePopUp(frame: rectShape)
        view.city_array = self.city_array
        view.delegate = self
        view.lblTitleText.text = "City"
        view.lblSubtitleText.text = ""
        view.popUpHeightConstraint.constant = 320
        view.setupUI()
        view.tblPopUpView.reloadData()
        let popupController = CNPPopupController(contents: [view])
        self.popController = popupController
        self.popController?.theme = CNPPopupTheme.default()
        self.popController?.theme.popupStyle = .centered
        self.popController?.delegate = self
        popupController.present(animated: false)
    }
    
    func pincodeActionPopUp() {
        let rectShape = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        let view = CNDeliveryTimePopUp(frame: rectShape)
        view.city_pincode = self.city_pincode
        view.delegate = self
        view.lblTitleText.text = "Pincode"
        view.lblSubtitleText.text = ""
        view.popUpHeightConstraint.constant = 320
        view.setupUI()
        view.tblPopUpView.reloadData()
        let popupController = CNPPopupController(contents: [view])
        self.popController = popupController
        self.popController?.theme = CNPPopupTheme.default()
        self.popController?.theme.popupStyle = .centered
        self.popController?.delegate = self
        popupController.present(animated: false)
    }
    
    // MARK: - Button Actions
    
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnStateAction(_ sender: UIButton) {
        self.callStateListAPI()
    }
    
    @IBAction func btnCityAction(_ sender: UIButton) {
        self.callCityListAPI()
    }
    
    @IBAction func btnPincodeAction(_ sender: UIButton) {
        self.callPincodeListAPI()
    }
    
    @IBAction func btnContinueAction(_ sender: UIButton) {
        if self.isValid().validity {
            self.callUpdateAddressAPI()
        } else {
            self.view.makeToast(self.isValid().errorMessage)
        }
    }
    
    // MARK: - Validity check function
    
    func isValid() -> (errorMessage: String, validity: Bool){
        if !(self.tfFormField[0].text?.isValidField ?? false) {
            return (errorMessage: ConstantValue.houseMessage.rawValue, validity: false)
        } else if !(self.tfFormField[1].text?.isValidField ?? false) {
            return (errorMessage: ConstantValue.streetMessage.rawValue, validity: false)
        } else if !(self.tfFormField[2].text?.isValidField ?? false){
            return (errorMessage: ConstantValue.stateMessage.rawValue, validity: false)
        } else if !(self.tfFormField[3].text?.isValidField ?? false){
            return (errorMessage: ConstantValue.cityMessage.rawValue, validity: false)
        } else if !(self.tfFormField[4].text?.isValidField ?? false){
            return (errorMessage: ConstantValue.pincodeMessage.rawValue, validity: false)
        } else {
            return (errorMessage: "", validity: true)
        }
    }
}

extension EnterAddressVC: deliveryTimeDelegate {
    func deliveryTimePresed(timePresed: String?, popUpPresentText: String?, timeIdPresed: String?) {
        if popUpPresentText == "State" {
            self.tfFormField[2].text = timePresed
            self.state_id = timeIdPresed
        } else if popUpPresentText == "City" {
            self.tfFormField[3].text = timePresed
            self.city_id = timeIdPresed
        } else {
            self.tfFormField[4].text = timePresed
        }
        self.popController?.dismiss(animated: false)
    }
    
    func dismissPopup() {
        self.popController?.dismiss(animated: false)
    }
}
extension EnterAddressVC: CNPPopupControllerDelegate {
    
}
