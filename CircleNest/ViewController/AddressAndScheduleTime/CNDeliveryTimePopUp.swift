//
//  CNDeliveryTimePopUp.swift
//  CircleNest
//
//  Created by techjini on 07/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

protocol deliveryTimeDelegate {
    func deliveryTimePresed(timePresed: String?, popUpPresentText: String?, timeIdPresed: String?)
    func dismissPopup()
}

class CNDeliveryTimePopUp: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var lblTitleText: UILabel!
    @IBOutlet weak var lblSubtitleText: UILabel!
    @IBOutlet weak var tblPopUpView: UITableView!
    @IBOutlet weak var subtitleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var popUpHeightConstraint: NSLayoutConstraint!
    
    var state_array: [stateList]?
    var city_array: [cityList]?
    var city_pincode: [cityPincodeList]?
    var data_array: [String]?
    var delegate: deliveryTimeDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("CNDeliveryTimePopUp", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        tblPopUpView.register(UINib(nibName: "CNPopUpAlertCell", bundle: nil), forCellReuseIdentifier: "CNPopUpAlertCell")
    }
    
    func setupUI() {
        if lblSubtitleText.text?.isEmpty ?? false {
            subtitleTopConstraint.constant = 0
        } else {
            subtitleTopConstraint.constant = 15
        }
    }
    @IBAction func btnDismissPopUp(_ sender: UIButton) {
        delegate?.dismissPopup()
    }
    
}

extension CNDeliveryTimePopUp: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.lblTitleText.text == "State" {
            return state_array?.count ?? 0
        } else if self.lblTitleText.text == "City" {
            return city_array?.count ?? 0
        } else if self.lblTitleText.text == "Pincode" {
            return city_pincode?.count ?? 0
        } else {
            return data_array?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CNPopUpAlertCell", for: indexPath) as? CNPopUpAlertCell {
            if self.lblTitleText.text == "State" {
                cell.lblScheduleTimeText.text = state_array?[indexPath.row].state_name?.type
            } else if self.lblTitleText.text == "City" {
                cell.lblScheduleTimeText.text = city_array?[indexPath.row].name?.type
            } else if self.lblTitleText.text == "Pincode" {
                cell.lblScheduleTimeText.text = city_pincode?[indexPath.row].pincode?.type
            } else {
                cell.lblScheduleTimeText.text = data_array?[indexPath.row]
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.lblTitleText.text == "State" {
            delegate?.deliveryTimePresed(timePresed: state_array?[indexPath.row].state_name?.type, popUpPresentText: self.lblTitleText.text, timeIdPresed: state_array?[indexPath.row].id?.type)
        } else if self.lblTitleText.text == "City" {
            delegate?.deliveryTimePresed(timePresed: city_array?[indexPath.row].name?.type, popUpPresentText: self.lblTitleText.text, timeIdPresed: city_array?[indexPath.row].id?.type)
        } else if self.lblTitleText.text == "Pincode" {
            delegate?.deliveryTimePresed(timePresed: city_pincode?[indexPath.row].pincode?.type, popUpPresentText: self.lblTitleText.text, timeIdPresed: city_pincode?[indexPath.row].id?.type)
        } else {
            delegate?.deliveryTimePresed(timePresed: data_array?[indexPath.row], popUpPresentText: self.lblTitleText.text, timeIdPresed: "")
        }
    }
}
