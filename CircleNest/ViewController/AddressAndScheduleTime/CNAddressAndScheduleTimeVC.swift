//
//  CNAddressAndScheduleTimeVC.swift
//  CircleNest
//
//  Created by techjini on 07/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNAddressAndScheduleTimeVC: UIViewController {

    @IBOutlet weak var tableViewAddressAndScheduleTime: UITableView!
    @IBOutlet weak var lblTotalOrderAmount: UILabel!
    var popController: CNPPopupController?
    var selectedTime: String = "Select Delivery Time"
    var order_Amount: Double?
    var addressUserName: String?
    var addressValue: String?
    var addressPincode: String?
    var array_main : [CategoryData] = []
    var address: String? = ""
    var state: String? = ""
    var city: String? = ""
    var delivery_Date: String? = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblTotalOrderAmount.text = "₹ \(String(format: "%.2f", order_Amount ?? 0.0))"
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.callAddressSaleAPI()
    }
    
    func setupUI() {
        self.tableViewAddressAndScheduleTime.register(UINib(nibName: "CNDeliveryTimeCell", bundle: nil), forCellReuseIdentifier: "CNDeliveryTimeCell")
        self.tableViewAddressAndScheduleTime.register(UINib(nibName: "CNAddressDetailCell", bundle: nil), forCellReuseIdentifier: "CNAddressDetailCell")
    }
    
    func callAddressSaleAPI() {
        self.showHUD(progressLabel: "Loading")
        let vm = CNAddressAndScheduleVM()
        var paramDict  = ["method" : "getAddress","storeId" : 101] as [String:AnyObject]
        let userDefaults = UserDefaults.standard
        if let userId = userDefaults.value(forKey: "user_id") {
            paramDict.updateValue(userId as AnyObject, forKey: "userId")
        }
        vm.callGetSaveAddressAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                let userDefaults = UserDefaults.standard
                self.addressUserName = userDefaults.value(forKey: "user_name") as? String
                self.addressPincode = result?.pincode
                self.address = result?.address
                self.state = result?.state
                self.city = result?.city
                self.addressValue = "\(result?.house_no ?? ""),\(result?.address ?? ""),\(result?.city ?? ""),\(result?.state ?? ""),\nPincode - \(result?.pincode ?? "")"
                self.tableViewAddressAndScheduleTime.reloadData()
            } else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    
    @IBAction func btnProceedToCheckout(_ sender: UIButton) {
        if self.addressPincode?.isEmpty ?? false {
            self.view.makeToast("Please enter your current address")
        } else if self.selectedTime == "Select Delivery Time" {
            self.view.makeToast("Please enter delivery time")
        } else {
            let storyboard = UIStoryboard(name: "CircleNest", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "CNPaymentOptionsVC") as? CNPaymentOptionsVC
            vc?.order_Amount = self.order_Amount ?? 0.0
            vc?.array_main = self.array_main
            vc?.pincode = self.addressPincode
            vc?.address = self.address
            vc?.state = self.state
            vc?.city = self.city
            vc?.delivery_Date = self.delivery_Date
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension CNAddressAndScheduleTimeVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNAddressDetailCell", for: indexPath) as? CNAddressDetailCell {
                cell.delegate = self
                cell.lblUserAddressName.text = self.addressUserName
                cell.lblUserAddress.text = self.addressValue
                cell.lblUserAddressPincode.text = ""
                return cell
            }
            return UITableViewCell()
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CNDeliveryTimeCell", for: indexPath) as? CNDeliveryTimeCell {
                cell.delegate = self
                cell.lblSelectDeliveryTime.text = self.selectedTime
                return cell
            }
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}


extension CNAddressAndScheduleTimeVC: addressChangeActionDelegate, deliveryTimeActionDelegate {
    
    func addressButtonPresed() {
        let storyboard = UIStoryboard(name: "CircleNest", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "EnterAddressVC") as? EnterAddressVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func deliveryTimePresed() {
        let rectShape = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        let view = CNDeliveryTimePopUp(frame: rectShape)
        view.data_array = ["Current Delivery","Tomorrow 9:30 AM-12.00 PM","Tomorrow 12.00 PM-4.00 PM","Tomorrow 5.00 PM-7:30 PM","Tomorrow 8.00 PM-10:30 PM"]
        view.delegate = self
        view.lblTitleText.text = "Select Delivery Time"
        view.lblSubtitleText.text = ""
        view.popUpHeightConstraint.constant = 320
        view.setupUI()
        view.tblPopUpView.reloadData()
        let popupController = CNPPopupController(contents: [view])
        self.popController = popupController
        self.popController?.theme = CNPPopupTheme.default()
        self.popController?.theme.popupStyle = .centered
        self.popController?.delegate = self
        popupController.present(animated: false)
    }
}

extension CNAddressAndScheduleTimeVC: deliveryTimeDelegate {
    func deliveryTimePresed(timePresed: String?, popUpPresentText: String?, timeIdPresed: String?) {
        self.selectedTime = timePresed ?? ""
        if timePresed?.contains("Tomorrow") ?? false {
            self.delivery_Date = Date.tomorrow.string(format: "dd-MMM-yyyy")
        } else {
            self.delivery_Date = Date().string(format: "dd-MMM-yyyy")
        }
        self.popController?.dismiss(animated: false)
        self.tableViewAddressAndScheduleTime.reloadData()
    }
    
    func dismissPopup() {
        self.popController?.dismiss(animated: false)
    }
}

extension CNAddressAndScheduleTimeVC: CNPPopupControllerDelegate {
    
}
