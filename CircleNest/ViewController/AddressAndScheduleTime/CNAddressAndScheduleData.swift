//
//  CNAddressAndScheduleData.swift
//  CircleNest
//
//  Created by techjini on 08/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import Foundation

public class CNStateListModel : Codable {
    public var responseCode : Int?
    public var data : [stateList]?
    
    private enum CodingKeys : String, CodingKey {
        case responseCode = "responseCode", data = "data"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let responseCode = try container.decodeIfPresent(Int.self, forKey: .responseCode) {
            self.responseCode = responseCode
        }
        
        if let data = try container.decodeIfPresent([stateList].self, forKey: .data) {
            self.data = data
        }
    }

}

public class stateList : Codable {
    public var state_name : DynamicDataType?
    public var id : DynamicDataType?
    
    private enum CodingKeys : String, CodingKey {
        case state_name = "state_name", id = "id"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        if let state_name = try container.decodeIfPresent(DynamicDataType.self, forKey: .state_name) {
            self.state_name = state_name
        }
        if let id = try container.decodeIfPresent(DynamicDataType.self, forKey: .id) {
            self.id = id
        }
    }

}

public class CNCityListModel : Codable {
    public var responseCode : Int?
    public var data : cityDict?
    
    private enum CodingKeys : String, CodingKey {
        case responseCode = "responseCode", data = "data"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let responseCode = try container.decodeIfPresent(Int.self, forKey: .responseCode) {
            self.responseCode = responseCode
        }
        
        if let data = try container.decodeIfPresent(cityDict.self, forKey: .data) {
            self.data = data
        }
    }

}

public class cityDict : Codable {
    public var city : [cityList]?
    
    private enum CodingKeys : String, CodingKey {
        case city = "city"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        if let city = try container.decodeIfPresent([cityList].self, forKey: .city) {
            self.city = city
        }
    }

}

public class cityList : Codable {
    public var name : DynamicDataType?
    public var id : DynamicDataType?
    
    private enum CodingKeys : String, CodingKey {
        case name = "name", id = "id"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        if let name = try container.decodeIfPresent(DynamicDataType.self, forKey: .name) {
            self.name = name
        }
        if let id = try container.decodeIfPresent(DynamicDataType.self, forKey: .id) {
            self.id = id
        }
    }

}

public class CNPincodeListModel : Codable {
    public var responseCode : Int?
    public var data : pincodeDict?
    
    private enum CodingKeys : String, CodingKey {
        case responseCode = "responseCode", data = "data"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let responseCode = try container.decodeIfPresent(Int.self, forKey: .responseCode) {
            self.responseCode = responseCode
        }
        
        if let data = try container.decodeIfPresent(pincodeDict.self, forKey: .data) {
            self.data = data
        }
    }

}

public class pincodeDict : Codable {
    public var cityPincode : [cityPincodeList]?
    
    private enum CodingKeys : String, CodingKey {
        case cityPincode = "CityPincode"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        if let cityPincode = try container.decodeIfPresent([cityPincodeList].self, forKey: .cityPincode) {
            self.cityPincode = cityPincode
        }
    }

}

public class cityPincodeList : Codable {
    public var pincode : DynamicDataType?
    public var id : DynamicDataType?
    
    private enum CodingKeys : String, CodingKey {
        case pincode = "pincode", id = "id"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        if let pincode = try container.decodeIfPresent(DynamicDataType.self, forKey: .pincode) {
            self.pincode = pincode
        }
        if let id = try container.decodeIfPresent(DynamicDataType.self, forKey: .id) {
            self.id = id
        }
    }
}

public class CNSavedAddressModel : Codable {
    public var responseCode : Int?
    public var data : addressData?
    public var msg : String?
    
    private enum CodingKeys : String, CodingKey {
        case responseCode = "responseCode", data = "data", msg = "msg"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let responseCode = try container.decodeIfPresent(Int.self, forKey: .responseCode) {
            self.responseCode = responseCode
        }
        
        if let data = try container.decodeIfPresent(addressData.self, forKey: .data) {
            self.data = data
        }
        
        if let msg = try container.decodeIfPresent(String.self, forKey: .msg) {
            self.msg = msg
        }
    }

}

public class addressData : Codable {
    public var city : DynamicDataType?
    public var address : DynamicDataType?
    public var houseno : DynamicDataType?
    public var pincode : DynamicDataType?
    public var state : DynamicDataType?
    
    private enum CodingKeys : String, CodingKey {
        case city = "city", address = "address", houseno = "houseno", pincode = "pincode", state = "state"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        if let city = try container.decodeIfPresent(DynamicDataType.self, forKey: .city) {
            self.city = city
        }
        if let address = try container.decodeIfPresent(DynamicDataType.self, forKey: .address) {
            self.address = address
        }
        if let houseno = try container.decodeIfPresent(DynamicDataType.self, forKey: .houseno) {
            self.houseno = houseno
        }
        if let pincode = try container.decodeIfPresent(DynamicDataType.self, forKey: .pincode) {
            self.pincode = pincode
        }
        if let state = try container.decodeIfPresent(DynamicDataType.self, forKey: .state) {
            self.state = state
        }
    }

}
