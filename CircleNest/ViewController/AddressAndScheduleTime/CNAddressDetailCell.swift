//
//  CNAddressDetailCell.swift
//  CircleNest
//
//  Created by techjini on 07/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

protocol addressChangeActionDelegate {
    func addressButtonPresed()
}

class CNAddressDetailCell: UITableViewCell {

    @IBOutlet weak var lblUserAddressName: UILabel!
    @IBOutlet weak var lblUserAddress: UILabel!
    @IBOutlet weak var lblUserAddressPincode: UILabel!
    @IBOutlet weak var lblChangeButtonOutlet: UIButton!
    
    var delegate: addressChangeActionDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnAddressChangeAction(_ sender: UIButton) {
        delegate?.addressButtonPresed()
    }
    
}
