//
//  CNDeliveryTimeCell.swift
//  CircleNest
//
//  Created by techjini on 07/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

protocol deliveryTimeActionDelegate {
    func deliveryTimePresed()
}

class CNDeliveryTimeCell: UITableViewCell {

    @IBOutlet weak var lblSelectDeliveryTime: UILabel!
    @IBOutlet weak var btnDeliveryTimeSelection: UIButton!
    
    var delegate: deliveryTimeActionDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnDeliveryTimeSelectionAction(_ sender: UIButton) {
        delegate?.deliveryTimePresed()
    }
    
    
}
