//
//  CNOwnAStoreFormVC.swift
//  CircleNest
//
//  Created by techjini on 09/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNOwnAStoreFormVC: UIViewController {
    
    @IBOutlet var tfScheduleForm: [UITextField]!
    @IBOutlet weak var btnConfirmScheduleForm: UIButton!
    
    var datePicker :UIDatePicker!
    var pickerView :UIPickerView!
    var arrOwnAStoreCategory: [String] = ["Open Your Plant Nursery","Earn From Your Space","Business From Your Home"]
    var strSelectedPickerData: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDatePickerForTextField()
        self.setupPickerViewForTextField()
    }
    
    @IBAction func btnBackPressed(_sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnConfirmScheduleFormAction(_ sender: UIButton) {
        
        if self.isValid().validity {
            self.showHUD(progressLabel: "Loading")
            let vm = CNOwnAStoreFormVM()
            let paramDict = self.gettingRequest()
            vm.callSaveOpenStoreFormAPI(param: paramDict) { (success, response) in
                self.dismissHUD(isAnimated: true)
                if success {
                    let storyboard = UIStoryboard(name: "CircleNest", bundle: nil)
                    let vc = storyboard.instantiateViewController(identifier: "CNThankYouVC") as? CNThankYouVC
                    self.navigationController?.pushViewController(vc!, animated: false)
                } else {
                    Utill.showAlertWithMessage(title: ConstantValue.title.rawValue, Message: response ?? "Something went wrong", viewController: self)
                }
            }
        } else {
            Utill.showAlertWithMessage(title: ConstantValue.title.rawValue, Message: self.isValid().errorMessage, viewController: self)
        }
    }
    
    func setupDatePickerForTextField() {
        datePicker = UIDatePicker.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 200))
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
        tfScheduleForm[4].inputView = datePicker
        let doneButton = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(self.datePickerDone))
        let toolBar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 44))
        toolBar.setItems([UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil), doneButton], animated: true)
        tfScheduleForm[4].inputAccessoryView = toolBar
    }
    
    func setupPickerViewForTextField() {
        pickerView = UIPickerView.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 200))
        tfScheduleForm[0].inputView = pickerView
        pickerView.delegate = self
        pickerView.dataSource = self
        let doneButton = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(self.pickerDone))
        let toolBar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 44))
        toolBar.setItems([UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil), doneButton], animated: true)
        tfScheduleForm[0].inputAccessoryView = toolBar
    }
    
    @objc func pickerDone() {
        if tfScheduleForm[0].text?.isEmpty ?? false, arrOwnAStoreCategory.count > 0 {
            tfScheduleForm[0].text = arrOwnAStoreCategory.first
        } else {
            tfScheduleForm[0].text = strSelectedPickerData
        }
        tfScheduleForm[0].resignFirstResponder()
    }
    
    @objc func datePickerDone() {
        datePicker.datePickerMode = UIDatePicker.Mode.date
        let selectedDate = Utill.settingDateFormatForDate(dateFormat: "MM/dd/yyyy", dateValue: datePicker.date)
        tfScheduleForm[4].text = "\(selectedDate)"
        tfScheduleForm[4].resignFirstResponder()
    }
    
}

extension CNOwnAStoreFormVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrOwnAStoreCategory.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.arrOwnAStoreCategory[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        strSelectedPickerData = self.arrOwnAStoreCategory[row]
    }
}

extension CNOwnAStoreFormVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
}

extension CNOwnAStoreFormVC {
    
    func isValid() -> (errorMessage: String, validity: Bool){
        if !(tfScheduleForm[0].text?.isValidField ?? false) {
            return (errorMessage: ConstantValue.storeAlertMessage.rawValue, validity: false)
        } else if !(tfScheduleForm[1].text?.isValidField ?? false) {
            return (errorMessage: ConstantValue.nameAlertMessage.rawValue, validity: false)
        } else if !(tfScheduleForm[2].text?.isValidEmail ?? false) {
            return (errorMessage: ConstantValue.emailAlertMessage.rawValue, validity: false)
        } else if !(tfScheduleForm[3].text?.isValidPhoneNumberMBL ?? false){
            return (errorMessage: ConstantValue.mobileAlertMessage.rawValue, validity: false)
        } else if !(tfScheduleForm[4].text?.isValidField ?? false) {
            return (errorMessage: ConstantValue.dobAlertMessage.rawValue, validity: false)
        } else if !(tfScheduleForm[5].text?.isValidField ?? false) {
            return (errorMessage: ConstantValue.addressAlertMessage.rawValue, validity: false)
        } else if !(tfScheduleForm[6].text?.isValidField ?? false) {
            return (errorMessage: ConstantValue.cityAlertMessage.rawValue, validity: false)
        } else if !(tfScheduleForm[7].text?.isValidField ?? false) {
            return (errorMessage: ConstantValue.stateAlertMessage.rawValue, validity: false)
        } else if !(tfScheduleForm[8].text?.isValidField ?? false) {
            return (errorMessage: ConstantValue.pincodeAlertMessage.rawValue, validity: false)
        } else if !(tfScheduleForm[9].text?.isValidField ?? false) {
            return (errorMessage: ConstantValue.feedbackAlertMessage.rawValue, validity: false)
        } else {
            return (errorMessage: "", validity: true)
        }
    }
    
    func gettingRequest() -> [String:AnyObject] {
        
        var paramDict: [String:AnyObject]
        paramDict = ["method": "SaveOpenStore"] as [String:AnyObject]
        paramDict.updateValue(101 as AnyObject, forKey: "storeId")
        if let StoreName = tfScheduleForm[0].text {
            paramDict.updateValue(StoreName as AnyObject, forKey: "StoreName")
        }
        
        if let Name = tfScheduleForm[1].text {
            paramDict.updateValue(Name as AnyObject, forKey: "Name")
        }
        
        if let Email = tfScheduleForm[2].text {
            paramDict.updateValue(Email as AnyObject, forKey: "Email")
        }
        
        if let Mobile = tfScheduleForm[3].text {
            paramDict.updateValue(Mobile as AnyObject, forKey: "Mobile")
        }
        
        if let Dob = tfScheduleForm[4].text {
            paramDict.updateValue(Dob as AnyObject, forKey: "Dob")
        }
        
        if let Address = tfScheduleForm[5].text {
            paramDict.updateValue(Address as AnyObject, forKey: "Address")
        }
        
        if let City = tfScheduleForm[6].text {
            paramDict.updateValue(City as AnyObject, forKey: "City")
        }
        
        if let State = tfScheduleForm[7].text {
            paramDict.updateValue(State as AnyObject, forKey: "State")
        }
        
        if let PinCode = tfScheduleForm[8].text {
            paramDict.updateValue(PinCode as AnyObject, forKey: "PinCode")
        }
        
        if let Feedback = tfScheduleForm[9].text {
            paramDict.updateValue(Feedback as AnyObject, forKey: "Feedback")
        }
        
        return paramDict as [String:AnyObject]
        
    }
    
}
