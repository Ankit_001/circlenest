//
//  CNSubcategoryItemCollectionCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 23/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNSubcategoryItemCollectionCell: UICollectionViewCell {
    @IBOutlet weak var imgProduct : UIImageView!
    @IBOutlet weak var lblName : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
