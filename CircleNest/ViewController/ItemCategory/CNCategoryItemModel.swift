//
//  CNCategoryItemModel.swift
//  CircleNest
//
//  Created by Ankit Gupta on 21/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNCategoryItemModel: Codable {
    public var responseCode : Int?
    public var msg : String?
    public var data : [item]?
}


public class item : Codable {
    let id : DynamicDataType?
    let name : DynamicDataType?
    let image : DynamicDataType?
}
