//
//  CNSubcategoryItemModal.swift
//  CircleNest
//
//  Created by Ankit Gupta on 23/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNSubcategoryItemModal: Codable {
    public var responseCode : Int?
    public var msg : String?
    public var data : [sub_item]?
}

public class sub_item : Codable {
    let id : String?
    let name : String?
    let image : String?
}
