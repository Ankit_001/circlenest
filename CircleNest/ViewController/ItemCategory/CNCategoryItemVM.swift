//
//  CNCategoryItemVM.swift
//  CircleNest
//
//  Created by Ankit Gupta on 21/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
import Alamofire

class CNCategoryItemVM: NSObject {
    func callsubcategoryBannerAPI (param:[String:Any],completionHandler: @escaping (_ success: Bool, _ message: String, _ response : CNSubCategoryModel?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    if let dict = json["msg"] as? [String:Any]{
                        let result = Utill.parseObject(CNSubCategoryModel.self, data: dict)
                        completionHandler(true,"",result)
                    }
                }
            case .failure :
                completionHandler(false,"",nil)
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
                           
            }
        }
    }
    func callItemListAPI (param:[String:Any],completionHandler: @escaping (_ success: Bool, _ message: String, _ response : CNCategoryItemModel?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
                print(response)
                switch response.result {
                case .success :
                    let jsonData = response.data
                    guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                        return
                    }
                    DispatchQueue.main.async {
                        if let dict = json["msg"] as? [String:Any]{
                            let result = Utill.parseObject(CNCategoryItemModel.self, data: dict)
                            completionHandler(true,"",result)
                        }
                    }
                case .failure :
                    completionHandler(false,"",nil)
                    Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
                    
            }
        }
    }
    func callSubcategoryItemListAPI (param:[String:Any],completionHandler: @escaping (_ success: Bool, _ message: String, _ response : CNSubcategoryItemModal?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
                print(response)
                switch response.result {
                case .success :
                    let jsonData = response.data
                    guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                        return
                    }
                    DispatchQueue.main.async {
                        if let dict = json["msg"] as? [String:Any]{
                            let result = Utill.parseObject(CNSubcategoryItemModal.self, data: dict)
                            completionHandler(true,"",result)
                        }
                    }
                case .failure :
                    Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
                    
            }
        }
    }
    

}
