//
//  CNSubCategoryModel.swift
//  CircleNest
//
//  Created by Ankit Gupta on 21/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNSubCategoryModel: Codable {
    public var responseCode : Int?
    public var data : [subcat_banner]?
    public var msg : String?

}


public class subcat_banner : Codable {
    public let catbanner : String?
    public let id : String?
}
