//
//  CNTableViewHeader.swift
//  CircleNest
//
//  Created by Ankit Gupta on 01/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit


class CNTableViewHeader: UIView {
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var viewDiscount : UIView!
    let app = UIApplication.shared.delegate as! AppDelegate
    
    var total : String?
    
    var array_banner: [subcat_banner]?{
        didSet{
            app.array_subcatBanner = array_banner
        }
    }

    override func awakeFromNib() {
        self.viewDiscount.layer.cornerRadius = 5.0
        self.viewDiscount.clipsToBounds = true
        self.registerCell()
        print(self.array_banner?.count ?? 0)
        self.array_banner = app.array_subcatBanner
        print(self.array_banner?.count ?? 0)
        
    }
    func registerCell(){
        self.collectionView.register(UINib(nibName: "CNCollectionBannerAdCellCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CNCollectionBannerAdCellCollectionViewCell")
    }
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CNTableViewHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

}
extension CNTableViewHeader : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.array_banner?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CNCollectionBannerAdCellCollectionViewCell", for: indexPath) as? CNCollectionBannerAdCellCollectionViewCell{
            let image = self.array_banner?[indexPath.row].catbanner?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            cell.imgViewBanner.sd_setImage(with: URL(string: image ?? ""), placeholderImage: UIImage(named: "Placeholder"))
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.collectionView.frame.size.width
        return CGSize(width: width, height: 170)
    }

    
}
