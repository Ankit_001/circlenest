//
//  CNItemListHeaderTableViewCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 22/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
protocol ClickSubcategoryDelegate {
    func subCategoryItemPressed(indexPath : IndexPath , array : [sub_item]?)
}


class CNItemListHeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var viewCard : UIView!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDesc : UILabel!
    @IBOutlet weak var imgView : UIImageView!

    
    @IBOutlet weak var btnArrow : UIButton!
    @IBOutlet weak var collectionView : UICollectionView!
    var array_item : [sub_item]?
    var delegate : ClickSubcategoryDelegate?
    var indexPath : IndexPath!

    override func awakeFromNib() {
        
        super.awakeFromNib()
        viewCard.layer.cornerRadius = 5.0
        viewCard.layer.masksToBounds = false
        viewCard.layer.shadowColor = UIColor.black.cgColor
        viewCard.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        viewCard.layer.shadowRadius = 5.0
        viewCard.layer.shadowOpacity = 0.3
        self.registerCollectionCell()

    }
    func registerCollectionCell(){
        self.collectionView.register(UINib(nibName: "CNSubcategoryItemCollectionCell", bundle: nil), forCellWithReuseIdentifier: "CNSubcategoryItemCollectionCell")
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
extension CNItemListHeaderTableViewCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array_item?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CNSubcategoryItemCollectionCell", for: indexPath) as? CNSubcategoryItemCollectionCell{
            cell.lblName.text = self.array_item?[indexPath.item].name
            let image_name = self.array_item?[indexPath.item].image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            
            cell.imgProduct.sd_setImage(with: URL(string: image_name ?? ""), placeholderImage: UIImage(named: "Placeholder"))
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width/3-10
        return CGSize(width: width, height: 145)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.subCategoryItemPressed(indexPath: indexPath, array: self.array_item)
    }
    
}
