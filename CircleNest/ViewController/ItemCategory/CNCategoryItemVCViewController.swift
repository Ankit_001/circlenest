//
//  CNCategoryItemVCViewController.swift
//  CircleNest
//
//  Created by Ankit Gupta on 22/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNCategoryItemVCViewController: CNBaseViewController {
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var imgNoCategoryAvailable: UIImageView!
    var selectedIndex : Int!
    var collectionCell_height : Float = 145.0
    var dict_subcat : Category?
    var array_item : [item]?
    
    var array_banner : [subcat_banner]?
    var array_subitem : [sub_item]?
    
    var array_check : [Int] = []
    var array_main = NSMutableArray()
    var selected_row : Int!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.callsubCategoryBannerApi(_id: self.dict_subcat?.id ?? "0")
        titleLabel.text = dict_subcat?.name
        self.registerCell()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setItemCount()
    }
    func callsubCategoryBannerApi(_id : String){
        let vm = CNCategoryItemVM()
        let paramDict  = ["method" : "catBanner","storeId" : 101,"catId" : _id] as [String:AnyObject]
        vm.callsubcategoryBannerAPI(param: paramDict) { (success, message,response) in
            if success && message.isEmpty {
                self.array_banner = response?.data
                let view =  CNTableViewHeader()
                view.array_banner = self.array_banner
                let header_view =  CNTableViewHeader.instanceFromNib()
                if self.array_banner?.count ?? 0 > 0{
                   self.tableView.tableHeaderView = header_view
                }else{
                    self.tableView.tableHeaderView = nil
                }
                self.callCategoryItemAPI(_id: self.dict_subcat?.id ?? "0")
            }
        }
    }
    func callCategoryItemAPI(_id : String){
        self.showHUD(progressLabel: "Loading")
        let vm = CNCategoryItemVM()
        let paramDict  = ["method" : "getSubCategoryList","storeId" : 101,"catId" : _id] as [String:AnyObject]
        vm.callItemListAPI(param: paramDict) { (success, message,response) in
            if success && message.isEmpty {
                self.dismissHUD(isAnimated: true)
                self.array_item = response?.data
                print(self.array_item?.count ?? 0)
                if self.array_item?.count ?? 0 > 0{
                    self.tableView.reloadData()
                } else {
                    if self.array_banner?.isEmpty ?? false {
                        self.tableView.isHidden = true
                        if self.dict_subcat?.name?.lowercased() == "Sweets".lowercased() {
                            self.imgNoCategoryAvailable.image = UIImage(named: "nosweet.jpeg")
                        } else if self.dict_subcat?.name?.lowercased() == "Restaurants".lowercased() {
                            self.imgNoCategoryAvailable.image = UIImage(named: "norest.jpeg")
                        }
                    }
                }
            }
        }
    }
    func registerCell(){
        self.tableView.register(UINib(nibName: "CNItemListHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "CNItemListHeaderTableViewCell")
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sizeHeaderToFit()
    }
    func sizeHeaderToFit(){
        if let headerView = tableView.tableHeaderView {
            headerView.setNeedsLayout()
            headerView.layoutIfNeeded()
            let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var frame = headerView.frame
            frame.size.height = height
            headerView.frame = frame
            tableView.tableHeaderView = headerView
        }
    }
    @IBAction func btnBackPressed(_sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    func callSubCategoryItemAPI(_id : String){
        self.showHUD(progressLabel: "Loading")
        let vm = CNCategoryItemVM()
        let paramDict  = ["method" : "getSubCategoryList","storeId" : 101,"catId" : _id] as [String:AnyObject]
        vm.callSubcategoryItemListAPI(param: paramDict) { (success, message,response) in
            if success && message.isEmpty {
                self.dismissHUD(isAnimated: true)
                self.array_subitem = response?.data
                print(self.array_subitem?.count ?? 0)
                self.array_main.add(self.array_subitem ?? [])
                print(self.array_main.count)
                let ip = IndexPath(row: self.selected_row, section: 0)
                self.tableView.reloadRows(at: [ip], with: UITableView.RowAnimation.none)
            }
        }
    }
}

extension CNCategoryItemVCViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.array_item?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CNItemListHeaderTableViewCell", for: indexPath) as? CNItemListHeaderTableViewCell{
            cell.selectionStyle = .none
            cell.indexPath = indexPath
            cell.lblTitle.text = self.array_item?[indexPath.row].name?.type
            let image = self.array_item?[indexPath.row].image?.type?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            cell.imgView.sd_setImage(with: URL(string: image ?? ""), placeholderImage: UIImage(named: "Placeholder"))
            
            for i in 0..<self.array_check.count{
                if array_check[i] == indexPath.row{
                    let arr = self.array_main[i] as? [sub_item]
                    cell.array_item = arr
                    cell.delegate = self
                    print(self.array_subitem?.count ?? 0)
                    cell.collectionView.reloadData()
                }
            }
            return cell

        }else{
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selected_row = indexPath.row
        if self.array_check.contains(selected_row) {
            if let index = self.array_check.firstIndex(of: selected_row){
                self.array_check.remove(at: index)
                self.array_main.removeObject(at: index)
            }
            self.tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)

        }else{
            self.array_check.append(selected_row)
            let id  = self.array_item?[indexPath.row].id
            self.callSubCategoryItemAPI(_id: id?.type ?? "")
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.array_check.count>0{
            for i in 0..<self.array_check.count{
                if self.array_check[i] == indexPath.row{
                    let arr = self.array_main[i] as? [sub_item]
                    let total_row = Float(arr?.count ?? 0) / 3.0
                    let totalrow_roundOff = ceilf(total_row)
                    return CGFloat(totalrow_roundOff*collectionCell_height)+125
                }
            }
        }
        return 125

    }
   
}
extension CNCategoryItemVCViewController : ClickSubcategoryDelegate{
    func subCategoryItemPressed(indexPath: IndexPath, array: [sub_item]?) {
        if let array_item = array{
            let dict_item = array_item[indexPath.row]
            let vc = self.storyboard?.instantiateViewController(identifier: "CNSubcategoryItemVC") as? CNSubcategoryItemVC
            vc?.dict_subitem = dict_item
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        
    }
    
    

}
 
