//
//  CNMyOrderListData.swift
//  CircleNest
//
//  Created by techjini on 02/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import Foundation

public class CNOrderListModel : Codable {
    public var responseCode : Int?
    public var data : [orderData]?
    public var msg : String?
    
    private enum CodingKeys : String, CodingKey {
        case responseCode = "responseCode", data = "data", msg = "msg"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let responseCode = try container.decodeIfPresent(Int.self, forKey: .responseCode) {
            self.responseCode = responseCode
        }
        
        if let msg = try container.decodeIfPresent(String.self, forKey: .msg) {
            self.msg = msg
        }
        if let data = try container.decodeIfPresent([orderData].self, forKey: .data) {
            self.data = data
        }
    }

}

public class orderData : Codable {
    public var cashOnDelivery : DynamicDataType?
    public var TotalAmount : DynamicDataType?
    public var id : DynamicDataType?
    public var userId : DynamicDataType?
    public var cod : DynamicDataType?
    public var couponName : DynamicDataType?
    public var couponPrice : DynamicDataType?
    public var couponType : DynamicDataType?
    public var couponTypeValue : DynamicDataType?
    public var deliverydate : DynamicDataType?
    public var invoiceNo : DynamicDataType?
    public var isCouponApplied : DynamicDataType?
    public var orderAmount : DynamicDataType?
    public var orderDate : DynamicDataType?
    public var orderStatus : DynamicDataType?
    public var pickup : DynamicDataType?
    public var shippingAmount : DynamicDataType?
    public var shopAddress : DynamicDataType?
    public var shopId : DynamicDataType?
    public var storeId : DynamicDataType?
    public var sub_total : DynamicDataType?
    public var useCashback : DynamicDataType?
    public var userName : DynamicDataType?
    public var paid_amount : DynamicDataType?
    
    private enum CodingKeys : String, CodingKey {
        case cashOnDelivery = "Cash on Delivery", TotalAmount = "TotalAmount", id = "id", userId = "userId", storeId = "storeId", cod = "cod", paid_amount = "paid_amount", sub_total = "sub_total", pickup = "pickup", orderAmount = "orderAmount", shippingAmount = "shippingAmount", deliverydate = "deliverydate", useCashback = "useCashback", orderStatus = "orderStatus", shopId = "shopId", invoiceNo = "invoiceNo", isCouponApplied = "isCouponApplied", couponName = "couponName", couponPrice = "couponPrice", couponType = "couponType", couponTypeValue = "couponTypeValue", shopAddress = "shopAddress", orderDate = "orderDate", userName = "userName"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        if let cashOnDelivery = try container.decodeIfPresent(DynamicDataType.self, forKey: .cashOnDelivery) {
            self.cashOnDelivery = cashOnDelivery
        }
        if let TotalAmount = try container.decodeIfPresent(DynamicDataType.self, forKey: .TotalAmount) {
            self.TotalAmount = TotalAmount
        }
        if let id = try container.decodeIfPresent(DynamicDataType.self, forKey: .id) {
            self.id = id
        }
        if let userId = try container.decodeIfPresent(DynamicDataType.self, forKey: .userId) {
            self.userId = userId
        }
        if let cod = try container.decodeIfPresent(DynamicDataType.self, forKey: .cod) {
            self.cod = cod
        }
        if let couponName = try container.decodeIfPresent(DynamicDataType.self, forKey: .couponName) {
            self.couponName = couponName
        }
        if let couponPrice = try container.decodeIfPresent(DynamicDataType.self, forKey: .couponPrice) {
            self.couponPrice = couponPrice
        }
        if let couponType = try container.decodeIfPresent(DynamicDataType.self, forKey: .couponType) {
            self.couponType = couponType
        }
        if let couponTypeValue = try container.decodeIfPresent(DynamicDataType.self, forKey: .couponTypeValue) {
            self.couponTypeValue = couponTypeValue
        }
        if let deliverydate = try container.decodeIfPresent(DynamicDataType.self, forKey: .deliverydate) {
            self.deliverydate = deliverydate
        }
        if let invoiceNo = try container.decodeIfPresent(DynamicDataType.self, forKey: .invoiceNo) {
            self.invoiceNo = invoiceNo
        }
        if let isCouponApplied = try container.decodeIfPresent(DynamicDataType.self, forKey: .isCouponApplied) {
            self.isCouponApplied = isCouponApplied
        }
        if let orderAmount = try container.decodeIfPresent(DynamicDataType.self, forKey: .orderAmount) {
            self.orderAmount = orderAmount
        }
        if let orderDate = try container.decodeIfPresent(DynamicDataType.self, forKey: .orderDate) {
            self.orderDate = orderDate
        }
        if let orderStatus = try container.decodeIfPresent(DynamicDataType.self, forKey: .orderStatus) {
            self.orderStatus = orderStatus
        }
        if let pickup = try container.decodeIfPresent(DynamicDataType.self, forKey: .pickup) {
            self.pickup = pickup
        }
        if let shippingAmount = try container.decodeIfPresent(DynamicDataType.self, forKey: .shippingAmount) {
            self.shippingAmount = shippingAmount
        }
        if let shopAddress = try container.decodeIfPresent(DynamicDataType.self, forKey: .shopAddress) {
            self.shopAddress = shopAddress
        }
        if let shopId = try container.decodeIfPresent(DynamicDataType.self, forKey: .shopId) {
            self.shopId = shopId
        }
        if let storeId = try container.decodeIfPresent(DynamicDataType.self, forKey: .storeId) {
            self.storeId = storeId
        }
        if let sub_total = try container.decodeIfPresent(DynamicDataType.self, forKey: .sub_total) {
            self.sub_total = sub_total
        }
        if let useCashback = try container.decodeIfPresent(DynamicDataType.self, forKey: .useCashback) {
            self.useCashback = useCashback
        }
        if let userName = try container.decodeIfPresent(DynamicDataType.self, forKey: .userName) {
            self.userName = userName
        }
        if let paid_amount = try container.decodeIfPresent(DynamicDataType.self, forKey: .paid_amount) {
            self.paid_amount = paid_amount
        }
    }

}

public struct DynamicDataType: Codable {
    let type: String?

    public init(from decoder: Decoder) throws {
        let container =  try decoder.singleValueContainer()

        // Decode the double
        do {
            let doubleVal = try container.decode(String.self)
            self.type = doubleVal
        } catch DecodingError.typeMismatch {
            // Decode the string
            do {
                let stringVal = try container.decode(Double.self)
                self.type = String(stringVal)
            } catch DecodingError.typeMismatch {
                // Decode the string
                do {
                    let IntVal = try container.decode(Int.self)
                    self.type = String(IntVal)
                } catch DecodingError.typeMismatch {
                    // Decode the string
                    let floatVal = try container.decode(Float.self)
                    self.type = String(floatVal)
                }
            }
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(type)
    }
}
