//
//  CNOrderViewCellTableCell.swift
//  CircleNest
//
//  Created by techjini on 10/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

protocol OrderViewDelegate {
    func actionPerformedOrderViewDetails(indexPath: Int)
}

class CNOrderViewCellTableCell: UITableViewCell {

    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var lblCouponPriceValue: UILabel!
    @IBOutlet weak var lblCouponCodeValue: UILabel!
    @IBOutlet weak var lblStorePriceValue: UILabel!
    @IBOutlet weak var lblDeliveryChargeValue: UILabel!
    @IBOutlet weak var lblTotalValue: UILabel!
    @IBOutlet weak var lblOrderIdValue: UILabel!
    @IBOutlet weak var lblInvoiceNoValue: UILabel!
    @IBOutlet weak var lblCashOnDeliveryValue: UILabel!
    @IBOutlet weak var lblPaidAmountValue: UILabel!
    @IBOutlet weak var btnViewDetails: UIButton!
    @IBOutlet weak var cnstCouponCardView: NSLayoutConstraint!
    @IBOutlet weak var vwPriceDetailView: UIView!
    
    var protocolDelegate: OrderViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //set the values for top,left,bottom,right margins
    }
    
    func setupUI() {
        self.lblOrderStatus.layer.cornerRadius = self.lblOrderStatus.frame.height/2
        self.lblOrderStatus.layer.masksToBounds = true
    }
    
    @IBAction func btnViewDetailsAction(_ sender: UIButton) {
        protocolDelegate?.actionPerformedOrderViewDetails(indexPath: sender.tag)
    }
    
}
