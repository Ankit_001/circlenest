//
//  CNMyorderListVC.swift
//  CircleNest
//
//  Created by Ankit Gupta on 13/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNMyorderListVC: UIViewController {

    var drawerVw = DrawerView()
    var vwBG = UIView()
    var array_order: [orderData]?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noOrderImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.callUserOrderListAPI()
    }
    
    func setupUI() {

        self.tableView.register(UINib(nibName: "CNOrderViewCellTableCell", bundle: nil), forCellReuseIdentifier: "CNOrderViewCellTableCell")
        
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func callUserOrderListAPI(){
        self.showHUD(progressLabel: "Loading")
        let vm = CNMyOrderListVM()
        var paramDict  = ["method" : "getOrderList","storeId" : 101] as [String:AnyObject]
        let userDefaults = UserDefaults.standard
        if let userId = userDefaults.value(forKey: "user_id") {
            paramDict.updateValue(userId as AnyObject, forKey: "userId")
        }
        vm.callUserOrderListAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.array_order = result?.data
                self.noOrderImage.isHidden = self.array_order?.count ?? 0 > 0
                self.tableView.reloadData()
            } else {
                self.noOrderImage.isHidden = false
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }

}

extension CNMyorderListVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.array_order?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CNOrderViewCellTableCell", for: indexPath) as? CNOrderViewCellTableCell{
            cell.btnViewDetails.tag = indexPath.row
            cell.protocolDelegate = self
            let orderData = self.array_order?[indexPath.row]
            cell.lblOrderDate.text = "\(orderData?.orderDate?.type ?? "")"
            cell.lblOrderStatus.text = "\(orderData?.orderStatus?.type?.uppercased() ?? "")"
            cell.lblCouponPriceValue.text = "₹ \(orderData?.couponPrice?.type ?? "")"
            cell.lblCouponCodeValue.text = "\(orderData?.couponName?.type ?? "")"
            cell.lblStorePriceValue.text = "₹ \(orderData?.sub_total?.type ?? "")"
            cell.lblDeliveryChargeValue.text = "₹ \(orderData?.shippingAmount?.type ?? "")"
            cell.lblTotalValue.text = "₹ \(orderData?.TotalAmount?.type ?? "")"
            cell.lblOrderIdValue.text = "\(orderData?.id?.type ?? "")"
            cell.lblInvoiceNoValue.text = "\(orderData?.invoiceNo?.type ?? "")"
            cell.lblPaidAmountValue.text = "₹ \(orderData?.paid_amount?.type ?? "")"
            cell.lblCashOnDeliveryValue.text = "₹ \(orderData?.cashOnDelivery?.type ?? "")"
            if cell.lblCouponCodeValue.text?.isEmpty ?? false {
                cell.cnstCouponCardView.constant = 0
                cell.lblCouponCodeValue.text = ""
                cell.lblCouponPriceValue.text = ""
            } else {
                cell.cnstCouponCardView.constant = 49.0
            }
            return cell
            
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
}

extension CNMyorderListVC : OrderViewDelegate{
    func actionPerformedOrderViewDetails(indexPath: Int) {
        let storyboard = UIStoryboard(name: "CircleNest", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "CNMyOrderDetailsVC") as? CNMyOrderDetailsVC
        vc?.indexValue = indexPath
        vc?.orderId = self.array_order?[indexPath].id
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
