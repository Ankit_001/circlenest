//
//  CNCartListCellTableViewCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 04/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
protocol btnCartDelegate {
    func btnDeletePressed(indexpath : IndexPath , cell : CNCartListCellTableViewCell)
    func btnPlusPressed(indexpath : IndexPath , cell : CNCartListCellTableViewCell)
    func btnMinusPressed(indexpath : IndexPath , cell : CNCartListCellTableViewCell)

}
class CNCartListCellTableViewCell: UITableViewCell {
    @IBOutlet weak var viewCard : UIView!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblWeight : UILabel!
    @IBOutlet weak var lblPrice : UILabel!
    @IBOutlet weak var imgProduct : UIImageView!
    @IBOutlet weak var btnDelete : UIButton!
    
    @IBOutlet weak var viewCount : UIView!
    @IBOutlet weak var btnPlus : UIButton!
    @IBOutlet weak var btnMinus : UIButton!
    @IBOutlet weak var lblCount : UILabel!
    var delegate : btnCartDelegate?
    var indexPath : IndexPath!

    override func awakeFromNib() {
        super.awakeFromNib()
        viewCard.layer.cornerRadius = 5.0
        viewCard.layer.masksToBounds = false
        viewCard.layer.shadowColor = UIColor.black.cgColor
        viewCard.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        viewCard.layer.shadowRadius = 5.0
        viewCard.layer.shadowOpacity = 0.3
        
    }
    
    func populatedata(dict_item : ProductsData){
        self.lblName.text = dict_item.name?.type
        
        let unit = dict_item.unit
        self.lblWeight.text = "\(dict_item.weight?.type ?? "")" + "\(" ")" + (unit?.type ?? "")
        self.lblPrice.text  = "₹ \(dict_item.price?.type ?? "")"
        let image = dict_item.image?.type?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        self.imgProduct.sd_setImage(with: URL(string: image ?? ""), placeholderImage: UIImage(named: "Placeholder"))
        self.lblCount.text = "\(dict_item.qty ?? 0)"
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func btnPlusPressed(_sender:Any){
        self.delegate?.btnPlusPressed(indexpath: indexPath, cell: self)
    }
    @IBAction func btnMinusPressed(_sender:Any){
        self.delegate?.btnMinusPressed(indexpath: indexPath, cell: self)
    }
    @IBAction func btnDeletePressed(_sender:Any){
        self.delegate?.btnDeletePressed(indexpath: indexPath, cell: self)
        
    }
}
