//
//  CNCartListVC.swift
//  CircleNest
//
//  Created by Ankit Gupta on 04/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNCartListVC: UIViewController {
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var viewCheckOut : UIView!
    @IBOutlet weak var btnCheckOut : UIButton!
    @IBOutlet weak var lblTotal : UILabel!
    @IBOutlet weak var viewEmptyCart: UIView!
    var array_main : [CategoryData] = []
    var array_cart  : [CartItem] = []
    var titleLabel : UILabel!
    var order_amount: Double?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCell()

        self.tableView.estimatedRowHeight = 1000
        self.tableView.rowHeight = UITableView.automaticDimension
        titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width-30, height: view.frame.height))
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont(name: "AirbnbCerealApp-Medium", size: 17)
    
        self.viewCheckOut.layer.cornerRadius = 5.0
        self.viewCheckOut.clipsToBounds = true
        self.btnCheckOut.layer.cornerRadius = 5.0
        self.btnCheckOut.clipsToBounds = true
        self.callCartListAPI()
        setItemCount()
    }
    
    func setItemCount(){
        var  totalCount : Int64 = 0
        self.array_cart = CNCoreDataManager.sharedManager.getCartItemData()
        if self.array_cart.count>0{
            for dict in array_cart{
                let count = dict.count
                totalCount = totalCount  + count
                titleLabel.text = "My Cart (" + "\(totalCount)" + ")"
                navigationItem.titleView = titleLabel
                
            }
        }else{
            titleLabel.text = "My Cart"
            navigationItem.titleView = titleLabel
            
        }
    }

    func registerCell(){
        self.tableView.register(UINib(nibName: "CNCartListCellTableViewCell", bundle: nil), forCellReuseIdentifier: "CNCartListCellTableViewCell")
        self.tableView.register(UINib(nibName: "CNHeaderCell", bundle: nil), forCellReuseIdentifier: "CNHeaderCell")

    }
    
    func setFooterView(response : CNCartListModal?){
        let view_footer = CNCartFooterView.instanceFromNib()
        view_footer.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 187)
        let total = (Double(response?.Total?.type ?? "0.0") ?? 0.0).roundTo(places: 2)
        let shipping = Double(50).roundTo(places: 2)
        let order_amount = (total+shipping).roundTo(places: 2)
        self.order_amount = order_amount
        view_footer.lblTotal.text = "₹ \(String(format: "%.2f", total))"
        view_footer.lblShiping.text = "₹ \(String(format: "%.2f", shipping))"
        view_footer.lblOrderAmt.text = "₹ \(String(format: "%.2f", order_amount))"
        self.lblTotal.text = "₹ \(String(format: "%.2f", order_amount))"
        self.tableView.tableFooterView = view_footer
    }
    @IBAction func btnBackPressed(_sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnCheckOutPressed(_sender:Any){
        if let totalOrderAmount = self.order_amount, totalOrderAmount >= Double(200) {
            let storyboard = UIStoryboard(name: "CircleNest", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "CNAddressAndScheduleTimeVC") as? CNAddressAndScheduleTimeVC
            vc?.order_Amount = totalOrderAmount
            vc?.array_main = self.array_main
            self.navigationController?.pushViewController(vc!, animated: true)
        }else {
            self.view.makeToast("Please place upto Rs.200")
        }
    }
    @IBAction func btnContinueShopiingAction(_ sender: UIButton) {
        //self.navigationController?.popViewController(animated: true)
        self.navigationController?.popToRootViewController(animated: false)

    }
    
    func callCartListAPI(){
        let user_id = UserDefaults.standard.string(forKey: "user_id")
        self.showHUD(progressLabel: "Loading")
        let vm = CNCartListVM()
        let paramDict  = ["method" : "getCartList2",
                          "storeId" : 101,
                          "userId" : user_id ?? "0"] as [String:AnyObject]
        
        vm.callCartListAPI(param: paramDict) { (success, message,response) in
            self.dismissHUD(isAnimated: true)
            if success && message.isEmpty {
                self.array_main = response?.data ?? []
                
                if self.array_main.count>0{
                    for dict in self.array_main{
                        if let array_product = dict.products{
                            for product in array_product{
                                let qty = product.qty
                                CNCoreDataManager.sharedManager.checkCartItem(dict_item: product, item_count: qty ?? 0)
                            }
                        }
                    }
                    self.tableView.reloadData()
                    self.setFooterView(response: response)
                    self.tableView.isHidden = false
                    self.viewCheckOut.isHidden = false
                    self.viewEmptyCart.isHidden = true
                }else{
                    self.tableView.reloadData()
                    self.tableView.tableFooterView = nil
                    self.tableView.isHidden = true
                    self.viewCheckOut.isHidden = true
                    self.viewEmptyCart.isHidden = false
                }
            }else{
                self.tableView.reloadData()
                self.tableView.tableFooterView = nil
                self.tableView.isHidden = true
                self.viewCheckOut.isHidden = true
                self.viewEmptyCart.isHidden = false
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }

    func deleteItemFromCart(productId : String){
        let user_id = UserDefaults.standard.string(forKey: "user_id")
        self.showHUD(progressLabel: "Loading")
        let vm = CNCartListVM()
        let paramDict  = ["method" : "removeCart",
                          "storeId" : 101,
                          "userId" : user_id ?? "0",
                          "productId" : productId] as [String:AnyObject]
        
        vm.deleteItemFromCartAPI(param: paramDict) { (success, message,response) in
            self.dismissHUD(isAnimated: true)
            if success && message.isEmpty {
                CNCoreDataManager.sharedManager.deleteItemFromCart(product_id: productId)
                self.callCartListAPI()
                self.setItemCount()
               
            }else{
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    
    func updateItemIntoCart(dict_item : ProductsData , count : Int){
        self.showHUD(progressLabel: "Loading")
        let vm = CNSubcategoryItemVM()
        let user_id = UserDefaults.standard.string(forKey: "user_id")
        let paramDict  = ["method" : "updateCart",
                          "storeId" : 101,
                          "userId" : user_id ?? "0",
                          "productId":dict_item.id?.type ?? "",
                          "qty":count] as [String:AnyObject]
        print(paramDict)
        
        vm.callAddItemIntoCarttAPI(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.insertItemIntoDB(dict_item: dict_item, count: count)
                self.callCartListAPI()
                self.setItemCount()
            
            } else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    func removeItemFromCart(dict_item : ProductsData , count : Int){
        self.showHUD(progressLabel: "Loading")
        let vm = CNSubcategoryItemVM()
        let user_id = UserDefaults.standard.string(forKey: "user_id")
        let paramDict  = ["method" : "removeCart",
                          "storeId" : 101,
                          "userId" : user_id ?? "0",
                          "productId":dict_item.id?.type ?? "",
                          ] as [String:AnyObject]
        print(paramDict)
        
        vm.removeItemFromCart(param: paramDict) { (success , responseMessage, result) in
            self.dismissHUD(isAnimated: true)
            if success {
                self.insertItemIntoDB(dict_item: dict_item, count: count)
                self.callCartListAPI()
                self.setItemCount()
                
            
            } else {
                self.view.makeToast("Somethinng went wrong")
            }
        }
    }
    
    func insertItemIntoDB(dict_item : ProductsData , count : Int){
        CNCoreDataManager.sharedManager.checkCartItem(dict_item: dict_item, item_count: count)
    }
    

}
extension CNCartListVC : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.array_main.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array_item = self.array_main[section].products{
           return array_item.count
        }else{
            return  0
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CNCartListCellTableViewCell", for: indexPath) as? CNCartListCellTableViewCell{
            cell.indexPath = indexPath
            cell.delegate = self
            
            if let array_item = self.array_main[indexPath.section].products{
                let dict_item = array_item[indexPath.row]
                cell.populatedata(dict_item: dict_item)

            }
            return cell

        }else{
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CNHeaderCell") as? CNHeaderCell{
            cell.lblHeader.text = self.array_main[section].name?.type
            return cell

        }else{
            return UITableViewCell()
        }
    }
}
extension CNCartListVC :btnCartDelegate{
    func btnDeletePressed(indexpath: IndexPath, cell: CNCartListCellTableViewCell) {
        if let array_item = self.array_main[indexpath.section].products{
            let dict_item = array_item[indexpath.row]
            self.deleteItemFromCart(productId: dict_item.id?.type ?? "")
        }
    }
    func btnPlusPressed(indexpath: IndexPath, cell: CNCartListCellTableViewCell) {
        if let array_item = self.array_main[indexpath.section].products{
            let dict_item = array_item[indexpath.row]
            let count = Int(cell.lblCount.text ?? "") ?? 0
            let count_inc = count+1
            cell.lblCount.text = "\(count_inc)"
            self.updateItemIntoCart(dict_item: dict_item, count: count_inc)

        }
    }
    
    func btnMinusPressed(indexpath: IndexPath, cell: CNCartListCellTableViewCell) {
        if let array_item = self.array_main[indexpath.section].products{
            let dict_item = array_item[indexpath.row]
            let count = Int(cell.lblCount.text ?? "") ?? 0
            let count_inc = count-1
            
            if count_inc == 0{
                self.removeItemFromCart(dict_item: dict_item, count: count_inc)
            }else{
                cell.lblCount.text = "\(count_inc)"
                self.updateItemIntoCart(dict_item: dict_item, count: count_inc)
            }

        }
    
    }
    
    
}

