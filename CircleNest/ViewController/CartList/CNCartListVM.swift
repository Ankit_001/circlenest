//
//  CNCartListVM.swift
//  CircleNest
//
//  Created by Ankit Gupta on 04/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNCartListVM: NSObject {
    func callCartListAPI (param:[String:Any],completionHandler: @escaping (_ success: Bool, _ message: String, _ response : CNCartListModal?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    let result = Utill.parseObject(CNCartListModal.self, data: json)
                    completionHandler(true,"",result)

                }
            case .failure :
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
                           
            }
        }
    }
    func deleteItemFromCartAPI (param:[String:Any],completionHandler: @escaping (_ success: Bool, _ message: String, _ response : CNCartListModal?) -> Void) {
        
        let aStrApi = Utill.kBaseUrl.baseUrl
        APIManager().callPostApi(baseUrl: aStrApi, param: param) { response in
            print(response)
            switch response.result {
            case .success :
                let jsonData = response.data
                
                guard let json = (try? JSONSerialization.jsonObject(with: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    return
                }
                DispatchQueue.main.async {
                    let result = Utill.parseObject(CNCartListModal.self, data: json)
                    completionHandler(true,"",result)

                }
            case .failure :
                Utill.showAlert(title: "Circle Nest", Message: "Somethinng went wrong")
                           
            }
        }
    }

}
