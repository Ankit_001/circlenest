//
//  CNCartFooterView.swift
//  CircleNest
//
//  Created by Ankit Gupta on 04/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit


class CNCartFooterView: UIView {
    @IBOutlet weak var viewCard : UIView!
    @IBOutlet weak var lblTotal : UILabel!
    @IBOutlet weak var lblShiping : UILabel!
    @IBOutlet weak var lblOrderAmt : UILabel!

    override func awakeFromNib() {
        viewCard.layer.cornerRadius = 5.0
        viewCard.layer.masksToBounds = false
        viewCard.layer.shadowColor = UIColor.black.cgColor
        viewCard.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        viewCard.layer.shadowRadius = 5.0
        viewCard.layer.shadowOpacity = 0.3
    }
    
    class func instanceFromNib() -> CNCartFooterView {
        return UINib(nibName: "CNCartFooterView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CNCartFooterView
    }

    

}
