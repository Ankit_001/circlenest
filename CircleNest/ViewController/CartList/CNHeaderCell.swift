//
//  CNHeaderCell.swift
//  CircleNest
//
//  Created by Ankit Gupta on 04/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNHeaderCell: UITableViewCell {
    @IBOutlet weak var lblHeader : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
