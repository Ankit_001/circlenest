//
//  CNCartListModal.swift
//  CircleNest
//
//  Created by Ankit Gupta on 04/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit


class CNCartListModal: Codable {
    public var responseCode : DynamicDataType?
    public var Total : DynamicDataType?
    public var stockValidate : Bool?
    public var data : [CategoryData]?
}


public class CategoryData : Codable {
    public var id : DynamicDataType?
    public var name : DynamicDataType?
    public var products : [ProductsData]?

}
public class ProductsData : Codable {
    public var subsubcatcategory_name : DynamicDataType?
    public var subcat_id : DynamicDataType?
    public var id : DynamicDataType?
    public var name : DynamicDataType?
    public var parent : DynamicDataType?
    public var qty : Int?
    public var price : DynamicDataType?
    public var final_price : DynamicDataType?
    public var offer_price : DynamicDataType?
    public var discount : DynamicDataType?
    public var weight : DynamicDataType?
    public var unit : DynamicDataType?
    public var stock : DynamicDataType?
    public var stock_qty : DynamicDataType?
    public var isAvailable : Bool?
    public var total : DynamicDataType?
    public var image : DynamicDataType?
    public var subsubcategory_id : DynamicDataType?

}
