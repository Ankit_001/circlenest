//
//  ComingSoonVC.swift
//  CircleNest
//
//  Created by techjini on 01/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class CNComingSoonVC: UIViewController {
    
    @IBOutlet weak var ivComingSoon: UIImageView!
    var isComingFrom: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        if self.isComingFrom == "Breathe Clean With Paudhewale" {
            self.title = "Breathe Clean With Paudhewale"
            self.ivComingSoon.image = UIImage(named: "noplant.jpeg")
        }

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
