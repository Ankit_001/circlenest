//
//  CNBaseViewController.swift
//  CircleNest
//
//  Created by Ankit Gupta on 02/11/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit



class CNBaseViewController: UIViewController {
    var titleLabel : UILabel!
    var countLabel : UILabel!
    let size:CGFloat = 25
    var array_cart  : [CartItem] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false
        titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width-30, height: view.frame.height))
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont(name: "AirbnbCerealApp-Medium", size: 17)
        navigationItem.titleView = titleLabel

        self.countLabel = UILabel(frame: CGRect(x: 0, y: -10, width: 23, height: 23))
        

        self.countLabel.font = UIFont(name: "AirbnbCerealApp-Medium", size: 13)
        self.countLabel.textColor = .black
        self.countLabel.textAlignment = .center

        // button
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        rightButton.setBackgroundImage(UIImage(named: "cart"), for: .normal)
        rightButton.addTarget(self, action: #selector(didTapCartButton), for: .touchUpInside)
        rightButton.addSubview(self.countLabel)

        let rightBarButtomItem = UIBarButtonItem(customView: rightButton)
        navigationItem.rightBarButtonItem = rightBarButtomItem

//        navigationItem.rightBarButtonItems = [cartButton, searchButton]
        

    }
    
    func setItemCount(){
        var  totalCount : Int64 = 0
        self.array_cart = CNCoreDataManager.sharedManager.getCartItemData()
        if self.array_cart.count>0{
            for dict in array_cart{
                let count = dict.count
                totalCount = totalCount  + count
                countLabel.text = "\(totalCount)"
                countLabel.isHidden = false
            }
        }else{
            countLabel.text = "0"
            countLabel.isHidden = true
        }
    }

    
    @objc func didTapCartButton(sender: Any){
        let vc = self.storyboard?.instantiateViewController(identifier: "CNCartListVC") as? CNCartListVC
        vc?.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc!, animated: true)
    }

    @objc func didTapSearchButton(sender: Any){

    }
    
}
