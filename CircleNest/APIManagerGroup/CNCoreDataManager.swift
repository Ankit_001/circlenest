//
//  CNCoreDataManager.swift
//  CircleNest
//
//  Created by Ankit Gupta on 27/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
import CoreData


let appDelegate = UIApplication.shared.delegate as! AppDelegate
let moc = appDelegate.persistentContainer.viewContext


class CNCoreDataManager {
    static let sharedManager = CNCoreDataManager()
    private init() {} // Prevent clients from creating another instance.

    func saveContext () {
        do {
            try moc.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    func getWishListData() -> [Wishlist]{
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Wishlist")
        let result = try? moc.fetch(fetchRequest)
        if let array_wishlist = result as? [Wishlist]{
            return array_wishlist
        }
        return []
    }
    
    func insertWishlistItemInLocalDB(dict_item : item_list, cat_id : String){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Wishlist")
        let result = try? moc.fetch(fetchRequest)
        if let resultData = result as? [Wishlist]{
            for object in resultData {
                if object.id == dict_item.id?.type {
                    moc.delete(object)
                    self.saveContext()
                    return
                }
            }
            self.saveInfoForWishList(dict_item: dict_item, cat_id: cat_id)
        }
    }
    func saveInfoForWishList(dict_item : item_list, cat_id : String){
        let entity = NSEntityDescription.entity(forEntityName: "Wishlist",in: moc)!
        let wishList = NSManagedObject(entity: entity, insertInto: moc)
        wishList.setValue(dict_item.name?.type, forKeyPath: "name")
        wishList.setValue(dict_item.id?.type, forKeyPath: "id")
        wishList.setValue(dict_item.brand?.type, forKeyPath: "brand")
        wishList.setValue(dict_item.description?.type, forKeyPath: "desc")
        wishList.setValue(dict_item.discount?.type, forKeyPath: "discount")
        wishList.setValue(dict_item.image?.type, forKeyPath: "image")
        wishList.setValue(dict_item.isAvailable, forKeyPath: "isAvailable")
        wishList.setValue(dict_item.price?.type, forKeyPath: "price")
        wishList.setValue(dict_item.sku?.type, forKeyPath: "sku")
        wishList.setValue(dict_item.status?.type, forKeyPath: "status")
        wishList.setValue(dict_item.stock?.type, forKeyPath: "stock")
        wishList.setValue(dict_item.stock_qty?.type, forKeyPath: "stock_qty")
        wishList.setValue(dict_item.storeId?.type, forKeyPath: "storeId")
        wishList.setValue(dict_item.weight?.type, forKeyPath: "weight")
        wishList.setValue(dict_item.category?.type, forKeyPath: "category")
        wishList.setValue(cat_id, forKeyPath: "cat_id")
        //wishList.setValue(dict_item.variants, forKeyPath: "variants")
        self.saveContext()
        
    }
    func deleteWishlistItemFromLocalDB(item_id : String){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Wishlist")
        let result = try? moc.fetch(fetchRequest)
        if let resultData = result as? [Wishlist]{
            for object in resultData {
                if object.id == item_id{
                    moc.delete(object)
                    self.saveContext()
                    return
                }
            }
        }
    }
    
    func checkItemPresence(dict_item : item_list,item_count : Int){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CartItem")
        let result = try? moc.fetch(fetchRequest)
        if let resultData = result as? [CartItem]{
            for object in resultData {
                if object.id == dict_item.id?.type {
                    moc.delete(object)
                    if item_count>0{
                      self.saveInfoForCartItem(dict_item : dict_item ,item_count : Int64(item_count))
                    }
                    
                    self.saveContext()
                    return
                }
            }
            self.saveInfoForCartItem(dict_item : dict_item, item_count : Int64(item_count))
        }
    }
    
    func saveInfoForCartItem(dict_item : item_list,item_count : Int64){
        let entity = NSEntityDescription.entity(forEntityName: "CartItem",in: moc)!
        let cart_item = NSManagedObject(entity: entity, insertInto: moc)
        
        cart_item.setValue(dict_item.id?.type, forKeyPath: "id")
        cart_item.setValue(dict_item.name?.type, forKeyPath: "name")
        cart_item.setValue(dict_item.price?.type, forKeyPath: "price")
        cart_item.setValue(item_count, forKeyPath: "count")
        self.saveContext()
    }
    func getCartItemData() -> [CartItem]{
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CartItem")
        let result = try? moc.fetch(fetchRequest)
        if let array_cart = result as? [CartItem]{
            return array_cart
        }
        return []
    }
    
    func deleteAllItemFromCart(){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CartItem")
        let result = try? moc.fetch(fetchRequest)
        if let resultData = result as? [CartItem]{
            for object in resultData {
                moc.delete(object)
                self.saveContext()
            }
        }
    }
    
    func deleteItemFromCart(product_id : String){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CartItem")
        let result = try? moc.fetch(fetchRequest)
        if let resultData = result as? [CartItem]{
            for object in resultData {
                if object.id == product_id{
                    moc.delete(object)
                    self.saveContext()
                    return
                }
            }
        }
    }

    func checkCartItem(dict_item : ProductsData,item_count : Int){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CartItem")
        let result = try? moc.fetch(fetchRequest)
        if let resultData = result as? [CartItem]{
            for object in resultData {
                if object.id == dict_item.id?.type {
                    moc.delete(object)
                    if item_count>0{
                      self.saveItemIntCart(dict_item : dict_item ,item_count : Int64(item_count))
                    }
                    
                    self.saveContext()
                    return
                }
            }
            self.saveItemIntCart(dict_item : dict_item, item_count : Int64(item_count))
        }
    }
    func saveItemIntCart(dict_item : ProductsData,item_count : Int64){
        let entity = NSEntityDescription.entity(forEntityName: "CartItem",in: moc)!
        let cart_item = NSManagedObject(entity: entity, insertInto: moc)
        
        let price = "\(dict_item.price?.type ?? "")"
        cart_item.setValue(dict_item.id?.type, forKeyPath: "id")
        cart_item.setValue(dict_item.name?.type, forKeyPath: "name")
        cart_item.setValue(price, forKeyPath: "price")
        cart_item.setValue(item_count, forKeyPath: "count")
        self.saveContext()
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
//
//    func checkItemPresenceInCart(dict_item : CategoryData,item_count : Int){
//        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CartItem")
//        let result = try? moc.fetch(fetchRequest)
//        if let resultData = result as? [CartItem]{
//            for object in resultData {
//                if object.id == dict_item.id?.type {
//                    moc.delete(object)
//                    if item_count>0{
//                      self.saveInfoForCartItem(dict_item : dict_item ,item_count : Int64(item_count))
//                    }
//
//                    self.saveContext()
//                    return
//                }
//            }
//            self.saveInfoForCartItem(dict_item : dict_item, item_count : Int64(item_count))
//        }
//    }
//
//    func saveInfoForCartItem(dict_item : item_list,item_count : Int64){
//        let entity = NSEntityDescription.entity(forEntityName: "CartItem",in: moc)!
//        let cart_item = NSManagedObject(entity: entity, insertInto: moc)
//
//        cart_item.setValue(dict_item.id?.type, forKeyPath: "id")
//        cart_item.setValue(dict_item.name?.type, forKeyPath: "name")
//        cart_item.setValue(dict_item.price?.type, forKeyPath: "price")
//        cart_item.setValue(item_count, forKeyPath: "count")
//        self.saveContext()
//    }
    

}

