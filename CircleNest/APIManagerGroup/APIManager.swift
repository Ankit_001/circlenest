//
//  APIManager.swift
//  CircleNest
//
//  Created by techjini on 22/10/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import Foundation
import Alamofire

class APIManager: NSObject {
    
    func callPostApi(baseUrl: String, param:[String:Any],completionHandler: @escaping (_ response : AFDataResponse<Any>) -> Void) {
        AF.request(baseUrl, method: .post, parameters: param, encoding: JSONEncoding.default)
            .responseJSON { response in
                completionHandler(response)
        }
    }
    
}
