//
//  Utill.swift
//  CircleNest
//
//  Created by Ankit Gupta on 17/09/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
import MBProgressHUD

let keyWindow = UIApplication.shared.connectedScenes
.lazy
.filter { $0.activationState == .foregroundActive }
.compactMap { $0 as? UIWindowScene }
.first?
.windows
.first { $0.isKeyWindow }

var isPushed: Bool = false
class Utill: NSObject {

    struct kBaseUrl {
        static let baseUrl = "http://mothersnests.com/rest/api1.php"
        
    }
    
    struct Color {
        static let color_blue = UIColor(red: 58.0/255.0, green: 86.0/255.0, blue: 159.0/255.0, alpha: 1.0)
        static let color_orrange = UIColor(red: 255.0/255.0, green: 133.0/255.0, blue: 75.0/255.0, alpha: 1.0)

    }
    
    
    struct DrawerArray {
        static let array:NSArray = ["Home", "Become A Member", "Become A Partner","Own A Store", "My Wallet", "Refer A Friend", "About Us","Help & Support","Logout"]
        
        static let img_array:NSArray =  ["Homemenu","becomemember","becomepartner","ownstore","MyWallet","referfrnd","About","Support","Logout"]
    }
    
    
    
    class func showAlertWithLogout(title : String , Message : String){
        let alert = UIAlertController(title: title, message: Message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
            
            let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CNLoginStartupVC")
            let window = UIApplication.shared.windows.first
            let nav = UINavigationController(rootViewController: loginVC)
            window?.rootViewController = nav
            window?.makeKeyAndVisible()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
        }))
        

        keyWindow?.rootViewController?.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    class func showAlert(title : String , Message : String){
        let alert = UIAlertController(title: title, message: Message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in }))

        keyWindow?.rootViewController?.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    class func showAlertWithMessage(title : String , Message : String, viewController: UIViewController){
        let alert = UIAlertController(title: title, message: Message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (_) in }))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    
    class func scrollNextCellUsingDots(collectionView : UICollectionView , pageControl : UIPageControl){
        let cellSize = CGSize(width: collectionView.frame.width, height:collectionView.frame.height);
        let contentOffset = collectionView.contentOffset;
        if collectionView.contentSize.width <= collectionView.contentOffset.x + cellSize.width{
            collectionView.scrollRectToVisible(CGRect(x: 0, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true);
            pageControl.currentPage = 0
       
        }else {
            collectionView.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true);
            pageControl.currentPage += 1
        }
    }
    
    class func settingDateFormatForDate(dateFormat: String, dateValue: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: dateValue)
    }
    class func parseObject<T: Codable>(_: T.Type, data: [String: Any]) -> T? {
        do {
            let resultsData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            return try JSONDecoder().decode(T.self, from: resultsData)
        } catch {
            //Logger.log("\(error)")
            print(error.localizedDescription)
            return nil
        }
    }
    class func addNavigationButton(navigationItem : UINavigationItem){
        let cartButton   = UIBarButtonItem(image: UIImage(named: "cart"),  style: .plain, target: self, action: #selector(self.didTapCartButton))

        let searchButton = UIBarButtonItem(image: UIImage(named: "search"),  style: .plain, target: self, action: #selector(self.didTapSearchButton))

        cartButton.tintColor = UIColor.black
        searchButton.tintColor = UIColor.black
        navigationItem.rightBarButtonItems = [cartButton, searchButton]
    }
    @objc func didTapCartButton(sender: Any){
        
    }

    @objc func didTapSearchButton(sender: Any){
    
    }
    
    class func openShareLinkDialogue(viewController: UIViewController) {
        let items = [URL(string: "https://www.apple.com")!]
        let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)

        if let popoverController = activityViewController.popoverPresentationController {
            popoverController.sourceRect = CGRect(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2, width: 0, height: 0)
            popoverController.sourceView = viewController.view
            popoverController.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
        }

        viewController.present(activityViewController, animated: true, completion: nil)
    }
 
}



enum ConstantValue : String {
    case title = "CircleNest"
    case emailAlertMessage = "Please enter correct email"
    case mobileAlertMessage = "Please enter correct mobile"
    case otpAlertMessage = "Please enter correct OTP"
    case nameAlertMessage = "Please enter your Full Name"
    case storeAlertMessage = "Please Select your store"
    case dobAlertMessage = "Please Select your Date of birth"
    case addressAlertMessage = "Please enter your full address"
    case cityAlertMessage = "Please enter your city"
    case stateAlertMessage = "Please enter your state"
    case pincodeAlertMessage = "Please enter your pincode"
    case feedbackAlertMessage = "Please enter your feedback"
    case descriptionAlertMessage = "Please enter detailed description"
    case categoryAlertMessage = "Please select your category"
    case planAlertMessage = "Please select your membership plan"
    case collateralMessage = "Please enter correct values for the fields"
    case houseMessage = "Please enter your house number or block number"
    case streetMessage = "Please enter your street address"
    case stateMessage = "Please enter your current address state"
    case cityMessage = "Please enter your current address city"
    case pincodeMessage = "Please enter your current address pincode"
    case orderCancelError = "Please select a reason for cancellation"
    case noWalletBalance = "Your wallet has no money, Pay only by COD"
}

private var __maxLengths = [UITextField: Int]()
extension UITextField {
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return 150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    @objc func fix(textField: UITextField) {
        if let t = textField.text {
            textField.text = String(t.prefix(maxLength))
        }
    }
}

@IBDesignable
extension UITextField {

    @IBInspectable var paddingLeftCustom: CGFloat {
        get {
            return leftView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            leftView = paddingView
            leftViewMode = .always
        }
    }

    @IBInspectable var paddingRightCustom: CGFloat {
        get {
            return rightView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            rightView = paddingView
            rightViewMode = .always
        }
    }
}

@IBDesignable extension UIView {
    @IBInspectable var shadowColor: UIColor?{
        set {
            guard let uiColor = newValue else { return }
            layer.shadowColor = uiColor.cgColor
        }
        get{
            guard let color = layer.shadowColor else { return nil }
            return UIColor(cgColor: color)
        }
    }

    @IBInspectable var shadowOpacity: Float{
        set {
            layer.shadowOpacity = newValue
        }
        get{
            return layer.shadowOpacity
        }
    }

    @IBInspectable var shadowOffset: CGSize{
        set {
            layer.shadowOffset = newValue
        }
        get{
            return layer.shadowOffset
        }
    }

    @IBInspectable var shadowRadius: CGFloat{
        set {
            layer.shadowRadius = newValue
        }
        get{
            return layer.shadowRadius
        }
    }
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var maskToBound: Bool {
        get {
            return layer.masksToBounds
        }
        set {
            layer.masksToBounds = newValue
        }
    }

    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
}
extension UIImageView {
    
    func makeCircleImage() {
        self.layer.cornerRadius = self.frame.width / 2
        self.layer.masksToBounds = true
    }
}
extension String {
    var isValidEmail: Bool {
        let emailFormat = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: self)
    }
    
    var isValidPhoneNumberMBL : Bool {
        let regex = "^[56789]\\d{9}$"
        let numberTest = NSPredicate.init(format: "SELF MATCHES %@", regex)
        return numberTest.evaluate(with: self)
    }
    
    var isValidField : Bool {
        return !self.isEmpty
    }
    
    var bool: Bool {
        let lowercaseSelf = self.lowercased()
        switch lowercaseSelf {
        case "true", "yes", "1":
            return true
        case "false", "no", "0":
            return false
        default:
            return false
        }
    }
    
}
extension UIViewController {

    func showHUD(progressLabel:String){
        DispatchQueue.main.async{
            let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
            progressHUD.label.text = progressLabel
        }
    }

    func dismissHUD(isAnimated:Bool) {
        DispatchQueue.main.async{
            MBProgressHUD.hide(for: self.view, animated: isAnimated)
        }
    }
}

extension Double {
    func roundTo(places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension Date {
    static var tomorrow:  Date { return Date().tommDate }
    var tommDate: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: Date()) ?? Date()
    }
    
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
